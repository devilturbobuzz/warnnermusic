<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artist extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->model('artist_m');
		$this->load->library('session');
		if(!$this->session->userdata('username_session')){
			redirect("/Login");
		}
    }
	public function index()
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['arr_data']=$this->artist_m->selectdata(NULL);
		$this->load->view('artist',$data);
		
			
	}
	public function search(){
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['val_search']=$this->input->post('keyword');
		$data['arr_data']=$this->artist_m->selectdata_search($this->input->post('keyword'));
		$this->load->view('artist',$data);
	}
	public function search_album(){
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['val_search']=$this->input->post('keyword');
		$data['arr_data']=$this->artist_m->selectdata_album_search($this->input->post('keyword'));
		$this->load->view('album',$data);
	}
	
	
	public function Form()
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['arr_nationality'] =$this->artist_m->selectdata_nationality(NULL);
		$data['arr_genres']=$this->artist_m->selectdata_genres(NULL);
		$data['arr_gallery']=$this->artist_m->selectdata_gallery(NULL);
		$data['arr_online_ch']=$this->artist_m->selectdata_online_ch(NULL);
		$this->load->view('artist_form',$data);
		
			
	}
	public function Mood()
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['arr_data'] =$this->artist_m->selectdata_mood(NULL);
		$this->load->view('mood',$data);
		
	}
	public function FormMood()
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$this->load->view('mood_form');
		
			
	}
	public function Nationality()
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['arr_data'] =$this->artist_m->selectdata_nationality(NULL);
		$this->load->view('nationality',$data);
		
	}
	public function FormNationality()
	{
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$this->load->view('nationality_form');

	}
	public function Album()
	{
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['arr_data'] =$this->artist_m->selectdata_album(NULL);
	   
		$this->load->view('album',$data);		
	}
	public function FormAlbum()
	{
		$data = array();
		$data['data_artist'] =$this->artist_m->selectdata(NULL);
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$this->load->view('album_form',$data);		
	}
	public function genres()
	{
		$this->load->view('head');
		$this->load->view('_nav');
		$data['arr_data']=$this->artist_m->selectdata_genres(NULL);
		$this->load->view('genres',$data);		
	}
	
	public function FormGenres()
	{
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$this->load->view('genres_form');

	}
	
	public function Form_save_genres()
	{
		$id=$this->input->post('id');
		$date_stamp=str_replace("/","-",$this->input->post('Release_on'));
		$data=array();
		$data['Release_on']= date('Y-m-d',strtotime($date_stamp));
		$data['genres_name']= $this->input->post('genres_name');
		$data['updatedatetime']= date("Y-m-d h:i:s");
		if($_FILES["fileToUpload_thumnail"]["name"]){
			$url_picture = $this->do_upload_genres();
			$data['thumnail_path']= $url_picture;
		}
		
		if($id==""){
			$data['createdatetime']= date("Y-m-d h:i:s");
		}	
		$data['staff']=$this->session->userdata('username_session');
		//$data['tags']= $this->input->post('tags');
		$this->artist_m->save($data,"genres",$id,"id");
		redirect("/Artist/genres");

	}
	
	
	public function EditArtist($id)
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['arr_nationality'] =$this->artist_m->selectdata_nationality(NULL);
		$data['arr_genres']=$this->artist_m->selectdata_genres(NULL);
		$data['arr_gallery']=$this->artist_m->selectdata_gallery(NULL);
		$data['arr_online_ch']=$this->artist_m->selectdata_online_ch(NULL);
		$data['editdata']=$this->artist_m->selectdata($id);
		$this->load->view('artist_form',$data);
		
	}
	public function EditMood($id)
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['editdata']=$this->artist_m->selectdata_mood($id);
		$this->load->view('mood_form',$data);	
	}
	public function EditNationality($id)
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['editdata']=$this->artist_m->selectdata_nationality($id);
		$this->load->view('nationality_form',$data);	
	}
	public function EditGenres($id)
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['editdata']=$this->artist_m->selectdata_genres($id);
		$this->load->view('genres_form',$data);	
	}
	public function EditAlbum($id)
	{
		$data = array();
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['data_artist']=$this->artist_m->selectdata(NULL);
		$data['data_album']=$this->artist_m->selectdata_album($id);
		$data['arr_gallery']=$this->artist_m->selectdata_gallery(NULL);
		$this->load->view('album_form',$data);	
	}

	public function form_delete($id)
	{
		 $this->artist_m->deletedata($id);
		$this->artist_m->deletedata_album_artist($id);
		
		 redirect("/Artist");
			
	}
	public function form_delete_album($id)
	{
		 $this->artist_m->deletedata_album($id);
		 redirect("/Artist/album");
			
	}
	public function form_delete_mood($id)
	{
		 $this->artist_m->deletedata_mood($id);
		 redirect("/Artist/mood");
			
	}
	public function form_delete_nationality($id,$id_update)
	{
		 $data=array();
		 $data['Nationality']= $id_update;
		 $this->artist_m->save($data,"artist",$id,"Nationality");
		 $this->artist_m->deletedata_nationality($id);
		 redirect("/Artist/Nationality");
			
	}
	public function form_delete_genres($id,$id_update)
	{
		 $data=array();
		 $data['Genres']= $id_update;
		 $this->artist_m->save($data,"artist",$id,"Genres");
		 $this->artist_m->deletedata_genres($id);
		 redirect("/Artist/genres");
			
	}
	
	public function Form_save()
	{
		$id=$this->input->post('id');
		$data=array();
		$data['artist_name']= $this->input->post('artist_name');
		$data['Nationality']= $this->input->post('Nationality');
		$data['Description']= $this->input->post('Description');
		$data['Genres']= $this->input->post('Genres');
		$data['updatedatetime']= date("Y-m-d h:i:s");
		if($_FILES["fileToUpload_thumnail"]["name"]){
			$url_picture = $this->do_upload1();
			$data['thumnail_path']= $url_picture;
		}
		if($_FILES["fileToUpload_highlight"]["name"]){
			$url_highlight = $this->do_upload2();
			$data['pic_description_path']= $url_highlight;
		}
		if($id==""){
			$data['createdatetime']= date("Y-m-d h:i:s");
		}
		if(count($this->input->post('url_subject'))>0){
			$url_subject="";
			foreach ($this->input->post('url_subject') as $key => $value) {
				$url_subject.=$value.",";
			}
			$data['url_subject']= rtrim($url_subject,",");
			
		}
		if(count($this->input->post('url_download'))>0){
			$url_download="";
			foreach ($this->input->post('url_download') as $key => $value) {
				$url_download.=$value.",";
			}
			$data['url_download']= rtrim($url_download,",");
			
		}
		if(count($this->input->post('url_off'))>0){
			$url_off="";
			foreach ($this->input->post('url_off') as $key => $value) {
				$url_off.=$value.",";
			}
			$data['url_off']= rtrim($url_off,",");
			
		}
		if(count($this->input->post('url_off_download'))>0){
			$url_off_download="";
			foreach ($this->input->post('url_download') as $key => $value) {
				$url_off_download.=$value.",";
			}
			$data['url_off_download']= rtrim($url_off_download,",");
			
		}
	
		$data['staff']=$this->session->userdata('username_session');
		$data['tags']= $this->input->post('tags');
		$this->artist_m->save($data,"artist",$id,"artist_id");
		
		/**** tags module ****/
		if($id==""){
			$id=$this->artist_m->select_max_artist();
			$id=$id[0]->id;
		}
		$this->artist_m->deletedata_tags($id);
		$data_tags=array();
		$data_tags['tags_module']="artist";
		$data_tags['staff']=$this->session->userdata('username_session');;
		$data_tags['id_module']=$id;
		$data_tags['updatedatetime']=date("Y-m-d h:i:s");
		$data_tags['createdatetime']=date("Y-m-d h:i:s");
		if(strpos($this->input->post('tags'),",")>0){
			$arr_tags=explode(",",$this->input->post('tags'));
			for($i=0;$i<count($arr_tags);$i++){
				$data_tags['tags_index']=$arr_tags[$i];
				if($arr_tags[$i]){
					$this->artist_m->save_tags($data_tags,$id);
				}
			}	
		}else{
			$data_tags['tags_index']=$this->input->post('tags');
			if($this->input->post('tags')){
				$this->artist_m->save_tags($data_tags,$id);
			}
				
		}
		/**** tags module ****/
		
		
		redirect("/artist");

	}
	public function Form_save_album()
	{
		$id=$this->input->post('id');
	 $date_stamp=str_replace("/","-",$this->input->post('Release_on'));
		
		$data=array();
		$data['Release_on']= date('Y-m-d',strtotime($date_stamp));
		$data['artist_id']= $this->input->post('Artist');
		$data['album_name']= $this->input->post('AlbumName');
		$data['description']= $this->input->post('description');
		$data['music_description']= $this->input->post('music_description');
		
		$data['updatedatetime']= date("Y-m-d h:i:s");
		if($id==""){
			$data['updatedatetime']= date("Y-m-d h:i:s");
		}	
		if($_FILES["fileToUpload_thumnail"]["name"]){
			$url_picture = $this->do_upload_ablum();
			$data['thumnail_path']= $url_picture;
		}
		
		if(count($this->input->post('url_subject'))>0){
			$url_subject="";
			foreach ($this->input->post('url_subject') as $key => $value) {
				$url_subject.=$value.",";
			}
			$data['url_subject']= rtrim($url_subject,",");
			
		}
		if(count($this->input->post('url_download'))>0){
			$url_download="";
			foreach ($this->input->post('url_download') as $key => $value) {
				$url_download.=$value.",";
			}
			$data['url_download']= rtrim($url_download,",");
			
		}
		$data['staff']=$this->session->userdata('username_session');
		//$data['tags']= $this->input->post('tags');
		$this->artist_m->save($data,"album",$id,"album_id");
		redirect("/artist/album");

	}
	public function Form_save_mood()
	{
		$id=$this->input->post('id');
		$data=array();
		$data['mood_description']= $this->input->post('mood_description');
		$data['updatedatetime']= date("Y-m-d h:i:s");
		if($id==""){
			$data['createdatetime']= date("Y-m-d h:i:s");
		}	
		if($_FILES["fileToUpload_thumnail"]["name"]){
			$url_picture = $this->do_upload_mood();
			$data['thumnail_path']= $url_picture;
		}
		//$data['tags']= $this->input->post('tags');
		$this->artist_m->save($data,"mood",$id,"mood_id");
		redirect("/artist/mood");

	}
	public function Form_save_nationality()
	{
		$id=$this->input->post('id');
		$date_stamp=str_replace("/","-",$this->input->post('Release_on'));
		$data=array();
		$data['Release_on']= date('Y-m-d',strtotime($date_stamp));
		$data['nationality_name']= $this->input->post('nationality_name');
		$data['updatedatetime']= date("Y-m-d h:i:s");
		if($id==""){
			$data['createdatetime']= date("Y-m-d h:i:s");
		}	
		//$data['tags']= $this->input->post('tags');
		$data['staff']=$this->session->userdata('username_session');
		$this->artist_m->save($data,"nationality",$id,"id");
		redirect("/artist/nationality");

	}
	private function do_upload_mood()
	{
		$type = explode('.', $_FILES["fileToUpload_thumnail"]["name"]);
		$type = strtolower($type[count($type)-1]);
		$url = "./shared/artist_upload/mood/".uniqid(rand()).'.'.$type;
		if(in_array($type, array("jpg", "jpeg", "png")))
			if(is_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"]))
				if(move_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"],$url))
					return $url;
		return "";
	}
	private function do_upload_ablum()
	{
		$type = explode('.', $_FILES["fileToUpload_thumnail"]["name"]);
		$type = strtolower($type[count($type)-1]);
		$url = "./shared/artist_upload/album/".uniqid(rand()).'.'.$type;
		if(in_array($type, array("jpg", "jpeg", "png")))
			if(is_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"]))
				if(move_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"],$url))
					return $url;
		return "";
	}
	private function do_upload1()
	{
		$type = explode('.', $_FILES["fileToUpload_thumnail"]["name"]);
		$type = strtolower($type[count($type)-1]);
		$url = "./shared/artist_upload/".uniqid(rand()).'.'.$type;
		if(in_array($type, array("jpg", "jpeg", "png")))
			if(is_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"]))
				if(move_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"],$url))
					return $url;
		return "";
	}
	private function do_upload2()
	{
		$type = explode('.', $_FILES["fileToUpload_highlight"]["name"]);
		$type = strtolower($type[count($type)-1]);
		$url = "./shared/artist_upload/".uniqid(rand()).'.'.$type;
		if(in_array($type, array("jpg", "jpeg", "png")))
			if(is_uploaded_file($_FILES["fileToUpload_highlight"]["tmp_name"]))
				if(move_uploaded_file($_FILES["fileToUpload_highlight"]["tmp_name"],$url))
					return $url;
		return "";
	}
	private function do_upload_genres()
	{
		$type = explode('.', $_FILES["fileToUpload_thumnail"]["name"]);
		$type = strtolower($type[count($type)-1]);
		$url = "./shared/genres_upload/".uniqid(rand()).'.'.$type;
		if(in_array($type, array("jpg", "jpeg", "png")))
			if(is_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"]))
				if(move_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"],$url))
					return $url;
		return "";
	}
	
}
