<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$this->load->model('home_m');
		$this->load->library('session');
		if(!$this->session->userdata('username_session')){
			redirect("/Login");
		}
    }
	public function index()
	{
		$data['arr_data']=$this->home_m->selectdata(NULL);
		$this->load->view('slide_top',$data);
	}
	public function slide_top_form()
	{
		$data['data_artist']=$this->home_m->selectdata_artist(NULL);
		$this->load->view('slide_top_form',$data);
	}
	public function Edit_slide_top($id)
	{
	
		$data['editdata']=$this->home_m->selectdata($id);
		$this->load->view('slide_top_form',$data);
	}

	public function form_delete_slide_top($id)
	{
		 $this->home_m->deletedata($id);
		 redirect("/Home");	
	}
	
	
	public function artist_month()
	{
		
		$data['arr_data'] =$this->home_m->selectdata_album(NULL);
		$this->load->view('artist_month',$data);		
	}

	public function search_artist_mounth()
	{
		
		$data['val_search']=$this->input->post('keyword');
		$data['arr_data']=$this->home_m->selectdata_artist_of_month_search($this->input->post('keyword'));
		$this->load->view('artist_month',$data);
	}
	
	public function save_artist_month($id,$highlight)
	{
		
		$data=array();
		$data['album_id']= $id;
		$data['artist_month']=$highlight;
		$data['updatedatetime']=date("Y-m-d h:i:s");
		$data['staff']=$this->session->userdata('username_session');
		$this->home_m->save($data,"album",$id,"album_id");
		redirect("/Home/artist_month");
			
	}	
	public function Form_save_slide_top()
	{
		$id=$this->input->post('id');
		$artist=$this->input->post('Artist');
		$date_stamp=str_replace("/","-",$this->input->post('released'));
	 
	
		$data=array();
		$data['released']= date('Y-m-d',strtotime($date_stamp));
		$data['url']=$this->input->post('url');
		$data['artist_name']= $artist;
		
		$data['updatedatetime']= date("Y-m-d h:i:s");
		if($id==""){
			$data['createdatetime']= date("Y-m-d h:i:s");
		}	
		if($_FILES["fileToUpload_thumnail"]["name"]){
			$url_picture = $this->do_upload1();
			$data['head_picture_path']= $url_picture;
		}
		$data['staff']=$this->session->userdata('username_session');
		$this->home_m->save($data,"home",$id,"id");
		$this->index();
	}


	private function do_upload1()
	{
		$type = explode('.', $_FILES["fileToUpload_thumnail"]["name"]);
		$type = strtolower($type[count($type)-1]);
		$url = "./shared/home_upload/".uniqid(rand()).'.'.$type;
		if(in_array($type, array("jpg", "jpeg", "png")))
			if(is_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"]))
				if(move_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"],$url))
					return $url;
		return "";
	}
}
