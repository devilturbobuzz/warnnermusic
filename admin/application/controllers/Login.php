<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->library('session');
		$this->load->model('login_m');
		$this->load->helper('url');
		$this->load->view('head');
    }
	public function index()
	{
		$this->load->view('login');
	}
	public function Checklogin(){
		$username= $this->input->post('username');
		$password= $this->input->post('password');
		$data['arr_login'] =$this->login_m->selectdata($username,$password);
		if($data['arr_login']==NULL){
			$data['txt_error']="Please check your username and password";
			$data['txt_user']=$username;
			$this->load->view('login',$data);
		}else{
			 $this->session->set_userdata('username_session', $data['arr_login'][0]->username);
			 redirect("/Dashboard");
		}
		
	}
	public function Logoff()
	{
		$this->session->unset_userdata('username_session');
		$this->load->view('login');
	}
}
