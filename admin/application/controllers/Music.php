<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Music extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->model('music_m');
		$this->load->library('session');
		$this->load->helper("file");
		if(!$this->session->userdata('username_session')){
			redirect("/Login");
		}
		
		$this->load->helper(array('form', 'url'));
    }
	
	public function index()
	{
		$this->load->view('head');
		$this->load->view('_nav');
		$data['arr_data']=$this->music_m->selectdata_music(NULL);
		$this->load->view('music',$data);		
	}
	public function music_video()
	{
		$this->load->view('head');
		$this->load->view('_nav');
		$data['arr_data']=$this->music_m->selectdata_video(NULL);
		$this->load->view('music_video',$data);		
	}
	public function music_video_form($id)
	{
		$this->load->view('head');
		$this->load->view('_nav');
		$data['editdata']=$this->music_m->selectdata_video($id);
		$this->load->view('music_video_form',$data);		
	}
	public function music_audio()
	{
		$this->load->view('head');
		$this->load->view('_nav');
		$data['arr_data']=$this->music_m->selectdata_music_audio(NULL);
		$this->load->view('music_audio',$data);		
	}
	public function music_audio_form($id_music)
	{
		$this->load->view('head');
		$this->load->view('_nav');
		$this->session->set_userdata('music_id', $id_music);
		$data['arr_audio']=$this->music_m->selectdata_audio($id_music);
		$data['editdata']=$this->music_m->selectdata_music($id_music);
		$data['arr_mood'] =$this->music_m->selectdata_mood(NULL);
	   //$this->output->enable_profiler(TRUE);
		$this->load->view('music_audio_form',$data);		
	}
	
	public function Form_video()
	{	
		$this->load->view('head');
		$this->load->view('_nav');
		$this->load->view('music_video_form');		
	
	}
	
	public function Form()
	{	
		$this->load->view('head');
		$this->load->view('_nav');
		$data['arr_data_album'] =$this->music_m->selectdata_album(NULL);
		$data['arr_data_artist'] =$this->music_m->selectdata(NULL);
		$this->load->view('music_form',$data);
		
			
	}
	
	public function Form_save()
	{
		$id=$this->input->post('id');
		$data=array();
		$data['music_type']= $this->input->post('music_type');
		$data['artist_id']= $this->input->post('artist_id');
		$data['album_id']= $this->input->post('album_id');
		//$data['Remark']= $this->input->post('Remark');
		$data['updatedatetime']= date("Y-m-d h:i:s");
	//	$data['tags']= $this->input->post('tags');
		if($id==""){
			$data['createdatetime']= date("Y-m-d h:i:s");
		}
		$data['staff']=$this->session->userdata('username_session');
		$this->music_m->save($data,"music",$id,"id");
		if($id==""){
		$id=$this->music_m->select_max_music();
	    $id=$id[0]->idmusic;
		}
	
		redirect("/Music/music_audio_form/".$id);
		
	}
	public function Form_save_video()
	{
		$id=$this->input->post('id');
		$data=array();
		$data['subject']= $this->input->post('subject');
		//$data['tags']= $this->input->post('tags');
		$data['description']= $this->input->post('description');
		
		$date_stamp=str_replace("/","-",$this->input->post('released'));
		
		
		$data['released']=date('Y-m-d',strtotime($date_stamp));
		$data['youtubeid']= $this->input->post('youtubeid');
		$data['updatedatetime']= date("Y-m-d h:i:s");
	
		if($id==""){
			$data['createdatetime']= date("Y-m-d h:i:s");
		}
		$data['staff']=$this->session->userdata('username_session');
		$this->music_m->save($data,"music_video",$id,"id");
		redirect("/Music/music_video");

	}
	public function Edit($id)
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['arr_data_album'] =$this->music_m->selectdata_album(NULL);
		$data['arr_data_artist'] =$this->music_m->selectdata(NULL);
		$data['editdata']=$this->music_m->selectdata_music($id);
		//$data['tags']= $this->input->post('tags');
		//$this->output->enable_profiler(TRUE);
		$this->load->view('music_form',$data);	
	}
	
	public function update_mood_music(){
		$music_name= $this->input->post('music_name');
		$music_id= $this->session->userdata('music_id');
		$data['mood_id']= $this->input->post('mood_id');
		$data['updatedatetime']= date("Y-m-d h:i:s");
		//$data['tags']= $this->input->post('tags');
		$data['staff']=$this->session->userdata('username_session');
		$this->music_m->updatemusic_mood($data,"music_audio",$music_name,$music_id);
		//$this->output->enable_profiler(TRUE);
		
	}
	
	public function update_thumnail_music(){
		 $music_name= $this->input->post('music_name');
		$music_id= $this->session->userdata('music_id');
		$id= $this->input->post('id');
		$data['thumnail_path']=$this->do_upload_thumnail();
		$data['staff']=$this->session->userdata('username_session');
		$data['updatedatetime']= date("Y-m-d h:i:s");
		$this->music_m->updatemusic_thumnail($data,"music_audio",$music_name,$music_id);
		//$this->output->enable_profiler(TRUE);
		
	}
	
	public function update_url_music($id){
		$id= $this->input->post('id');
		$data['staff']=$this->session->userdata('username_session');
		$data['url_subject']= $this->input->post('url_subject');
		$data['url_download']= $this->input->post('url_download');
		$data['updatedatetime']= date("Y-m-d h:i:s");
		$this->music_m->save($data,"music_audio",$id,"id");
	}
		
	public function update_link()
	{
		$id= $this->input->post('id');
		$fields= $this->input->post('type');
		$getvalue= $this->input->post('getvalue');
		$data[$fields]=$getvalue;
		$this->music_m->updatemusic_link($data,$id);
		$this->output->enable_profiler(TRUE);
			
	}	
		
	public function form_delete($id)
	{
		 $this->music_m->deletedata($id);
		 redirect("/Music/music_audio");
			
	}
	public function form_delete_video($id)
	{
		 $this->music_m->deletedata_video($id);
		 redirect("/Music/music_video");
			
	}
	public function delete_music()
	{
		$music_name= $this->input->post('music_name');
		$path= $this->input->post('path');
		$music_id= $this->session->userdata('music_id');
		$this->music_m->deletedata_music(base64_decode($music_name),$path,$music_id);
	    @unlink($path);
			
	}
	private function do_upload_thumnail()
	{
		$path = './shared/audio_upload/'.$this->session->userdata('music_id').'/thumnail';
		if (!is_dir($path)) { 
			  mkdir($path, 0777, TRUE); 
		}
		
		$type = explode('.', $_FILES["files_thumnail"]["name"]);
		$type = strtolower($type[count($type)-1]);
		$url = $path."/".uniqid(rand()).'.'.$type;
		if(in_array($type, array("jpg", "jpeg", "png")))
			if(is_uploaded_file($_FILES["files_thumnail"]["tmp_name"]))
				if(move_uploaded_file($_FILES["files_thumnail"]["tmp_name"],$url))
					return $url;
		return "";
	}
	public function do_upload(){
    
		
		
        // Detect form submission.
        if($this->input->post('submit')){
        
            $path = './shared/audio_upload/'.$this->session->userdata('music_id').'/music';
			$this->load->library('upload');
          
				if (!is_dir($path)) { 
						mkdir($path, 0777, TRUE); 
			}
          
            $this->upload->initialize(array(
				
                "upload_path"       =>  $path,
                "allowed_types"     =>  "*",
                "max_size"          =>  '256000'
             
            ));
            
            if($this->upload->do_multi_upload("uploadfile")){
                
                $data['upload_data'] = $this->upload->get_multi_upload_data();
            	$data['arr_mood'] =$this->music_m->selectdata_mood(NULL);
				
				
				for($i=0;$i<count($data['upload_data']);$i++){ ?>
				 <?
				
					$data_savemusic['music_id']=$this->session->userdata('music_id');
					$data_savemusic['music_name']=base64_decode($data['upload_data'][$i]['raw_name']);
					$path_music=$path."/".$data['upload_data'][$i]['file_name'];
					$data_savemusic['music_path']=$path_music;
					$data_savemusic['updatedatetime']= date("Y-m-d h:i:s");
					$data_savemusic['createdatetime']= date("Y-m-d h:i:s");
					$this->music_m->save($data_savemusic,"music_audio","","id");
					$data['staff']=$this->session->userdata('username_session');
					$id_audio=$this->music_m->select_max_audio();
					$id_audio=$id_audio[0]->id;
					

				?>
					 
					  <div class="box_music" >
						<!--   <div class="form-group row " >
							   <div class="col-md-2">
												 <label class="form-control-label" for="l0">Music Detail</label>
													
									 </div>

								 
									  <div class="col-md-4">
										<audio controls >
											<source src="<?=base_url()."shared/audio_upload/".$this->session->userdata('music_id')."/music/".$data['upload_data'][$i]['file_name']?>" type="audio/mpeg">
									</audio>
							 			 </audio>
									  </audio>
									 </div>	
									  <div class="col-md-2">
											
												<label class="form-control-label" for="l0"><?=base64_decode($data['upload_data'][$i]['raw_name'])?></label>
									 </div> 	 
						</div>-->
						
						<div class="form-group row " >
							 <div class="col-md-2">
											<label class="form-control-label" for="l0">Music </label>
												
									 </div>
									  <div class="col-md-8">
									    <div class="mediatec-cleanaudioplayer">
                                                        <ul data-theme="default">
                                                            <li data-title="<?=base64_decode($data['upload_data'][$i]['raw_name'])?>"  data-type="mp3" data-url="<?=base_url()."shared/audio_upload/".$this->session->userdata('music_id')."/music/".$data['upload_data'][$i]['file_name']?>" data-free="true"></li>
                                                        </ul>
                                                    </div>										
									 </div>	
									 	 
						</div>
						
						<ul class="url_download<?=$id_audio?>" style=" list-style: none;margin-left: 52px;" >
                            <? if(isset($editdata[0]->url_subject)){  
								$arr_url_subject=explode(",",$editdata[0]->url_subject);
								$arr_url_download=explode(",",$editdata[0]->url_download);
								for($i=0;$i<count($arr_url_subject);$i++){ ?>
								<li class="li"><?=$i==0?"Download":""?> &nbsp;&nbsp;&nbsp;  <input type="text" style="display: inline-block; width: 250px; <?=$i==0?"":"margin-left: 70px;"?>"  class="form-control" placeholder="subject" name="url_subject<?=$id_audio?>[]" value="<?=$arr_url_subject[$i]?>">&nbsp;&nbsp;&nbsp;   <input type="text" style="    display: inline-block;width: 500px; "  class="form-control" placeholder="link download" name="url_download[]" value="<?=$arr_url_download[$i]?>">&nbsp;&nbsp;&nbsp; <button type="button" onClick="<?=$i==0?"btn_url('".$id_audio."')":"$(this).closest('.li').remove()"?>" class="btn btn-icon <?=$i==0?"btn-info":"btn-danger"?> btn-info btn-rounded margin-inline">
                             		
                              		
                               		<i class="fa  <?=$i==0?"fa-plus":"fa-minus"?>" aria-hidden="true"></i>
                               </button></li>
							<?		
								}
							?>
									
							<?	}else{ ?>
								<li class="li">Download &nbsp;&nbsp;&nbsp;  <input type="text" style="display: inline-block; width: 200px;"  class="form-control" placeholder="subject" name="url_subject<?=$id_audio?>[]" value="<?=isset($editdata[0]->url_subject)?$editdata[0]->url_subject:""?>">&nbsp;&nbsp;&nbsp;   <input type="text" style="    display: inline-block;width: 500px; "  class="form-control" placeholder="link download" name="url_download<?=$id_audio?>[]" value="<?=isset($editdata[0]->url_download)?$editdata[0]->url_download:""?>">&nbsp;&nbsp;&nbsp; <button type="button" onClick="btn_url('<?=$id_audio?>')" class="btn btn-icon btn-info btn-rounded margin-inline">
                               		<i class="fa fa-plus" aria-hidden="true"></i>
                               </button></li>
                            <?	} ?>
                            </ul>
                             <footer class="panel-footer text-right bg-light lter ">
                             
                        <button type="button" onClick="delete_music('<?=base64_encode($data['upload_data'][$i]['raw_name'])?>','<?=$path_music?>')" class="btn btn-danger btn-s-xs">Delete</button>
                       
                        
                    </footer>
				    </div>
					
				<? }
				
		
                             
            } else {    
                // Output the errors
                $errors = array('error' => $this->upload->display_errors('<p class = "bg-danger">', '</p>'));               
            
                foreach($errors as $k => $error){
                    echo $error;
                }
                
            }
            
        } else {
            echo '<p class = "bg-danger">An error occured, please try again later.</p>';
            
        }
        // Exit to avoid further execution
        exit();
    }

}
