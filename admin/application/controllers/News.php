<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->model('news_m');
		$this->load->library('session');
		if(!$this->session->userdata('username_session')){
			redirect("/Login");
		}

    }
	public function index()
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['newsdata']=$this->news_m->selectdata(NULL);
		$this->load->view('news',$data);
		
			
	}
	public function search(){
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['val_search']=$this->input->post('keyword');
		$data['newsdata']=$this->news_m->selectdata_search($this->input->post('keyword'));
		$this->load->view('news',$data);
	}
	public function Form()
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['editdata']=$this->news_m->get_form();
		$this->load->view('news_form',$data);
		
			
	}
	public function Edit($id)
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['editdata']=$this->news_m->selectdata($id);
		$this->load->view('news_form',$data);	
	}
	
	public function Form_save()
	{
		
		if($_FILES["fileToUpload_thumnail"]["name"]){
			$url_picture = $this->do_upload1();
			$arr_setname_thumnail=explode("/",$url_picture);
			$url_thumnail = $arr_setname_thumnail[0]."/".$arr_setname_thumnail[1]."/".$arr_setname_thumnail[2]."/thumbnails_".$arr_setname_thumnail[3];
	    }
		if($_FILES["fileToUpload_highlight"]["name"]){
			$url_highlight = $this->do_upload2();
		}
		
		$id=$this->input->post('id');
		$date_stamp=str_replace("/","-",$this->input->post('date_stamp'));
		$data=array();
		$data['date_news']= date('Y-m-d',strtotime($date_stamp));
		$data['subject_news']=$this->input->post('Subject');
		$data['description_news']=$this->input->post('Description');
		if($_FILES["fileToUpload_thumnail"]["name"]){
			$data['thumnail_path']=$url_thumnail;
			$data['head_picture_path']=$url_picture;
		}
		if($_FILES["fileToUpload_highlight"]["name"]){
			$data['picture_highlight']=$url_highlight;
		}
		if($id==""){
			$data['createdatetime']=date("Y-m-d h:i:s");
		}else{
			
		}
		$data['staff']=$this->session->userdata('username_session');
		$data['updatedatetime']=date("Y-m-d h:i:s");
		$data['tags']= $this->input->post('tags');
		$this->news_m->save($data,$id);
		if($id==""){
			$id=$this->news_m->select_max_news();
			$id=$id[0]->id;
		}
		$this->news_m->deletedata_tags($id);
		$data_tags=array();
		$data_tags['tags_module']="news";
		$data_tags['staff']=$this->session->userdata('username_session');;
		$data_tags['id_module']=$id;
		$data_tags['updatedatetime']=date("Y-m-d h:i:s");
		$data_tags['createdatetime']=date("Y-m-d h:i:s");
		if(strpos($this->input->post('tags'),",")>0){
			$arr_tags=explode(",",$this->input->post('tags'));
			for($i=0;$i<count($arr_tags);$i++){
				$data_tags['tags_index']=$arr_tags[$i];
				if($arr_tags[$i]){
					$this->news_m->save_tags($data_tags,$id);
				}
			}	
		}else{
			$data_tags['tags_index']=$this->input->post('tags');
			if($this->input->post('tags')){
					$this->news_m->save_tags($data_tags,$id);
			}
			
		}
		//$this->output->enable_profiler(TRUE);
		
		redirect("/News");

	}
	private function do_upload1()
	{
		$type = explode('.', $_FILES["fileToUpload_thumnail"]["name"]);
		$type = strtolower($type[count($type)-1]);
		$name_file=uniqid(rand()).'.'.$type;
		$url = "./shared/news_upload/".$name_file;
		$images = $_FILES["fileToUpload_thumnail"]["tmp_name"];
		$new_images = "thumbnails_".$name_file;
		$width=320;
		$height=224;
		$size=GetimageSize($images);

		 switch ($size['mime']) {
					  case "image/gif":
						 $images_orig = imagecreatefromgif($images);
						 break;
					  case "image/jpeg":
						 $images_orig = imagecreatefromjpeg($images);
						 break;
					  case "image/png":
						 $images_orig = imagecreatefrompng($images);
						 break;
				   }

		$photoX = ImagesX($images_orig);
		$photoY = ImagesY($images_orig);
		$images_fin = ImageCreateTrueColor($width, $height);
		ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
		ImageJPEG($images_fin,"./shared/news_upload/".$new_images);
		ImageDestroy($images_orig);
		ImageDestroy($images_fin);
		
		if(in_array($type, array("jpg", "jpeg", "png")))
			if(is_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"]))
				if(move_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"],$url))
					return $url;
		return "";
	}
	private function do_upload2()
	{
		$type = explode('.', $_FILES["fileToUpload_highlight"]["name"]);
		$type = strtolower($type[count($type)-1]);
		$url = "./shared/news_upload/".uniqid(rand()).'.'.$type;
		if(in_array($type, array("jpg", "jpeg", "png")))
			if(is_uploaded_file($_FILES["fileToUpload_highlight"]["tmp_name"]))
				if(move_uploaded_file($_FILES["fileToUpload_highlight"]["tmp_name"],$url))
					return $url;
		return "";
	}
	public function save_approve($id,$approve)
	{
		$data = array(
			'table_name' => 'news',
			'id' => $id,
			'approve' => $approve);
		$this->news_m->upddata($data);
		if($this->news_m->upddata($data)){
				redirect("/News");
		}else{
			
		}
		
			
	}
	public function save_highlight($id,$highlight)
	{
		
		$data = array(
			'table_name' => 'news',
			'id' => $id,
			'highlight_news' => $highlight);
		
		$this->news_m->upddata_highlight($data);
		if($this->news_m->upddata_highlight($data)){
				redirect("/News");
		}else{
			
		}
		
			
	}
	public function form_delete($id)
	{
		 $this->news_m->deletedata($id);
		 redirect("/News");
			
	}
	
}
