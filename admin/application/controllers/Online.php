<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Online extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->model('Online_m');
		$this->load->library('session');
		if(!$this->session->userdata('username_session')){
			redirect("/Login");
		}

    }
	public function index()
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['arr_data']=$this->Online_m->selectdata(NULL);
		$this->load->view('online_ch',$data);
		
			
	}
	public function Form()
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$this->load->view('online_ch_form');
		
			
	}
	public function Edit($id)
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['editdata']=$this->Online_m->selectdata($id);
		$this->load->view('online_ch_form',$data);	
	}
	
	public function Form_save()
	{
		$date_stamp=str_replace("/","-",$this->input->post('Release_on'));
		$data=array();
		$data['Release_on']= date('Y-m-d',strtotime($date_stamp));
		if($_FILES["fileToUpload_thumnail"]["name"]){
			$url_picture = $this->do_upload();
		}
		$id=$this->input->post('id');
		$data['name']=$this->input->post('name');
		if($id==""){
			$data['createdatetime']=date("Y-m-d h:i:s");
			
		}
		if($_FILES["fileToUpload_thumnail"]["name"]){
			$data['thumnail_path']=$url_picture;
		}
		
		$data['staff']=$this->session->userdata('username_session');
		$data['updatedatetime']=date("Y-m-d h:i:s");
		$this->Online_m->save($data,$id);
		redirect("/Online");

	}

	
	public function form_delete($id)
	{
		$data=$this->Online_m->selectdata($id);
		@unlink($data[0]->thumnail_path);
		$this->Online_m->deletedata($id);
		redirect("/Online");	
	}
	private function do_upload()
	{
		$type = explode('.', $_FILES["fileToUpload_thumnail"]["name"]);
		$type = strtolower($type[count($type)-1]);
		$url = "./shared/online/".uniqid(rand()).'.'.$type;
		if(in_array($type, array("jpg", "jpeg", "png")))
			if(is_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"]))
				if(move_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"],$url))
					return $url;
		return "";
	}
}
