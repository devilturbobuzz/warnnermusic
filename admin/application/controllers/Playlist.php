<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Playlist extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->model('playlist_m');
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		if(!$this->session->userdata('username_session')){
			redirect("/Login");
		}

    }
	public function search(){
		
		$data['val_search']=$this->input->post('keyword');
		$data['arr_data']=$this->playlist_m->selectdata_search($this->input->post('keyword'));
		$this->load->view('playlist',$data);
	}
	public function index()
	{	
		$data['arr_data']=$this->playlist_m->selectdata(NULL);
		$genres_name_str=array();
		foreach ($this->playlist_m->selectdata(NULL) as $item) {
			$genres_name="";

			foreach ($this->playlist_m->selectdata_genres_name($item->id_playlist_cat) as $items) {
				$genres_name.=$items->subject.",";
			}

				array_push($genres_name_str, rtrim($genres_name,","));
		}
		$data['arr_playlist']=$genres_name_str;
		$this->load->view('playlist',$data);	
	}
	public function theme()
	{
		
		$data['arr_data']=$this->playlist_m->selectdata_playlist_cat(NULL);
		$this->load->view('playlist_mood',$data);	
	}
	public function themeForm()
	{
		
		$this->load->view('playlist_mood_form');	
	}


	public function form_delete_mood($id,$id_update)
	{
		 $data=array();
		 $data['id_playlist_cat']= $id_update;
		 $this->artist_m->save($data,"playlist_cat",$id,"Genres");
		 $this->artist_m->deletedata_genres($id);
		 redirect("/Playlist/Playlist_mood");
			
	}

	public function Form()
	{
		
		$data['arr_artist']=$this->playlist_m->selectdata_artist(NULL);
		$data['arr_playlist']=$this->playlist_m->selectdata_playlist_cat_all();
		
		$data['arr_gallery']=$this->playlist_m->selectdata_gallery(NULL);
		$this->load->view('playlist_form',$data);	
	}
	
	public function getmusic(){
		$artisti_id=$this->input->post('artisti_id');
		$music_artist=$this->playlist_m->selectdata_music($artisti_id);
		 echo json_encode($music_artist);
	}
	public function Edit($id)
	{
		
		$data['arr_playlist']=$this->playlist_m->selectdata_playlist_cat_all();
		$data['arr_artist']=$this->playlist_m->selectdata_artist(NULL);
		$data['arr_gallery']=$this->playlist_m->selectdata_gallery(NULL);
		$data['editdata']=$this->playlist_m->selectdata($id);
		$this->load->view('playlist_form',$data);	
	}
	public function edit_theme($id)
	{
		
		$data['editdata']=$this->playlist_m->selectdata_playlist_cat($id);
	
		$this->load->view('playlist_mood_form',$data);
	}
	public function Form_save_theme()
	{
		$id=$this->input->post('id');
		
		if($_FILES["fileToUpload_thumnail"]["name"]){
			$url_picture = $this->do_upload2();
			$arr_setname_thumnail=explode("/",$url_picture);
			$url_thumnail = $arr_setname_thumnail[0]."/".$arr_setname_thumnail[1]."/".$arr_setname_thumnail[2]."/thumbnails_".$arr_setname_thumnail[3];
	    }
	    $data=array();
		$data['subject']=$this->input->post('subject');
		
		if($_FILES["fileToUpload_thumnail"]["name"]){
			$data['thumnail_path']=$url_thumnail;
			$data['head_picture_path']=$url_picture;
		}
		
		if($id==""){
			$data['createdatetime']=date("Y-m-d h:i:s");
		}
		//$data['tags']= $this->input->post('tags');
		$data['updatedatetime']=date("Y-m-d h:i:s");
		$this->playlist_m->save($data,"playlist_cat",$id,"id");
		if($id==""){
			$id=$this->playlist_m->select_max_playlist_cat();
			$id=$id[0]->id;
		}
		
		$id_playlist_sub = $this->input->post('id_playlist_sub');
		$playlist=$this->input->post('playlist');
		
		for($i=0;$i<count($playlist);$i++){
			  $data2 = array(
			  'id_playlist' =>$playlist[$i],
			  'id_playlist_cat' => $id
			 );
			
		
		if($playlist[$i]){	
			
			if($id_playlist_sub[$i]){
				
				$this->playlist_m->save($data2,"playlist_sub_cat",$id_playlist_sub[$i],"id"); 
				}else{
					$this->playlist_m->save($data2,"playlist_sub_cat","","id"); 
				
				}
				
			}
		}	
		   redirect("/Playlist/edit_theme/".$id);
		}
	
		
	
	
	public function Form_save()
	{
		if($_FILES["fileToUpload_thumnail"]["name"]){
			$url_picture = $this->do_upload1();
			$arr_setname_thumnail=explode("/",$url_picture);
			$url_thumnail = $arr_setname_thumnail[0]."/".$arr_setname_thumnail[1]."/".$arr_setname_thumnail[2]."/thumbnails_".$arr_setname_thumnail[3];
	    }
		$arr_genre_mood=$this->input->post('id_playlist_cat');
		$str_genre="";
		for($i=0;$i<count($arr_genre_mood);$i++){
			$str_genre.=$arr_genre_mood[$i].",";
		}
		
		$id=$this->input->post('id');
		$id_tags=$id;
		$date_stamp=str_replace("/","-",$this->input->post('date_stamp'));
        $data=array();
		$data['id_playlist_cat']= rtrim($str_genre,",");
		
		$data['release_on']= date('Y-m-d',strtotime($date_stamp));
		$data['Subject']=$this->input->post('Subject');
		$data['Description']=$this->input->post('Description');
		
		if(count($this->input->post('url_subject'))>0){
			$url_subject="";
			foreach ($this->input->post('url_subject') as $key => $value) {
				$url_subject.=$value.",";
			}
			$data['url_subject']= rtrim($url_subject,",");
			
		}
		if(count($this->input->post('url_download'))>0){
			$url_download="";
			foreach ($this->input->post('url_download') as $key => $value) {
				$url_download.=$value.",";
			}
			$data['url_download']= rtrim($url_download,",");
			
		}
		if($_FILES["fileToUpload_thumnail"]["name"]){
			$data['thumnail_path']=$url_thumnail;
			$data['head_picture_path']=$url_picture;
		}
		
		if($id==""){
			$data['createdatetime']=date("Y-m-d h:i:s");
		}
		$data['tags']= $this->input->post('tags');
		$data['updatedatetime']=date("Y-m-d h:i:s");
		$data['music_description']= $this->input->post('music_description');
		$this->playlist_m->save($data,"playlist",$id,"id");
		$id_playlist=$this->playlist_m->select_max_playlist();
		$id_playlist=$id_playlist[0]->id;
		if($id){
			$id_playlist=$id;
		}
		if($id_tags==""){
			$id_tags=$id_playlist;
		}
		/***** tags module ***/
		$this->playlist_m->deletedata_tags($id_tags);
		$data_tags=array();
		$data_tags['tags_module']="playlist";
		$data_tags['staff']=$this->session->userdata('username_session');;
		$data_tags['id_module']=$id_tags;
		$data_tags['updatedatetime']=date("Y-m-d h:i:s");
		$data_tags['createdatetime']=date("Y-m-d h:i:s");
		if(strpos($this->input->post('tags'),",")>0){
			$arr_tags=explode(",",$this->input->post('tags'));
			for($i=0;$i<count($arr_tags);$i++){
				$data_tags['tags_index']=$arr_tags[$i];
				if($arr_tags[$i]){
					$this->playlist_m->save_tags($data_tags,$id_tags);
				}
			}	
		}else{
			$data_tags['tags_index']=$this->input->post('tags');
			if($this->input->post('tags')){
				$this->playlist_m->save_tags($data_tags,$id_tags);
			}
				
		}
		
		/**** tags module ***/
		
		
		/*
		$music_id= $this->input->post('music');
		$artist_id = $this->input->post('artist');
		$id_music_sub = $this->input->post('id_playlist');
		$id_playlist_sub = $this->input->post('id_playlist_sub');
		
		for($i=0;$i<count($music_id);$i++){
			  $data2 = array(
			  'playlist_id' =>$id_playlist,
			  'music_id' => $music_id[$i],
			  'artist_id' =>$artist_id[$i],
			  'id'=>$id_playlist_sub[$i],
			  'createdatetime' => date("Y-m-d h:i:s"),
			  'updatedatetime' => date("Y-m-d h:i:s")
			  );
			
			if($artist_id[$i]){
				if($id_playlist_sub[$i]){
					$this->playlist_m->save($data2,"playlist_sub",$id_playlist_sub[$i],"id"); 
					//$this->output->enable_profiler(TRUE);
				}else{
					$this->playlist_m->save($data2,"playlist_sub","","id"); 
				}
				
			}
		}*/
		
		//$this->output->enable_profiler(TRUE);
		redirect("/Playlist");

	}
	private function do_upload1()
	{
		$type = explode('.', $_FILES["fileToUpload_thumnail"]["name"]);
		$type = strtolower($type[count($type)-1]);
		$name_file=uniqid(rand()).'.'.$type;
		$url = "./shared/playlist_upload/".$name_file;
		$images = $_FILES["fileToUpload_thumnail"]["tmp_name"];
		$new_images = "thumbnails_".$name_file;
		$width=320;
		$height=224;
		$size=GetimageSize($images);

		 switch ($size['mime']) {
					  case "image/gif":
						 $images_orig = imagecreatefromgif($images);
						 break;
					  case "image/jpeg":
						 $images_orig = imagecreatefromjpeg($images);
						 break;
					  case "image/png":
						 $images_orig = imagecreatefrompng($images);
						 break;
				   }

		$photoX = ImagesX($images_orig);
		$photoY = ImagesY($images_orig);
		$images_fin = ImageCreateTrueColor($width, $height);
		ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
		ImageJPEG($images_fin,"./shared/playlist_upload/".$new_images);
		ImageDestroy($images_orig);
		ImageDestroy($images_fin);
		
		if(in_array($type, array("jpg", "jpeg", "png")))
			if(is_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"]))
				if(move_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"],$url))
					return $url;
		return "";
	}
	private function do_upload2()
	{
		$type = explode('.', $_FILES["fileToUpload_thumnail"]["name"]);
		$type = strtolower($type[count($type)-1]);
		$name_file=uniqid(rand()).'.'.$type;
		$url = "./shared/playlist_sub_upload/".$name_file;
		$images = $_FILES["fileToUpload_thumnail"]["tmp_name"];
		$new_images = "thumbnails_".$name_file;
		$width=320;
		$height=224;
		$size=GetimageSize($images);

		 switch ($size['mime']) {
					  case "image/gif":
						 $images_orig = imagecreatefromgif($images);
						 break;
					  case "image/jpeg":
						 $images_orig = imagecreatefromjpeg($images);
						 break;
					  case "image/png":
						 $images_orig = imagecreatefrompng($images);
						 break;
				   }

		$photoX = ImagesX($images_orig);
		$photoY = ImagesY($images_orig);
		$images_fin = ImageCreateTrueColor($width, $height);
		ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
		ImageJPEG($images_fin,"./shared/playlist_sub_upload/".$new_images);
		ImageDestroy($images_orig);
		ImageDestroy($images_fin);
		
		if(in_array($type, array("jpg", "jpeg", "png")))
			if(is_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"]))
				if(move_uploaded_file($_FILES["fileToUpload_thumnail"]["tmp_name"],$url))
					return $url;
		return "";
	}

	
	public function form_delete($id)
	{
		 $this->playlist_m->deletedata($id);
		 redirect("/Playlist");		
	}
	
	public function form_delete_cat($id,$id_update)
	{
		 $data=array();
		
		 $this->playlist_m->update_playlist_cat($id,"playlist",$id_update,"id_playlist_cat");
		 $this->playlist_m->deletedata_cat($id);
		 redirect("/Playlist/theme");		
	}
	
}
