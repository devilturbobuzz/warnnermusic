<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->model('tags_m');
		$this->load->library('session');
		if(!$this->session->userdata('username_session')){
			redirect("/Login");
		}

    }
	public function index()
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['arr_data']=$this->tags_m->selectdata(NULL);
		$this->load->view('tags',$data);
		
			
	}
	public function Form()
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$this->load->view('tags_form');
		
			
	}
	public function Edit($id)
	{
		
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$data['editdata']=$this->tags_m->selectdata($id);
		$this->load->view('tags_form',$data);	
	}
	
	public function Form_save()
	{
		$data=array();
		$id=$this->input->post('id');
		$data['tags_name']=$this->input->post('tags_name');
		$data['tags_module']=$this->input->post('tags_module');
		$data['tags_index']=$this->input->post('tags_index');
		if($id==""){
			$data['createdatetime']=date("Y-m-d h:i:s");
			
		}
		$data['staff']=$this->session->userdata('username_session');
		$data['updatedatetime']=date("Y-m-d h:i:s");
		$this->tags_m->save($data,$id);
		redirect("/Tags");

	}

	
	public function form_delete($id)
	{
		 $this->tags_m->deletedata($id);
		 redirect("/Tags");
			
	}
	
}
