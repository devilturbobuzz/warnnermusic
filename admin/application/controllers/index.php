<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index()
	{
		$this->load->helper('url');
		$this->load->view('head');
		$this->load->view('_nav');
		$this->load->view('home');
		$this->load->view('renderscript');

	}
}
