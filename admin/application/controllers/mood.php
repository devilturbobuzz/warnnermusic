<body class="theme-default">


<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Tables -->
    <section class="panel">
        
        <div class="panel-body">
           
            <div class="row">
                <div class="col-lg-12">
                    <h4>Mood</h4>
                    <p><code>Atrist / Mood</code></p>
                    <br />
                    
                    <div class="table-responsive margin-bottom-50">
                        <table class="table table-hover" style="    font-size: 13px;">
                            <thead>
                                <tr>
                                    <th>Mood</th>
                                    <th>Picture</th>
                                    <th>DateCreate</th>
                                   
                                    <th><a href="/Artist/FormMood" class="btn btn-xs margin-inline" style="margin: 0px;"><i class="fa fa-plus"></i></a></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                       <th>Mood</th>
                                       <th>Picture</th>
                                    <th>DateCreate</th>
                                   
                                   
                                    <th><a href="/Artist/FormMood" class="btn btn-xs margin-inline" style="margin: 0px;"><i class="fa fa-plus"></i></a></th>
                                </tr>
                            </tfoot>
                            <tbody>
                              <?php 
                    			foreach($arr_data as $item)
								{
							  ?>
                                 <tr>
                                 <td><?=$item->mood_description?></td>
								 <td>
								 	 <a href="/<?=$item->thumnail_path?>" target="_blank">
                                 	  	<img src="/<?=$item->thumnail_path?>" width="60" height="60" />
                                 	  </a>
								 </td>
                                 <td><?=$item->createdatetime?></td>
                                 <td>
									   <a href="/Artist/EditMood/<?=$item->mood_id?>" target="_self"> 
									   <span class="fa fa-pencil btn btn-xs "  title="Edit"></span>&nbsp;
									   </a>
										<a href="/Artist/form_delete_mood/<?=$item->mood_id?>" target="_self">
										<span class="fa fa-close btn btn-xs " title="Delete"></span>
										</a>
                                 </td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Basic Tables  -->

</div>

<!-- Page Scripts -->
<script>

    $(function () {

        $("[data-toggle=tooltip]").tooltip();
		$(".music_menu").addClass("left-menu-list-opened").show();
		$("#mood_sub").css("color","#000");
    });

</script>
<!-- End Page Scripts -->
</section>

<div class="main-backdrop"><!-- --></div>

</body>