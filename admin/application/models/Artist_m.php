<?php
 class Artist_m extends CI_Model {     
	 function __construct() 
	{
		parent::__construct();
	}
	
	public function save($data,$table,$id,$wher_colums)
	{
		
		if($id){
			$this->db->update($table, $data, array($wher_colums => $id));
		}else{
			$this->db->insert($table, $data);
		}
	}
	 
	
	public function deletedata($id) {
		$this->db->where('artist_id', $id);
		$this->db->delete('artist');
	}
	public function deletedata_album_artist($id) {
		$this->db->where('artist_id', $id);
		$this->db->delete('album');
	}
	 public function deletedata_album($id) {
		$this->db->where('album_id', $id);
		$this->db->delete('album');
	}
	 public function deletedata_mood($id) {
		$this->db->where('mood_id', $id);
		$this->db->delete('mood');
	}
	 public function deletedata_nationality($id) {
		$this->db->where('id', $id);
		$this->db->delete('nationality');
	}
	 public function deletedata_genres($id) {
		$this->db->where('id', $id);
		$this->db->delete('genres');
	} 
	 
	 public function select_max_artist() {
			 $query = $this->db->select('MAX(a.artist_id) as id')->from('artist a')->get();
           return $query->result();

	} 
	public function deletedata_tags($id) {
		$this->db->where('id_module', $id);
		$this->db->delete('tags_search');
	}
	 public function save_tags($data,$id)
	{
			$this->db->insert('tags_search', $data);	
	} 
	 public function selectdata($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('a.*,(
		SELECT
			n.nationality_name
		FROM
			nationality n
		WHERE
			n.id = a.Nationality
		LIMIT 1
	) AS Nationality,
	(
		SELECT
			g.genres_name
		FROM
			genres g
		WHERE
			g.id = a.Genres
		LIMIT 1
	) AS Genres ,DATE_FORMAT( a.createdatetime, "%d/%m/%Y") as createdatetime')->from("artist a")->order_by("a.artist_name","COLLATE utf8_unicode_ci ASC")->get();
		}else if($id){
			  $query = $this->db->select('a.*,DATE_FORMAT( a.createdatetime, "%d/%m/%Y") as createdatetime')->from('artist a ')->where('a.artist_id', $id)->get();
			 
		 }
          return $query->result();
	}
	 
	public function selectdata_search($keyword) {
		
		if($keyword){
			$keyword=" WHERE a.artist_name LIKE '%".$keyword."%'";
		}
		
		  $query = $this->db->select('a.*,(
		SELECT
			n.nationality_name
		FROM
			nationality n
		WHERE
			n.id = a.Nationality
		LIMIT 1
	) AS Nationality,
	(
		SELECT
			g.genres_name
		FROM
			genres g
		WHERE
			g.id = a.Genres
		LIMIT 1
	) AS Genres ,DATE_FORMAT( a.createdatetime, "%d/%m/%Y") as createdatetime')->from('artist a '.$keyword)->order_by("a.artist_name","desc")->get();
		
          return $query->result();

	} 
	 public function selectdata_album_search($keyword) {
		if($keyword){
			$keyword=" WHERE (a.artist_name LIKE '%".$keyword."%' OR b.album_name LIKE '%".$keyword."%')";
		}
		  $query = $this->db->select('b.*,DATE_FORMAT( b.createdatetime, "%d/%m/%Y") as createdatetime, a.artist_name,DATE_FORMAT( b.Release_on, "%d/%m/%Y" ) as Release_on')->from('album  b , artist a'.$keyword)->order_by("createdatetime","desc")->get();
          return $query->result();

	} 
	 
	  public function selectdata_gallery($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('gallery')->order_by("name","desc")->get();
		}else if($id){
			  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('gallery')->where('id', $id)->get();
			 
		 }
          return $query->result();

	}
	 public function selectdata_online_ch($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('online_ch')->order_by("name","desc")->get();
		}else if($id){
			  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('online_ch')->where('id', $id)->get();
			 
		 }
          return $query->result();

	} 
	  public function selectdata_album($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('b.*,DATE_FORMAT( b.createdatetime, "%d/%m/%Y") as createdatetime,(select a.artist_name from artist a where a.artist_id=b.artist_id LIMIT 1) as artist_name,DATE_FORMAT( b.Release_on, "%d/%m/%Y" ) as Release_on')->from('album b')->order_by("createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('b.*,DATE_FORMAT( b.createdatetime, "%d/%m/%Y") as createdatetime,(select a.artist_name from artist a where a.artist_id=b.artist_id LIMIT 1) as artist_name,DATE_FORMAT( b.Release_on, "%d/%m/%Y" ) as Release_on')->from('album b')->where('album_id', $id)->get();
			 
		 }
          return $query->result();

	}
	  public function selectdata_mood($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('mood')->order_by("createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('mood')->where('mood_id', $id)->get();
			 
		 }
          return $query->result();

	}
	  public function selectdata_nationality($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('*,DATE_FORMAT( Release_on, "%d/%m/%Y") as Release_on')->from('nationality')->order_by("nationality_name","desc")->get();
		}else if($id){
			  $query = $this->db->select('*,DATE_FORMAT( Release_on, "%d/%m/%Y") as Release_on')->from('nationality')->where('id', $id)->get();
			 
		 }
          return $query->result();

	}
	 public function selectdata_genres($id) {
		 if($id==NULL){
		  $query = $this->db->select('*,DATE_FORMAT( Release_on, "%d/%m/%Y") as Release_on')->from('genres')->order_by("Release_on","desc")->get();
		}else if($id){
			  $query = $this->db->select('*,DATE_FORMAT( Release_on, "%d/%m/%Y") as Release_on')->from('genres')->where('id', $id)->get();
			 
		 }
          return $query->result();

	} 

	
 }

?>