<?php
 class gallery_m extends CI_Model {     
	 function __construct() 
	{
		parent::__construct();
	}
	
	public function save($data,$id)
	{
		if($id){
			$this->db->update('gallery', $data, array('id' => $id));
		}else{
			$this->db->insert('gallery', $data);
		}
		
	}
	public function deletedata($id) {
		$this->db->where('id', $id);
		$this->db->delete('gallery');
	}
	 public function selectdata($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('*,DATE_FORMAT( Release_on, "%d/%m/%Y") as Release_on')->from('gallery')->order_by("createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('*,DATE_FORMAT( Release_on, "%d/%m/%Y") as Release_on')->from('gallery')->where('id', $id)->get();
			 
		 }
          return $query->result();

	}
	
	
 }

?>