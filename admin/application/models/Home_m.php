<?php
 class Home_m extends CI_Model {     
	 function __construct() 
	{
		parent::__construct();
	}
	
	public function save($data,$table,$id,$wher_colums)
	{
		if($id){
			$this->db->update($table, $data, array($wher_colums => $id));
		}else{
			$this->db->insert($table, $data);
		}
	}
	 
	
	public function deletedata($id) {
		$this->db->where('id', $id);
		$this->db->delete('home');
	}
	
	
	  public function selectdata($id) {
		 if($id==NULL){
		  $query = $this->db->select('b.*,DATE_FORMAT( b.released, "%d/%m/%Y") as released,DATE_FORMAT( b.createdatetime, "%d/%m/%Y") as createdatetime')->from('home b')->order_by("released","desc")->get();
		}else if($id){
			  $query = $this->db->select('b.*,DATE_FORMAT( b.released, "%d/%m/%Y") as released ,DATE_FORMAT( b.released, "%d/%m/%Y") as createdatetime')->from('home b')->where('id', $id)->get(); 
		 }
          return $query->result();
	}
	  public function selectdata_artist() {
		  $query = $this->db->select('a.*')->from('artist a')->order_by("artist_name","desc")->get();
          return $query->result();
	}


	 public function selectdata_artist_of_month_search($keyword) {
		if($keyword){
			$keyword=" AND (a.artist_name LIKE '%".$keyword."%' OR b.album_name LIKE '%".$keyword."%')";
		}
		  $query = $this->db->select('b.*,DATE_FORMAT( b.createdatetime, "%d/%m/%Y") as createdatetime,a.artist_name,a.thumnail_path as thumnail_path_artist')->from('album b ')->join('artist a', 'a.artist_id=b.artist_id ')->where('1=1 '.$keyword)->get();
          return $query->result();
	} 
	  public function selectdata_album($id) {
		 if($id==NULL){
		  $query = $this->db->select('b.*,DATE_FORMAT( b.createdatetime, "%d/%m/%Y") as createdatetime,a.artist_name,a.thumnail_path as thumnail_path_artist')->from('album b')->join('artist a', 'a.artist_id=b.artist_id')->where('artist_month', 'Y')->order_by("createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('b.*,DATE_FORMAT( b.createdatetime, "%d/%m/%Y") as createdatetime,(select a.artist_name from artist a where a.artist_id=b.artist_id LIMIT 1) as artist_name')->from('album b')->where('album_id', $id)->get();
		 }
          return $query->result();
	}
 }
?>