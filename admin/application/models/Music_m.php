<?php
 class Music_m extends CI_Model {     
	 function __construct() 
	{
		parent::__construct();
	}
	
	public function save($data,$table,$id,$wher_colums)
	{
		
		if($id){
			$this->db->update($table, $data, array($wher_colums => $id));
		}else{
			$this->db->insert($table, $data);
		}
	}
	 
	public function updatemusic_mood($data,$table,$music_name,$music_id)
	{
		
		$this->db->update($table, $data, array("music_name" => $music_name,"music_id" => $music_id));
		
	}
	public function updatemusic_thumnail($data,$table,$music_name,$music_id)
	{
		
		$this->db->update($table, $data, array("music_name" => $music_name,"music_id" => $music_id));
		
	}
	public function updatemusic_link($data,$id)
	{
		
		$this->db->update('music_audio', $data, array("id" => $id));
		
	} 
	 
	
	public function deletedata($id) {
		$this->db->where('id', $id);
		$this->db->delete('music');
	}
	 public function deletedata_video($id) {
		$this->db->where('id', $id);
		$this->db->delete('music_video');
	}
	 
	 
	 
	public function deletedata_music($id,$path,$music_id) { //,$path
		$this->db->where('music_name', $id);
		$this->db->where('music_path', $path);
		$this->db->where('music_id', $music_id);
		$this->db->delete('music_audio');
	} 
	
	

	 public function selectdata($id) {
		 if($id==NULL){
		  $query = $this->db->select('a.*,DATE_FORMAT( a.createdatetime, "%d/%m/%Y") as createdatetime')->from('artist a')->order_by("createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('a.*,DATE_FORMAT( a.createdatetime, "%d/%m/%Y") as createdatetime')->from('artist a')->where('a.artist_id', $id)->get();
			 
		 }
          return $query->result();

	}
	 
	public function selectdata_video($id) {
		 if($id==NULL){
		  $query = $this->db->select('a.*,DATE_FORMAT( a.createdatetime, "%d/%m/%Y") as createdatetime,DATE_FORMAT( a.released, "%d/%m/%Y") as released')->from('music_video a')->order_by("createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('a.*,DATE_FORMAT( a.createdatetime, "%d/%m/%Y") as createdatetime,DATE_FORMAT( a.released, "%d/%m/%Y") as released')->from('music_video a')->where('a.id', $id)->get();
			 
		 }
          return $query->result();

	} 
	 
	public function select_max_audio() {
			 $query = $this->db->select('MAX(a.id) as id')->from('music_audio a')->get();
           return $query->result();

	} 
	 
	public function select_max_music() {
			 $query = $this->db->select('MAX(a.id) as idmusic')->from('music a')->get();
           return $query->result();

	}  
	public function selectdata_audio($id) {
			 $query = $this->db->select('a.*')->from('music_audio a')->where('a.music_id', $id)->get();
           return $query->result();

	}
	 
	  public function selectdata_album($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('b.*,DATE_FORMAT( b.createdatetime, "%d/%m/%Y") as createdatetime,(select a.artist_name from artist a where a.artist_id=b.artist_id LIMIT 1) as artist_name')->from('album b')->order_by("createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('b.*,DATE_FORMAT( b.createdatetime, "%d/%m/%Y") as createdatetime,(select a.artist_name from artist a where a.artist_id=b.artist_id LIMIT 1) as artist_name')->from('album b')->where('album_id', $id)->get();
			 
		 }
          return $query->result();

	}
	  public function selectdata_mood($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('mood')->order_by("createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('mood')->where('mood_id', $id)->get();
			 
		 }
          return $query->result();

	}
	 public function selectdata_music($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('m.*, m.id as id,
			     a.artist_name,
				 a.Nationality,
				 a.Genres,
				 (SELECT g.genres_name FROM genres g where g.id=a.Genres limit 1 ) as Genres_name,
				 (SELECT n.nationality_name FROM nationality n where n.id=a.Nationality limit 1 ) as nationality_name,
			     b.album_name,DATE_FORMAT( m.createdatetime, "%d/%m/%Y") as createdatetime')->from('music m')->join('artist a', ' m.artist_id = a.artist_id', 'left')->join('album b', ' m.album_id=b.album_id', 'left')->order_by("m.createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('m.*, , m.id as id,
			     a.artist_name,
				 a.Nationality,
				 a.Genres,
				 (SELECT g.genres_name FROM genres g where g.id=a.Genres limit 1 ) as Genres_name,
				 (SELECT n.nationality_name FROM nationality n where n.id=a.Nationality limit 1 ) as nationality_name,
			     b.album_name,DATE_FORMAT( m.createdatetime, "%d/%m/%Y") as createdatetime')->from('music m')->join('artist a', ' m.artist_id = a.artist_id', 'left')->join('album b', ' m.album_id=b.album_id', 'left')->where('m.id', $id)->get();
			 
		 }
          return $query->result();

	}
	 public function selectdata_music_audio($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('m.*, 
			     a.artist_name,
				 a.Nationality,
				 a.Genres,
				 (SELECT g.genres_name FROM genres g where g.id=a.Genres limit 1 ) as Genres_name,
				 (SELECT n.nationality_name FROM nationality n where n.id=a.Nationality limit 1 ) as nationality_name,
				 (select COUNT(x.music_id) From music_audio x where x.music_id=m.id) as track,
			     b.album_name,DATE_FORMAT( m.createdatetime, "%d/%m/%Y") as createdatetime')->from('music m')->join('artist a', ' m.artist_id = a.artist_id', 'left')->join('album b', ' m.album_id=b.album_id', 'left')->where('m.music_type', "Audio")->order_by("m.createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('m.*, 
			     a.artist_name,
				 a.Nationality,
				 a.Genres,
				 (SELECT g.genres_name FROM genres g where g.id=a.Genres limit 1 ) as Genres_name,
				 (SELECT n.nationality_name FROM nationality n where n.id=a.Nationality limit 1 ) as nationality_name,
			     b.album_name,DATE_FORMAT( m.createdatetime, "%d/%m/%Y") as createdatetime')->from('music m')->join('artist a', ' m.artist_id = a.artist_id', 'left')->join('album b', ' m.album_id=b.album_id', 'left')->where('m.id', $id)->where('m.music_type', "Audio")->get();
			 
		 }
          return $query->result();

	}
	
	
 }

?>