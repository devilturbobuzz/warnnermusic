<?php
 class News_m extends CI_Model {     
	 function __construct() 
	{
		parent::__construct();
	}
	
	public function save($data,$id)
	{
		if($id){
			$this->db->update('news', $data, array('id' => $id));
		}else{
			$this->db->insert('news', $data);
		}
		
	}
	
 
	public function save_tags($data,$id)
	{
			$this->db->insert('tags_search', $data);	
	} 
	public function deletedata_tags($id) {
		$this->db->where('id_module', $id);
		$this->db->delete('tags_search');
	}
	public function select_max_news() {
			 $query = $this->db->select('MAX(a.id) as id')->from('news a')->get();
           return $query->result();

	}  
	public function upddata($data) {
		extract($data);
		$this->db->where('id', $id);
		$this->db->update($table_name, array('approve' => $approve));
		
		return true;
	}
	 public function upddata_highlight($data) {
		extract($data);
		$this->db->where('id', $id);
		$this->db->update($table_name, array('highlight_news' => $highlight_news));
		return true;
	}
	public function deletedata($id) {
		$this->db->where('id', $id);
		$this->db->delete('news');
	}

	 public function selectdata($id) {
		 if($id==NULL){
		  $query = $this->db->select('*,DATE_FORMAT( date_news, "%d/%m/%Y" ) as date_news,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('news')->order_by("createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('*,DATE_FORMAT( date_news, "%d/%m/%Y" ) as date_news,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('news')->where('id', $id)->get();
			 
		 }
          return $query->result();
	}
	  public function selectdata_search($keyword) {
		 if($keyword=='ALL'){
		  $query = $this->db->select('*,DATE_FORMAT( date_news, "%d/%m/%Y" ) as date_news,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('news')->order_by("date_news","desc")->get();
		}else{
			  $query = $this->db->select('*,DATE_FORMAT( date_news, "%d/%m/%Y" ) as date_news,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('news')->where('highlight_news',$keyword)->get(); 
		 }
          return $query->result();
	}
	 public function get_form() {
		
		return NULL;

	}
	
 }

?>