<?php
 class online_m extends CI_Model {     
	 function __construct() 
	{
		parent::__construct();
	}
	
	public function save($data,$id)
	{
		if($id){
			$this->db->update('online_ch', $data, array('id' => $id));
		}else{
			$this->db->insert('online_ch', $data);
		}
		
	}
	public function deletedata($id) {
		$this->db->where('id', $id);
		$this->db->delete('online_ch');
	}
	 public function selectdata($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('*,DATE_FORMAT( Release_on, "%d/%m/%Y") as Release_on')->from('online_ch')->order_by("createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('*,DATE_FORMAT( Release_on, "%d/%m/%Y") as Release_on')->from('online_ch')->where('id', $id)->get();
			 
		 }
          return $query->result();

	}
	
	
 }

?>