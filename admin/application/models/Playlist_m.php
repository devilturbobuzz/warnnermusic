<?php
 class Playlist_m extends CI_Model {     
	 function __construct() 
	{
		parent::__construct();
	}
	
	public function save($data,$table,$id,$wher_colums)
	{
		
		if($id){
			$this->db->update($table, $data, array($wher_colums => $id));
		}else{
			$this->db->insert($table, $data);
		}
	}
	 
	 public function update_playlist_cat($id,$table,$id_update,$wher_colums)
	{
		if($id){
			$query = 'UPDATE  `playlist` SET `id_playlist_cat` = CONCAT(`id_playlist_cat`, ' ;
			$query .=  "',".$id_update."'";
			$query .= ") WHERE `id_playlist_cat` like '%".$id."%'";
			$this->db->query($query);
		}
	}
	 
	
	public function deletedata($id) {
		$this->db->where('id', $id);
		$this->db->delete('playlist');
	}
	public function deletedata_cat($id) {
		$this->db->where('id', $id);
		$this->db->delete('playlist_cat');
	} 
	 public function deletedata_album($id) {
		$this->db->where('album_id', $id);
		$this->db->delete('album');
	}
	  public function deletedata_mood($id) {
		$this->db->where('mood_id', $id);
		$this->db->delete('mood');
	}
	public function deletedata_tags($id) {
		$this->db->where('id_module', $id);
		$this->db->delete('tags_search');
	} 
	public function save_tags($data,$id)
	{
			$this->db->insert('tags_search', $data);
	}  

	 public function selectdata_playlist($id) {
			  $query = $this->db->select('a.*')->from('playlist_sub a')->where('a.playlist_id', $id)->get(); 
		      return $query->result();
	 }
	 
	 public function selectdata($id) {
		 if($id==NULL){
		  $query = $this->db->select('a.*,DATE_FORMAT( a.release_on, "%d/%m/%Y") as release_on,DATE_FORMAT( a.createdatetime, "%d/%m/%Y") as createdatetime')->from('playlist a')->order_by("a.release_on","desc")->get();
		}else if($id){
			  $query = $this->db->select('a.*,DATE_FORMAT( a.release_on, "%d/%m/%Y") as release_on,DATE_FORMAT( a.release_on, "%d/%m/%Y") as createdatetime')->from('playlist a')->where('a.id', $id)->get(); 
		 }
          return $query->result();

	 }
	
	  public function selectdata_genres_name($id) {
	  			$id = explode(",", $id);
			  $query = $this->db->select('subject')->from('playlist_cat')->where_in('id', $id)->get();
          return $query->result();
	} 



	 public function selectdata_playlist_sub_cat($id) {
		
			  $query = $this->db->select('a.*')->from('playlist_sub_cat a')->where('a.id_playlist_cat', $id)->get(); 
		 
          return $query->result();

	 }
	  public function selectdata_playlist_cat_all() {
		
			  $query = $this->db->select('a.*,DATE_FORMAT( a.createdatetime, "%d/%m/%Y") as createdatetime')->from('playlist_cat a')->get(); 
		 
          return $query->result();

	 }
	  public function selectdata_search($keyword) {
		if($keyword){
			$keyword=" WHERE a.subject LIKE '%".$keyword."%'";
		}
			  $query = $this->db->select('a.*,DATE_FORMAT( a.release_on, "%d/%m/%Y") as release_on,DATE_FORMAT( a.createdatetime, "%d/%m/%Y") as createdatetime')->from('playlist a'.$keyword)->order_by("a.release_on","desc")->get();
		 
          return $query->result();

	 }
		 
	 public function selectdata_playlist_cat($id) {
		 if($id==NULL){
		  $query = $this->db->select('a.*,DATE_FORMAT( a.release_on, "%d/%m/%Y") as release_on,DATE_FORMAT( a.createdatetime, "%d/%m/%Y") as createdatetime,(select count(x.id_playlist_cat) from playlist_sub_cat x where x.id_playlist_cat=a.id and x.id_playlist_cat<>0) as trackmusic')->from('playlist_cat a')->order_by("a.release_on","desc")->get();
		}else if($id){
			  $query = $this->db->select('a.*,DATE_FORMAT( a.release_on, "%d/%m/%Y") as release_on')->from('playlist_cat a')->where('a.id', $id)->get(); 
		 }
          return $query->result();

	 }
	 
	  public function selectdata_artist($id) {
	     	$query = $this->db->select('a.artist_name,a.artist_id')->from('artist a')->get();
          return $query->result();

	 }
	  public function selectdata_music($id) {
	     	$query = $this->db->select('a.id,a.music_name')->from('music_audio a')->join('music m', 'a.music_id=m.id', 'left')->where('m.artist_id', $id)->get();
			 
          return $query->result();

	 }
	 public function select_max_playlist() {
			 $query = $this->db->select('MAX(a.id) as id')->from('playlist a')->get();
           return $query->result();

	} 
	 public function select_max_playlist_cat() {
			 $query = $this->db->select('MAX(a.id) as id')->from('playlist_cat a')->get();
           return $query->result();

	}
	 
	  public function selectdata_mood($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('mood')->order_by("createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('mood')->where('mood_id', $id)->get();
			 
		 }
          return $query->result();

	}
	public function selectdata_gallery($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('*')->from('gallery')->order_by("name","desc")->get();
		}else if($id){
			  $query = $this->db->select('*')->from('gallery')->where('id', $id)->get();
			 
		 }
          return $query->result();

	}
	
	
 }

?>