<?php
 class Tags_m extends CI_Model {     
	 function __construct() 
	{
		parent::__construct();
	}
	
	public function save($data,$id)
	{
		if($id){
			$this->db->update('tags_search', $data, array('id' => $id));
		}else{
			$this->db->insert('tags_search', $data);
		}
		
	}
	public function deletedata($id) {
		$this->db->where('id', $id);
		$this->db->delete('tags_search');
	}
	 public function selectdata($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('tags_search')->order_by("createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('tags_search')->where('id', $id)->get();
			 
		 }
          return $query->result();

	}
	
	
 }

?>