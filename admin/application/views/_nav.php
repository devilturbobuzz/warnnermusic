<nav class="left-menu" left-menu>
    <div class="logo-container" style="background-color: #1a356c;">
        <a href="/" class="logo">
            <img src="<?=base_url()?>assets/common/img/logo.png" alt="Clean UI Admin Template" />
            <img class="logo-inverse" src="<?=base_url()?>assets/common/img/logo-inverse.png" alt="Clean UI Admin Template" />
        </a>
    </div>
    <div class="left-menu-inner scroll-pane">
        <ul class="left-menu-list left-menu-list-root list-unstyled">
            
            
            
            <li class="left-menu-list" id="Dashboard_menu">
                <a class="left-menu-link" href="<?=site_url('Dashboard/')?>">
                    <i class="left-menu-link-icon icmn-home2"><!-- --></i>
                    <span class="menu-top-hidden">Dashboard
                </a>
            </li>
            
            <li class="left-menu-list-separator"><!-- --></li>
            <li class="left-menu-list-submenu home_menu">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-files-empty2"><!-- --></i>
                    Home
                </a>
                <ul class="left-menu-list list-unstyled home_menu">
                    <li>
                        <a class="left-menu-link" id="slide_top_sub"  href="<?=site_url('Home/')?>">
                            Slide Top
                        </a>
                    </li>
                    <li>
                        <a class="left-menu-link"  id="artist_month" href="<?=site_url('Home/artist_month')?>">
                           Artist of the Month
                        </a>
                    </li>
                     <li>
                        <a class="left-menu-link" id="vedio_sub" href="<?=site_url('Music/music_video')?>">
                              Music Video 
                        </a>
                    </li>
                  
                    
                </ul>
            </li>
            
            
            
            <li class="left-menu-list-separator"><!-- --></li>
             <li class="left-menu-list" id="news_menu">
                <a class="left-menu-link" href="<?=site_url('News/')?>">
                    <i class="left-menu-link-icon fa fa-newspaper-o"><!-- --></i>
                    News
                </a>
            </li>
           <li class="left-menu-list-separator"><!-- --></li>

          <li class="left-menu-list-submenu artist_menu" >
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon fa fa-user-plus"><!-- --></i>
                   Artist
                </a>
                <ul class="left-menu-list list-unstyled artist_menu">
                  
                 
                  <li>
                        <a class="left-menu-link" id="Nationality_sub" href="<?=site_url('Artist/Nationality')?>">
                              Category 
                        </a>
                    </li>
                     <li>
                        <a class="left-menu-link" id="genres_sub" href="<?=site_url('Artist/Genres')?>">
                              Genre 
                        </a>
                    </li>
                  <li>
                        <a class="left-menu-link" id="artist_sub" href="<?=site_url('Artist/')?>">
                              Artist 
                        </a>
                    </li>
                    <li>
                        <a class="left-menu-link" id="album_sub" href="<?=site_url('Artist/Album')?>">
                              Album 
                        </a>
                    </li>
                  
                   
                </ul>
            </li>
           

             <!--<li class="left-menu-list-submenu music_menu" >
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon fa fa-music"></i>
                   Music
                </a>
                <ul class="left-menu-list list-unstyled music_menu">
                  <li>
                        <a class="left-menu-link" id="music" href="<?=site_url('Music/')?>">
                              Create Artist Of Music 
                        </a>
                    </li> 
                  <li>
                        <a class="left-menu-link" id="audio_sub" href="<?=site_url('Music/music_audio')?>">
                              Music Audio 
                        </a>
                    </li>
                   
                      <li>
                        <a class="left-menu-link" id="mood_sub" href="<?=site_url('Artist/Mood')?>">
                           Mood 
                        </a>
                    </li>
                </ul>
            </li>-->
            
           <li class="left-menu-list-separator"><!-- --></li>
             
            
           <li class="left-menu-list-submenu playlist_menu" >
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon fa fa-folder-open"><!-- --></i>
                   Playlist
                </a>
                <ul class="left-menu-list list-unstyled playlist_menu">
                   <li>
                        <a class="left-menu-link" id="playlist_sub" href="<?=site_url('Playlist/theme')?>">
                              Genre & Mood
                        </a>
                    </li>
                   
                  <li>
                        <a class="left-menu-link" id="playlist" href="<?=site_url('Playlist/')?>">
                              Playlist
                        </a>
                    </li>
                
                     
                </ul>
            </li>
            <li class="left-menu-list-separator"><!-- --></li>
             <li class="left-menu-list" id="tags_menu">
                <a class="left-menu-link" href="<?=site_url('Tags/')?>">
                    <i class="left-menu-link-icon  fa fa-tag"><!-- --></i>
                    Tags
                </a>
            </li>
            <li class="left-menu-list-separator"><!-- --></li>
             <li class="left-menu-list" id="gallery_menu">
                <a class="left-menu-link" href="<?=site_url('Gallery/')?>">
                    <i class="left-menu-link-icon  icmn-images"><!-- --></i>
                    Service
                </a>
            </li>
            <li class="left-menu-list-separator"><!-- --></li>
             <li class="left-menu-list" id="online_ch">
                <a class="left-menu-link" href="<?=site_url('Online/')?>">
                    <i class="left-menu-link-icon  fa fa-link"><!-- --></i>
                    online channels
                </a>
            </li>
        </ul>
    </div>
</nav>
<nav class="top-menu">
    <div class="menu-icon-container hidden-md-up">
        <div class="animate-menu-button left-menu-toggle">
            <div><!-- --></div>
        </div>
    </div>
    <div class="menu" style="background-color:#1a356c;">
        <div class="menu-user-block">
            <div class="dropdown dropdown-avatar">
                <a href="javascript: void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="avatar" href="javascript:void(0);">
                        <img src="<?=base_url()?>assets/common/img/temp/avatars/1.jpg" alt="Alternative text to the image">
                    </span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="" role="menu">
                  
                    <a class="dropdown-item" href="/Login/Logoff"><i class="dropdown-icon icmn-exit"></i> Logout</a>
                </ul>
            </div>
        </div>
        <div class="menu-info-block">
            <div class="left">
                
            </div>
            
            
            
        </div>
    </div>
</nav>