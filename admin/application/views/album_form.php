<!doctype html>
<html>
<body class="theme-default">



<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Form Elements -->
    <section class="panel">
      
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                  
                        <h4>Album Form</h4>
                        <br />
                        <!-- Horizontal Form -->
                        <form method="post" id="artist_save" action="/Artist/Form_save_album" enctype="multipart/form-data">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="form-control-label" for="l16">Thumbnail (750x750) .jpg .png <label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                   			 <input id="file-1"  type="file" multiple=true name="fileToUpload_thumnail" >
                						</div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Artist<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                   <select class="form-control" id="l13" name="Artist">
                                        <option value="">Select Artist</option>
                                         <?php 
                    						foreach($data_artist as $item)
											{

												$select_artist="";
												 if(isset($data_album[0]->artist_id)){
													 if($data_album[0]->artist_id==$item->artist_id){
														 $select_artist="selected";
													 }
												 }
									       ?>
                                        <option value="<?=$item->artist_id?>" <?=$select_artist?>><?=$item->artist_name?></option>
                                        
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l15">Album<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text"  class="form-control" placeholder="Album name" name="AlbumName" value="<?=isset($data_album[0]->album_name)?$data_album[0]->album_name:""?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l15">Date<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                   
                                   
                             
                                       <input type='text' name="Release_on"  value="<?=isset($data_album[0]->Release_on)?$data_album[0]->Release_on:""?>" class="form-control" id='datetimepicker'  />
                                 <small class="text-muted">Date mask input: วว/ดด/ปปปป</small>
                                </div>
                            </div>
                             <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l15">Description</label>
                                </div>
                                <div class="col-md-10">
                                    <textarea type="text"  class="form-control" rows="7" placeholder="Description" id="editor1" name="description" ><?=isset($data_album[0]->description)?$data_album[0]->description:""?></textarea>
                                </div>
                            </div>
                             <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l15">Album Music</label>
                                </div>
                                <div class="col-md-10">
                                    <textarea type="text"  class="form-control" rows="7" placeholder="Description" id="editor2" name="music_description" ><?=isset($data_album[0]->music_description)?$data_album[0]->music_description:""?></textarea>
                                </div>
                            </div>
                           
                           
                             <ul class="url_download" style=" list-style: none;margin-left: 52px;" >
                            <? if(isset($data_album[0]->url_subject)){  
								$arr_url_subject=explode(",",$data_album[0]->url_subject);
								$arr_url_download=explode(",",$data_album[0]->url_download);
								for($i=0;$i<count($arr_url_subject);$i++){ ?>
								<li class="li"><?=$i==0?"Service":""?><select style="display: inline-block; width: 250px; <?=$i==0?"margin-left: 21px;":"margin-left: 68px;"?>"  class="form-control"  name="url_subject[]">
                               <? foreach($arr_gallery as $item) { ?>
                               <?
									   $select_str="";
								   if($arr_url_subject[$i]==$item->id){
									   $select_str="selected";
								   }
							   ?>
								  <option <?=$select_str?> data-class="avatar" value="<?=$item->id?>" style="background-image:url(<?=$item->thumnail_path?>);"><?=$item->name?></option>
								<? } ?>  
								</select>  &nbsp;&nbsp;&nbsp;   
                            	<input type="text" style="    display: inline-block;width: 500px; "  class="form-control" placeholder="link download" name="url_download[]" value="<?=$arr_url_download[$i]?>">&nbsp;&nbsp;&nbsp; <button type="button" onClick="<?=$i==0?"btn_url()":"$(this).closest('.li').remove()"?>" class="btn btn-icon <?=$i==0?"btn-info":"btn-danger"?> btn-info btn-rounded margin-inline">
                             		
                              		
                               		<i class="fa  <?=$i==0?"fa-plus":"fa-minus"?>" aria-hidden="true"></i>
                               </button></li>
							<?		
								}
							?>
									
							<?	}else{ ?>
								<li class="li">Service &nbsp;&nbsp;&nbsp;  
                              		<select style="display: inline-block; width: 250px; "  class="form-control"  name="url_subject[]">
                               <? foreach($arr_gallery as $item) { ?>
                               <?
									   $select_str="";
								   if($arr_url_subject[$i]==$item->id){
									   $select_str="selected";
								   }
							   ?>
								  <option <?=$select_str?> data-class="avatar" value="<?=$item->id?>" style="background-image:url(<?=$item->thumnail_path?>);"><?=$item->name?></option>
								<? } ?>  
								</select>&nbsp;&nbsp;&nbsp;   
                              		<input type="text" style="    display: inline-block;width: 500px; "  class="form-control" placeholder="link download" name="url_download[]" value="<?=isset($data_album[0]->url_download)?$data_album[0]->url_download:""?>">&nbsp;&nbsp;&nbsp; <button type="button" onClick="btn_url()" class="btn btn-icon btn-info btn-rounded margin-inline">
                               		<i class="fa fa-plus" aria-hidden="true"></i>
                               </button></li>
                            <?	} ?>
                            </ul>
                            
                           <div class="form-actions">
                                <div class="form-group row">
                                    <div class="col-md-1 col-md-offset-9">
                                        <button type="button"  onClick="$('#artist_save').submit();" class="btn width-150 btn-info">Submit</button>
                                        
                                    </div>
                                </div>
                            </div>
                              <input type="hidden" value="<?=isset($data_album[0]->album_id)?$data_album[0]->album_id:""?>" name="id" />
                        </form>
                        <!-- End Horizontal Form -->
                   
                </div>
            </div>
         
        </div>
    </section>
    <!-- End -->

</div>
</section>

<div class="main-backdrop"><!-- --></div>

</body>
</html>
<script>

    $(function(){
$('#datetimepicker').datetimepicker({
                 format: 'DD/MM/YYYY'
           });
var file1="<?=isset($data_album[0]->thumnail_path)?"/".$data_album[0]->thumnail_path:""?>";
	
        $("[data-toggle=tooltip]").tooltip();
		$(".artist_menu").addClass("left-menu-list-opened").show();
		$("#album_sub").css("color","#000");
   
     
      
	
		$("#file-1").fileinput({
		showUpload: false,
		showCaption: false,
		browseClass: "btn btn-info",
        allowedFileExtensions : ['jpg', 'png'],
		initialPreview: [ '<img src="'+file1+'" style="width:auto;height:160px;" class="kv-preview-data file-preview-image" >']	
    });
    CKEDITOR.replace( 'editor1' );
		CKEDITOR.replace('editor2');
	CKEDITOR.config.width="100%";
	CKEDITOR.config.height="50%";
	
	

    })
</script>
	<style>.progress,.fileinput-cancel-button{display: none;}</style>