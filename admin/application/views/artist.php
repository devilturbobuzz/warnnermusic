<body class="theme-default">


<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Tables -->
    <section class="panel">
        
        <div class="panel-body">
           
            <div class="row">
                <div class="col-lg-12">
                    <h4>Artist</h4>
                    <p><code>Artist / index</code></p>
                    <br />
                     <div class="col-md-1">
                        <label class="form-control-label"  >Search :</label>
					</div>
                
                    <div class="col-md-4">
                    <form method="post"  action="/Artist/search_album" >
                    <input type="search" class="form-control input-sm" value="<?=isset($val_search)?$val_search:""?>" placeholder="artist"   name="keyword">
						</form>
					</div>
                    <br />   <br />   <br />
                    <div class="table-responsive margin-bottom-50">
                        <table class="table table-hover" style="    font-size: 13px;">
                            <thead>
                                <tr>
                                    <th>Artist</th>
                                    <th>Category</th>
                                    <th>Genre</th>
                                   
                                    <th><a href="Artist/Form" class="btn btn-xs margin-inline" style="margin: 0px;"><i class="fa fa-plus"></i></a></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                      <th>Artist</th>
                                    <th>Category</th>
                                    <th>Genre</th>
                                    <th><a href="Artist/Form" class="btn btn-xs margin-inline" style="margin: 0px;"><i class="fa fa-plus"></i></a></th>
                                </tr>
                            </tfoot>
                            <tbody>
                               <?php 
                    			foreach($arr_data as $item)
									{
								?>
                                 <tr>
                                 <td>
                                 <?php
										
										echo $item->artist_name;
									 
										 	
								  ?>
									
                                   </td>
                                   
                                    <td >
                                    	<?=$item->Genres?>	
                                    </td>
                                    <td><?=$item->Nationality?>	</td>
                                   
                                    
                                     <td>
                                   <a href="/Artist/EditArtist/<?=$item->artist_id?>" target="_self"> 
                                   <span class="fa fa-pencil btn btn-xs "  title="Edit"></span>&nbsp;
                                   </a>
                                    <a href="/Artist/form_delete/<?=$item->artist_id?>" target="_self">
                                    <span class="fa fa-close btn btn-xs swal-btn-warning" data-id="<?=$item->artist_id?>" title="Delete"></span>
                                    </a>
                                   </td>
                                </tr>
                             <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Basic Tables  -->

</div>

<!-- Page Scripts -->
<script>

    $(function () {

        $("[data-toggle=tooltip]").tooltip();
		$(".artist_menu").addClass("left-menu-list-opened").show();
		$("#artist_sub").css("color","#000");
		$('.swal-btn-warning').click(function(e){
			var data_delete=$(this).data("id");
            e.preventDefault();
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this file!",
                type: "warning",
                showCancelButton: true,
                cancelButtonClass: "btn-default",
                confirmButtonClass: "btn-warning",
                confirmButtonText: "Remove",
                closeOnConfirm: false
            },
            function(){
                swal({
                    title: "Deleted!",
                    text: "File has been deleted",
                    type: "success",
                    confirmButtonClass: "btn-success"
                });
				
				window.open("/Artist/form_delete/"+data_delete,"_self");
            });
			
        });
    });

</script>
<!-- End Page Scripts -->
</section>

<div class="main-backdrop"><!-- --></div>

</body>