<!doctype html>
<html>
<body class="theme-default">

<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Form Elements -->
    <section class="panel">
      
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                  
                        <h4>Artist Form</h4>
                        <br />
                        <!-- Horizontal Form -->
                        <form method="post" id="artist_save" action="/Artist/Form_save" enctype="multipart/form-data">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="form-control-label" for="l16">Thumbnail (167x178) .jpg .png <label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                      
                   			 <input id="file-1"  type="file" multiple=true name="fileToUpload_thumnail" >
                						</div>
                                  
                                </div>
                            </div>


                               <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="form-control-label" for="l16">Large Image (1400x768) .jpg .png</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                   
                                 <input id="file-2" type="file" multiple=true name="fileToUpload_highlight" >
                                  
                                </div>
                            </div>
                             </div>
                             
                          
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Artist<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text"  class="form-control" placeholder="artist name" name="artist_name" value="<?=isset($editdata[0]->artist_name)?$editdata[0]->artist_name:""?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Category<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                 
                                   <select class="form-control" id="l13" name="Nationality">
                                      <option value="">--Category--</option>
                                       <? foreach($arr_nationality as $item) { ?>
                                      		<?
												$select_Nationality="";
												 if(isset($editdata[0]->Nationality)){
													 if($editdata[0]->Nationality==$item->id){
														 $select_Nationality="selected";
													 }
												 }
									         ?>
                                       		<option value="<?=$item->id?>" <?=$select_Nationality?> ><?=$item->nationality_name?></option>
                                       <? } ?>
                                    </select>
                                </div>
                            </div>
                             <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Genre<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                   
                                    <select class="form-control" id="l13" name="Genres">
                                        <option value="">--Genres of music--</option>
                                         <? foreach($arr_genres as $item) { ?>
                                      		<?
												$select_Genres="";
												 if(isset($editdata[0]->Genres)){
													 if($editdata[0]->Genres==$item->id){
														 $select_Genres="selected";
													 }
												 }
									         ?>
                                       		<option value="<?=$item->id?>" <?=$select_Genres?> ><?=$item->genres_name?></option>
                                       <? } ?>
                                    </select>
                                </div>
                            </div>
                             
                            
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l15">Description </label>
                                </div>
                                <div class="col-md-10">
                                    <textarea class="form-control"    rows="3" id="editor1" name="Description"><?=isset($editdata[0]->Description)?$editdata[0]->Description:""?></textarea>
                                </div>
                            </div>
                              <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Tags</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" value="<?=isset($editdata[0]->tags)?$editdata[0]->tags:""?>" class="form-control" placeholder="Tags index" name="tags">
                                      <span class="text-info">ตัวอย่าง หากมี index มากกว่า 1 ให้ระบุโดยการใส่ " , " คั่นระหว่างคำ popnews,rocknew</span>
                                </div>
                               
                            </div>
                               
                            <ul class="url_download" style=" list-style: none;margin-left: 52px;" >
                            <? if(isset($editdata[0]->url_subject)){  
								$arr_url_subject=explode(",",$editdata[0]->url_subject);
								$arr_url_download=explode(",",$editdata[0]->url_download);
								for($i=0;$i<count($arr_url_subject);$i++){ ?>
								<li class="li"><?=$i==0?"Service":""?> &nbsp;&nbsp;&nbsp;    <select style="display:inline-block;margin-left: 20px;  width: 250px; <?=$i==0?"":"margin-left: 70px;"?>"  class="form-control"  name="url_subject[]">
                               <? foreach($arr_gallery as $item) { ?>
                               <?
									   $select_str="";
								   if($arr_url_subject[$i]==$item->id){
									   $select_str="selected";
								   }
							   ?>
								  <option <?=$select_str?> data-class="avatar" value="<?=$item->id?>" style="background-image:url(<?=$item->thumnail_path?>);"><?=$item->name?></option>
								<? } ?> 
								</select>  &nbsp;&nbsp;&nbsp;   
                            	<input type="text" style="    display: inline-block;width: 500px; "  class="form-control" placeholder="link download" name="url_download[]" value="<?=isset($arr_url_download[$i])?$arr_url_download[$i]:""?>">&nbsp;&nbsp;&nbsp; <button type="button" onClick="<?=$i==0?"btn_url()":"$(this).closest('.li').remove()"?>" class="btn btn-icon <?=$i==0?"btn-info":"btn-danger"?> btn-info btn-rounded margin-inline">
                             		
                              		
                               		<i class="fa  <?=$i==0?"fa-plus":"fa-minus"?>" aria-hidden="true"></i>
                               </button></li>
							<?		
								}
							?>
									
							<?	}else{ ?>
								<li class="li">Service &nbsp;&nbsp;&nbsp;  
                              		<select style="display: inline-block; width: 250px;margin-left: 20px; "  class="form-control"  name="url_subject[]">
                               <? foreach($arr_gallery as $item) { ?>
                               <?
									   $select_str="";
								   if($arr_url_subject[$i]==$item->id){
									   $select_str="selected";
								   }
							   ?>
								  <option <?=$select_str?> data-class="avatar" value="<?=$item->id?>" style="background-image:url(<?=$item->thumnail_path?>);"><?=$item->name?></option>
								<? } ?>  
								</select>&nbsp;&nbsp;&nbsp;   
                              		<input type="text" style="    display: inline-block;width: 500px; "  class="form-control" placeholder="link download" name="url_download[]" value="<?=isset($editdata[0]->url_download)?$editdata[0]->url_download:""?>">&nbsp;&nbsp;&nbsp; <button type="button" onClick="btn_url()" class="btn btn-icon btn-info btn-rounded margin-inline">
                               		<i class="fa fa-plus" aria-hidden="true"></i>
                               </button></li>
                            <?	} ?>
                            </ul>
                           
                             <ul class="url_account" style=" list-style: none;margin-left: 22px;" >
                            <? if(isset($editdata[0]->url_off)){  
								$arr_url_subject=explode(",",$editdata[0]->url_off);
								$arr_url_download=explode(",",$editdata[0]->url_off_download);
								for($i=0;$i<count($arr_url_subject);$i++){ ?>
								<li class="li_acc" <?=$i==0?"style='margin-left:-32px;'":""?>><?=$i==0?"Online channel":""?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <select style="display: inline-block; width: 250px; <?=$i==0?"":"margin-left: 64px;"?>"  class="form-control"  name="url_off[]">
                               <? foreach($arr_online_ch as $item) { ?>
                               <?
									   $select_str="";
								   if($arr_url_subject[$i]==$item->id){
									   $select_str="selected";
								   }
							   ?>
								  <option <?=$select_str?> data-class="avatar" value="<?=$item->id?>" style="background-image:url(<?=$item->thumnail_path?>);"><?=$item->name?></option>
								<? } ?>  
								</select>&nbsp;&nbsp;&nbsp;   <input type="text" style="    display: inline-block;width: 500px; "  class="form-control" placeholder="link account" name="url_off_download[]" value="<?=isset($arr_url_download[$i])?$arr_url_download[$i]:""?>">&nbsp;&nbsp;&nbsp; <button type="button" onClick="<?=$i==0?"btn_url_acc()":"$(this).closest('.li_acc').remove()"?>" class="btn btn-icon <?=$i==0?"btn-info":"btn-danger"?> btn-info btn-rounded margin-inline">
                             		
                              		
                               		<i class="fa  <?=$i==0?"fa-plus":"fa-minus"?>" aria-hidden="true"></i>
                               </button></li>
							<?		
								}
							?>
									
							<?	}else{ ?>
								<li class="li_acc">Online channel &nbsp;&nbsp;&nbsp;   <select style="display: inline-block; width: 250px; "   class="form-control"  name="url_off[]">
                               <? foreach($arr_online_ch as $item) { ?>
                               <?
									   $select_str="";
								   if($arr_url_subject[$i]==$item->id){
									   $select_str="selected";
								   }
							   ?>
								  <option <?=$select_str?> data-class="avatar" value="<?=$item->id?>" style="background-image:url(<?=$item->thumnail_path?>);"><?=$item->name?></option>
								<? } ?>  
								</select>&nbsp;&nbsp;&nbsp;   <input type="text" style="display: inline-block;width: 500px; "  class="form-control" placeholder="link account" name="url_off_download[]" value="<?=isset($editdata[0]->url_off_download)?$editdata[0]->url_off_download:""?>">&nbsp;&nbsp;&nbsp; <button type="button" onClick="btn_url_acc()" class="btn btn-icon btn-info btn-rounded margin-inline">
                               		<i class="fa fa-plus" aria-hidden="true"></i>
                               </button></li>
                            <?	} ?>
                            </ul>
                          
                          
                           <div class="form-actions">
                                <div class="form-group row">
                                    <div class="col-md-1 col-md-offset-9">
                                        <button type="button"  onClick="$('#artist_save').submit();" class="btn width-150 btn-info">Submit</button>
                                        
                                    </div>
                                </div>
                            </div>
                              <input type="hidden" value="<?=isset($editdata[0]->artist_id)?$editdata[0]->artist_id:""?>" name="id" />
                        </form>
                        <!-- End Horizontal Form -->
                   
                </div>
            </div>
         
        </div>
    </section>
    <!-- End -->

</div>
</section>
<div id="picture_get" style="display: none;">
	<select style='display: inline-block; width: 250px;margin-left: 70px;'  class='form-control'  name='url_subject[]'>
       <? foreach($arr_gallery as $item) { ?>
         <option  value='<?=$item->id?>' style='background-image:url(<?=$item->thumnail_path?>);'><?=$item->name?></option>
		<? } ?>  
	</select>
</div>
<div id="picture_get2" style="display: none;">
	<select style='display: inline-block; width: 250px;margin-left: 70px;'  class='form-control'  name='url_off[]'>
       <? foreach($arr_gallery as $item) { ?>
         <option  value='<?=$item->id?>' style='background-image:url(<?=$item->thumnail_path?>);'><?=$item->name?></option>
		<? } ?>  
	</select>
</div>
<div class="main-backdrop"><!-- --></div>

</body>
</html>
<script>

    $(function(){
var file1="<?=isset($editdata[0]->thumnail_path)?"/".$editdata[0]->thumnail_path:""?>";
var file2="<?=isset($editdata[0]->pic_description_path)?"/".$editdata[0]->pic_description_path:""?>";
       
		$(".artist_menu").addClass("left-menu-list-opened").show();
		$("#artist_sub").css("color","#000");
		
		$("#file-1").fileinput({
		showUpload: false,
		showCaption: false,
		browseClass: "btn btn-info",
        allowedFileExtensions : ['jpg', 'png'],
		initialPreview: [ '<img src="'+file1+'" style="width:auto;height:160px;" class="kv-preview-data file-preview-image" >']	
    });
    
        $("#file-2").fileinput({
        showUpload: false,
        showCaption: false,
        browseClass: "btn btn-info",
        allowedFileExtensions : ['jpg', 'png'],
		initialPreview: [ '<img src="'+file2+'" style="width:auto;height:160px;" class="kv-preview-data file-preview-image" >']
		
       
	});
		CKEDITOR.replace( 'editor1' );
		CKEDITOR.config.width="100%";
		CKEDITOR.config.height="80%";
		CKEDITOR.config.extraAllowedContent = 'iframe[*]'
    });

    function remove_url(v){
		$(v).closest('.li').remove();
	}
	 function remove_url_acc(v){
		$(v).closest('.li_acc').remove();
	}
	function btn_url(){
		var picture_get=$("#picture_get").html();
		var url_app="<li class='li'> &nbsp;&nbsp;&nbsp;  "+picture_get+"&nbsp;&nbsp;&nbsp;   <input type='text' style='    display: inline-block;width: 500px; '  class='form-control' placeholder='link download' name='url_download[]' value=''>&nbsp;&nbsp;&nbsp; <button type='button' onclick='remove_url(this)' class='btn btn-icon btn-danger  btn-rounded margin-inline'><i class='fa fa-minus' aria-hidden='true'></i></button></li>";
		 $(".url_download").append(url_app);
	}
	function btn_url_acc(){
		var picture_get=$("#picture_get2").html();
		var url_app="<li class='li_acc'> &nbsp;&nbsp;&nbsp;  "+picture_get+"&nbsp;&nbsp;&nbsp;   <input type='text' style='    display: inline-block;width: 500px; '  class='form-control' placeholder='link account' name='url_download[]' value=''>&nbsp;&nbsp;&nbsp; <button type='button' onclick='remove_url_acc(this)' class='btn btn-icon btn-danger  btn-rounded margin-inline'><i class='fa fa-minus' aria-hidden='true'></i></button></li>";
		 $(".url_account").append(url_app);
	}
</script>
	<style>.progress,.fileinput-cancel-button{display: none;}</style>