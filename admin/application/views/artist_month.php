<body class="theme-default">


<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Tables -->
    <section class="panel">
        
        <div class="panel-body">
           
            <div class="row">
                <div class="col-lg-12">
                    <h4>Atrist Of The Month</h4>
                    <p><code>Atrist Of The Month / index</code></p>
                  
                     <br />
                     <div class="col-md-1">
                        <label class="form-control-label"  >Search :</label>
                    </div>
                
                    <div class="col-md-4">
                    <form method="post"  action="/Home/search_artist_mounth" >
                    <input type="search" class="form-control input-sm" value="<?=isset($val_search)?$val_search:""?>" placeholder="Artist Or Album Name "   name="keyword">
                        </form>
                    </div>
                    <br />   <br />   <br />
                    <div class="table-responsive margin-bottom-50">
                        <table class="table table-hover" style="    font-size: 13px;">
                            <thead>
                                <tr>
                                    <th>Subject</th>
                                    <th>Album</th>
                                    <th>Description</th>
                                    <th>Picture Artist</th>
                                    <th>Picture Album</th>
                                  
                                    <th></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Subject</th>
                                    <th>Album</th>
                                    <th>Description</th>
                                     <th>Picture Artist</th>
                                     <th>Picture Album</th>
                                   
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                               <?php 
                    			foreach($arr_data as $item)
									{
								?>
                                 <tr>
                                 <td><?=$item->artist_name?> </td>
                                 <td><?=$item->album_name?></td>
                                 <td width="450"> <?php
										$str_subject_detail=$item->description;
									 if (strlen($str_subject_detail) > 550){
										echo $str_subject_detail = substr($str_subject_detail, 0, 550) . '...';
									 }else{
										echo $str_subject_detail;
									 }
										 	
								  ?></td>
                                 <td>
                                 	  <a href="/<?=$item->thumnail_path_artist?>" target="_blank">
                                 	  	<img src="/<?=$item->thumnail_path_artist?>" width="60" height="60" />
                                 	  	
                                 	  </a>
                                  </td>
								<td>
                                 	  <a href="/<?=$item->thumnail_path?>" target="_blank">
                                 	  	<img src="/<?=$item->thumnail_path?>" width="60" height="60" />
                                 	  </a>
                                  </td>
                                 
                                  <td>
                                  
                                   <a href="/Home/save_artist_month/<?=$item->album_id?>/<?=$item->artist_month=="Y"?"N":"Y"?>" target="_self">
                                      <span class=" fa <?=$item->artist_month=="Y"?"fa fa-flag":"fa-flag-o"?>  btn btn-xs " title="Add highlight"></span>&nbsp;
                                      </a>
                                  
                                   
                                   </td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Basic Tables  -->

</div>

<!-- Page Scripts -->
<script>

    $(function () {

        $("#artist_month").css("color","#000");
		$(".home_menu").addClass("left-menu-list-opened").show();
		
    });

</script>
<!-- End Page Scripts -->
</section>

<div class="main-backdrop"><!-- --></div>

</body>