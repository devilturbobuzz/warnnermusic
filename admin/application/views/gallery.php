<body class="theme-default">


<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Tables -->
    <section class="panel">
        
        <div class="panel-body">
           
            <div class="row">
                <div class="col-lg-12">
                    <h4>Gallery</h4>
                    <p><code>Gallery / index</code></p>
                    <br />
                    
                    <div class="table-responsive margin-bottom-50">
                        <table class="table table-hover" style="    font-size: 13px;">
                            <thead>
                                <tr>
                                  
                                    <th>Subject</th>
                                   
                                    <th>Date</th>
                                   <th><a href="/Gallery/Form" class="btn btn-xs margin-inline" style="margin: 0px;"><i class="fa fa-plus"></i></a></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                   
                                    <th>Subject</th>
                                  
                                    <th>Date</th>
                                   <th><a href="/Gallery/Form" class="btn btn-xs margin-inline" style="margin: 0px;"><i class="fa fa-plus"></i></a></th> 
                                </tr>
                            </tfoot>
                            <tbody>
                              <?php 
								$x=1;
                    			foreach($arr_data as $item)
								{
							  ?>
                                 <tr>
                                
								 <td>
								 	<?=$item->name?>
								 </td>
                              
                                 <td><?=$item->Release_on?></td>
                                 <td>
									   <a href="/Gallery/Edit/<?=$item->id?>" target="_self"> 
									   <span class="fa fa-pencil btn btn-xs "  title="Edit"></span>&nbsp;
									   </a>
										<a href="/Gallery/form_delete/<?=$item->id?>" target="_self">
										<span class="fa fa-close btn btn-xs swal-btn-warning" data-id="<?=$item->id?>" title="Delete"></span>
										</a>
                                 </td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Basic Tables  -->

</div>

<!-- Page Scripts -->
<script>

    $(function () {

        $("[data-toggle=tooltip]").tooltip();
		$("#gallery_menu").addClass("left-menu-list-active");
		$('.swal-btn-warning').click(function(e){
			var data_delete=$(this).data("id");
            e.preventDefault();
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this file!",
                type: "warning",
                showCancelButton: true,
                cancelButtonClass: "btn-default",
                confirmButtonClass: "btn-warning",
                confirmButtonText: "Remove",
                closeOnConfirm: false
            },
            function(){
                swal({
                    title: "Deleted!",
                    text: "File has been deleted",
                    type: "success",
                    confirmButtonClass: "btn-success"
                });
				
				window.open("/Gallery/form_delete/"+data_delete,"_self");
            });
			
        });
    });

</script>
<!-- End Page Scripts -->
</section>

<div class="main-backdrop"><!-- --></div>

</body>