<!doctype html>
<html>
<body class="theme-default">



<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Form Elements -->
    <section class="panel">
      
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                  
                        <h4>Gallery Form</h4>
                        <br />
                        <!-- Horizontal Form -->
                        <form method="post" id="artist_save" action="/Gallery/Form_save" enctype="multipart/form-data" >
                            
                            
                            
                           
                                <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="form-control-label" for="l16">Icon (112 x 36) .jpg .png <label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-9">
                                  <img  id="preview_img" height="32" width="112" src="<?=isset($editdata[0]->thumnail_path)?"/".$editdata[0]->thumnail_path:base_url()."assets/common/img/temp/avatars/1.jpg";?>" alt="your image" style="margin-left: 5px;"/><br/><br/>
                                   <label for="files_thumnail" class="btn width-100 btn-info">Browse..</label>
										  
											 
  						<input id="files_thumnail" style="visibility:hidden;" type="file" onchange="readURL(this);"    multiple=true name="fileToUpload_thumnail">
                                  
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l15">Subject<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text"  class="form-control" placeholder="name" name="name" value="<?=isset($editdata[0]->name)?$editdata[0]->name:""?>">
                                </div>
                            </div>
                             <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Date<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                   
                                       <input type='text' name="Release_on"  value="<?=isset($editdata[0]->Release_on)?$editdata[0]->Release_on:""?>" class="form-control" id='datetimepicker'  />
                                      <small class="text-muted">Date mask input: วว/ดด/ปปปป</small>
                                   
                                </div>
                                
                            </div>
                           <div class="form-actions">
                                <div class="form-group row">
                                    <div class="col-md-1 col-md-offset-9">
                                        <button type="button"  onClick="$('#artist_save').submit();" class="btn width-150 btn-info">Submit</button>
                                        
                                    </div>
                                </div>
                            </div>
                             <input type="hidden" value="<?=isset($editdata[0]->id)?$editdata[0]->id:""?>" name="id" />
                        </form>
                        <!-- End Horizontal Form -->
                   
                </div>
            </div>
         
        </div>
    </section>
    <!-- End -->

</div>
</section>

<div class="main-backdrop"><!-- --></div>

</body>
</html>
<script>

    $(function(){
			$('#datetimepicker').datetimepicker({
                 format: 'DD/MM/YYYY'
           });
        $("[data-toggle=tooltip]").tooltip();
		$("#gallery_menu").addClass("left-menu-list-active");

    })
	 function readURL(input) {

            if (input.files && input.files[0]) {
				 var reader = new FileReader();
                     reader.onload = function (e) {
                       $('#preview_img').attr('src', e.target.result);
                     }
				
                reader.readAsDataURL(input.files[0]);
            }
	 }
</script>
	<style>.progress,.fileinput-cancel-button{display: none;}</style>