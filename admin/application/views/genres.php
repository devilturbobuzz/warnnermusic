<body class="theme-default">


<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Tables -->
    <section class="panel">
        
        <div class="panel-body">
           
            <div class="row">
                <div class="col-lg-12">
                    <h4>Genre</h4>
                    <p><code>Artist / Genre</code></p>
                    <br />
                    
                    <div class="table-responsive margin-bottom-50">
                        <table class="table table-hover" style="    font-size: 13px;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Genres</th>
                                    
                                    <th>Date</th>
                                    <th><a href="/Artist/FormGenres" class="btn btn-xs margin-inline" style="margin: 0px;"><i class="fa fa-plus"></i></a></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Genres</th>
                                   
                                    <th>Date</th>
                                    <th><a href="/Artist/FormGenres" class="btn btn-xs margin-inline" style="margin: 0px;"><i class="fa fa-plus"></i></a></th>
                                </tr>
                            </tfoot>
                            <tbody>
                              <?php 
								$x=1;
                    			foreach($arr_data as $item)
								{
							  ?>
                                 <tr>
                                 <td><?=$x++;?></td>
								 <td>
								 	<?=$item->genres_name?>
								 </td>
                                
                                 <td><?=$item->Release_on?></td>
                                 <td>
									   <a href="/Artist/EditGenres/<?=$item->id?>" target="_self"> 
									   <span class="fa fa-pencil btn btn-xs "  title="Edit"></span>&nbsp;
									   </a>
                                       <?php  ///Artist/form_delete_genres/<?=$item->id ?>
										<a href="#" target="_self">
										 <button type="button" class="fa fa-close btn btn-xs" data-toggle="modal" data-target="#smmodel" onClick="removeGenre(<?=$item->id?>)">
                                                
                                         </button>
										</a>

               
              
                                 </td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Basic Tables  -->

</div>
<input type="hidden" value="" id="btn_change" />
   <div class="modal fade modal-size-small" id="smmodel" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Warning</h4>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure?</p>
                                <p>Please Move Genre in Artist</p>   
                                <p>
                                <select class="form-control " id="genre_change" >
                                 <option value="">Choose Genre</option>
                                <? foreach($arr_data as $item){ ?>
                               		 <option value="<?=$item->id?>"><?=$item->genres_name?></option>
                               	<? } ?>
                                </select>
                                </p> 
                                <p>Your will not be able to recover this file!</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-warning" onClick="fn_delete()" >Save changes & Remove</button>
                            </div>
                        </div>
                    </div>
                </div>

<script>

    $(function () {
        $("[data-toggle=tooltip]").tooltip();
		$(".artist_menu").addClass("left-menu-list-opened").show();
		$("#genres_sub").css("color","#000");
    });
function removeGenre(id){
	$('#genre_change option[value="'+id+'"]').attr("disabled", true);
	$("#btn_change").val(id);
}
function fn_delete(){
	var id_update = $("#genre_change").val();
	var id_remove =$("#btn_change").val();
	if(id_update==""){
		alert("Genre empty");
		return false;
	}else{
		window.open("/Artist/form_delete_genres/"+id_remove+"/"+id_update,"_self");
		
	}
}	
</script>
<!-- End Page Scripts -->
</section>

<div class="main-backdrop"><!-- --></div>

</body>