<!doctype html>
<html>
<body class="theme-default">



<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Form Elements -->
    <section class="panel">
      
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                  
                        <h4>Genre Form</h4>
                        <br />
                        <!-- Horizontal Form -->
                        <form method="post" id="artist_save" action="/Artist/Form_save_genres" enctype="multipart/form-data">
                            
                             <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l15">Thumnail (200x80) .jpg .png<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                      					<input id="file-1"  type="file" multiple=true name="fileToUpload_thumnail" >
                					</div>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l15">Genres<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text"  class="form-control" placeholder="genres name" name="genres_name" value="<?=isset($editdata[0]->genres_name)?$editdata[0]->genres_name:""?>">
                                </div>
                            </div>
                              <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Date<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                   
                                       <input type='text' name="Release_on"  value="<?=isset($editdata[0]->Release_on)?$editdata[0]->Release_on:""?>" class="form-control" id='datetimepicker'  />
                                    <small class="text-muted">Date mask input: วว/ดด/ปปปป</small>
                                   
                                </div>
                                
                            </div>
                           <div class="form-actions">
                                <div class="form-group row">
                                    <div class="col-md-1 col-md-offset-9">
                                        <button type="button"  onClick="$('#artist_save').submit();" class="btn width-150 btn-info">Submit</button>
                                        
                                    </div>
                                </div>
                            </div>
                             <input type="hidden" value="<?=isset($editdata[0]->id)?$editdata[0]->id:""?>" name="id" />
                        </form>
                        <!-- End Horizontal Form -->
                   
                </div>
            </div>
         
        </div>
    </section>
    <!-- End -->

</div>
</section>

<div class="main-backdrop"><!-- --></div>

</body>
</html>
<script>

    $(function(){
			/*$('#datetimepicker').datetimepicker({
                 format: 'DD/MM/YYYY'
           });*/
	var file1="<?=isset($editdata[0]->thumnail_path)?"/".$editdata[0]->thumnail_path:""?>";
        $("[data-toggle=tooltip]").tooltip();
		$(".artist_menu").addClass("left-menu-list-opened").show();
		$("#genres_sub").css("color","#000");
		
		$("#file-1").fileinput({
		showUpload: false,
		showCaption: false,
		browseClass: "btn btn-info",
        allowedFileExtensions : ['jpg', 'png'],
		initialPreview: [ '<img src="'+file1+'" style="width:auto;height:160px;" class="kv-preview-data file-preview-image" >']	
		});
		
    });
	
</script>
	<style>.progress,.fileinput-cancel-button{display: none;}</style>