
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>ADMIN WARNERMUSIC</title>

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendors/jscrollpane/style/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendors/ladda/dist/ladda-themeless.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendors/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendors/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendors/cleanhtmlaudioplayer/src/player.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendors/cleanhtmlvideoplayer/src/player.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendors/bootstrap-sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendors/summernote/dist/summernote.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendors/owl.carousel/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendors/ionrangeslider/css/ion.rangeSlider.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendors/datatables/media/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/vendors/c3/c3.min.css">
    

    <!-- Clean UI Styles -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/common/css/source/main.css">

    <!-- Vendors Scripts -->
    <!-- v1.0.0 -->
    <script src="<?=base_url()?>assets/vendors/jquery/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/tether/dist/js/tether.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/jscrollpane/script/jquery.jscrollpane.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/spin.js/spin.js"></script>
    <script src="<?=base_url()?>assets/vendors/ladda/dist/ladda.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/html5-form-validation/dist/jquery.validation.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/jquery-typeahead/dist/jquery.typeahead.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/autosize/dist/autosize.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/bootstrap-show-password/bootstrap-show-password.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/cleanhtmlaudioplayer/src/jquery.cleanaudioplayer.js"></script>
    <script src="<?=base_url()?>assets/vendors/cleanhtmlvideoplayer/src/jquery.cleanvideoplayer.js"></script>
    <script src="<?=base_url()?>assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/summernote/dist/summernote.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/ionrangeslider/js/ion.rangeSlider.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/nestable/jquery.nestable.js"></script>
    <script src="<?=base_url()?>assets/vendors/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/datatables/media/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js"></script>
    <script src="<?=base_url()?>assets/vendors/datatables-responsive/js/dataTables.responsive.js"></script>
    <script src="<?=base_url()?>assets/vendors/editable-table/mindmup-editabletable.js"></script>
    <script src="<?=base_url()?>assets/vendors/d3/d3.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/c3/c3.min.js"></script>
   
    <script src="<?=base_url()?>assets/vendors/peity/jquery.peity.min.js"></script>
    <!-- v1.0.1 -->
   
    <!-- v1.1.1 -->
    <script src="<?=base_url()?>assets/vendors/gsap/src/minified/TweenMax.min.js"></script>
    <script src="<?=base_url()?>assets/vendors/hackertyper/hackertyper.js"></script>
    <script src="<?=base_url()?>assets/vendors/jquery-countTo/jquery.countTo.js"></script>

    <!-- Clean UI Scripts -->
    <script src="<?=base_url()?>assets/common/js/common.js"></script>
    <script src="<?=base_url()?>assets/common/js/demo.temp.js"></script>
    
    
    <link href="<?=base_url()?>assets/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
      
        <script src="<?=base_url()?>assets/js/fileinput.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
     
    <style>
		.txt_alert {
			color: red;
			font-size: 14px;
			padding-left: 5px;
			}
		.form-control-label{float:right;}
		.error{
		color: #fb434a;
   	 padding: 0px;
   	 margin: 5px 0px 0px 0px;
   	 font-size: 80%;
   	 font-weight: 400;
	}
		</style>
		
</head>