<body class="theme-default" style="background-color:#1a356c;">

<section class="page-content">
<div class="page-content-inner" >


    <div class="single-page-block">
        <div class="single-page-block-inner effect-3d-element">
            <div class="blur-placeholder"><!-- --></div>
            <div class="single-page-block-form">
                <h3 class="text-center" style="    text-transform: initial;">
                    <i class="icmn-enter margin-right-10"></i>
                   Admin Warnermusic(TH)
                </h3>
                <br />
                <? if(isset($txt_error)){ ?>
                <h5 class="text-center" style="color: #c53838;">
                   <?=$txt_error?>
                </h5>
                <? } ?>
                <form id="form-validation" name="form-validation" method="POST" action="/Login/Checklogin">
                    <div class="form-group">
                        <input id="username"
                               class="form-control"
                               placeholder="Email or Username" value="<?=isset($txt_error)?$txt_user:""?>" name="username" required >
                    </div>
                    <div class="form-group">
                        <input id="password"
                               class="form-control password"
                               name="password"
                               type="password" placeholder="Password" required >
                    </div>
                  
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary width-150">Sign In</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- End Login Page -->

</div>

<!-- Page Scripts -->

<!-- End Page Scripts -->
</section>

<div class="main-backdrop"><!-- --></div>
</body