<!doctype html>
<html>
<body class="theme-default">



<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Form Elements -->
    <section class="panel">
      
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                  
                        <h4>Mood Form</h4>
                        <br />
                        <!-- Horizontal Form -->
                        <form method="post" id="artist_save" action="/Artist/Form_save_mood" enctype="multipart/form-data">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="form-control-label" for="l16">Thumbnail (750x750) .jpg .png <label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                      
                   			 <input id="file-1"  type="file" multiple=true name="fileToUpload_thumnail" >
                						</div>
                                  
                                </div>
                            </div>
                            
                            
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l15">Mood<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text"  class="form-control" placeholder="Mood name" name="mood_description" value="<?=isset($editdata[0]->mood_description)?$editdata[0]->mood_description:""?>">
                                </div>
                            </div>
                           <div class="form-group row"  style="display: none;">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Tags</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" value="<?=isset($editdata[0]->tags)?$editdata[0]->tags:""?>" class="form-control" placeholder="Tags index" name="tags">
                                      <span class="text-info">ตัวอย่าง หากมี index มากกว่า 1 ให้ระบุโดยการใส่ " , " คั่นระหว่างคำ popnews,rocknew</span>
                                </div>
                               
                            </div>
                           <div class="form-actions">
                                <div class="form-group row">
                                    <div class="col-md-1 col-md-offset-9">
                                        <button type="button"  onClick="$('#artist_save').submit();" class="btn width-150 btn-info">Submit</button>
                                        
                                    </div>
                                </div>
                            </div>
                             <input type="hidden" value="<?=isset($editdata[0]->mood_id)?$editdata[0]->mood_id:""?>" name="id" />
                        </form>
                        <!-- End Horizontal Form -->
                   
                </div>
            </div>
         
        </div>
    </section>
    <!-- End -->

</div>
</section>

<div class="main-backdrop"><!-- --></div>

</body>
</html>
<script>

    $(function(){

		var file1="<?=isset($editdata[0]->thumnail_path)?"/".$editdata[0]->thumnail_path:""?>";
        $("[data-toggle=tooltip]").tooltip();
		$(".music_menu").addClass("left-menu-list-opened").show();
		$("#mood_sub").css("color","#000");
   
     
		
		$("#file-1").fileinput({
		showUpload: false,
		showCaption: false,
		browseClass: "btn btn-info",
        allowedFileExtensions : ['jpg', 'png'],
		initialPreview: [ '<img src="'+file1+'" style="width:auto;height:160px;" class="kv-preview-data file-preview-image" >']	
    });
    
    
    })
</script>
	<style>.progress,.fileinput-cancel-button{display: none;}</style>