<body class="theme-default">


<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Tables -->
    <section class="panel">
        
        <div class="panel-body">
           
            <div class="row">
                <div class="col-lg-12">
                    <h4>Music Audio</h4>
                    <p><code>Music Audio / index</code></p>
                    <br />
                    
                    <div class="table-responsive margin-bottom-50">
                        <table class="table table-hover" style="    font-size: 13px;">
                            <thead>
                                <tr>
                                   <th>Type</th>
                                    <th>Artist</th>
                                    <th>Album</th>
                                    <th>Genres</th>
                                     <th>Nationality</th>
                                    
                                    <th>Music</th>
                                    <th>DateCreate</th>
                                  <th><a href="/Music/Form" class="btn btn-xs margin-inline" style="margin: 0px;"><i class="fa fa-plus"></i></a></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                   <th>Type</th>
                                    <th>Artist</th>
                                    <th>Album</th>
                                    <th>Genres</th>
                                    <th>Nationality</th>                                  
                                    <th>Music</th>
                                    <th>DateCreate</th>
                                   <th><a href="/Music/Form" class="btn btn-xs margin-inline" style="margin: 0px;"><i class="fa fa-plus"></i></a></th>
                                </tr>
                            </tfoot>
                            <tbody>
                              <?php 
                    			foreach($arr_data as $item)
									{
								?>
                                 <tr>
                                 <td>
									<?php
										$str_pic="fa fa-film";
									 if ($item->music_type=="Audio"){
										 $str_pic = "fa fa-file-sound-o";
									 }
										 	
								   ?>
                                  <a href="#" class="btn btn-icon btn-danger"><i class="<?=$str_pic?>"></i></a>
                                   </td>
                                 <td>
									<?=$item->artist_name?>
                                   </td>
                                    <td>
                                     <?=$item->album_name?>
                                    </td>
									<td>
                                 	<?=$item->Genres_name?>
                                   </td>
                                    <td >
                                    	<?=$item->nationality_name?>	
                                    </td>
                                    <td>
                                     <?=$item->track?> Tracks 
									
                                    </td>
                                     <td>
                                     <?=$item->createdatetime?>
                                     </td>
                                    
                                     <td>
                                   <a href="/Music/music_audio_form/<?=$item->id?>" target="_self"> 
                                   <span class="fa fa-pencil btn btn-xs "  title="Add"></span>&nbsp;
                                   </a>
 <a href="/Music/form_delete/<?=$item->id?>" target="_self">
                                    <span class="fa fa-close btn btn-xs " title="Delete"></span>
                                    </a>
                                   </td>
                                </tr>
                             <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Basic Tables  -->

</div>

<!-- Page Scripts -->
<script>

    $(function () {

        $("[data-toggle=tooltip]").tooltip();
		$(".music_menu").addClass("left-menu-list-opened").show();
		$("#audio_sub").css("color","#000");
		
    });

</script>
<!-- End Page Scripts -->
</section>

<div class="main-backdrop"><!-- --></div>

</body>