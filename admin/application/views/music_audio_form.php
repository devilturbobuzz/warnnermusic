<!doctype html>


<html>
<body class="theme-default">

<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Form Elements -->
    <section class="panel">
      
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                  
                        <h4>Music Audio</h4>
                        <br />
                        <!-- Horizontal Form -->
                        
                            
                             <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Music Type<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-2">
                                  <?php
									$text_musictype="";
									if(isset($editdata[0]->music_type)){
										$text_musictype=$editdata[0]->music_type;
									}
									
								  ?>
                                   <?=$text_musictype?>
                                </div>
							</div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Artist<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                  	<label class="form-control-label" style="float:left;" for="l0"> <?=isset($editdata[0]->id)?$editdata[0]->artist_name:""?></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Album<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                 <label class="form-control-label" style="float:left;" for="l0"> <?=isset($editdata[0]->id)?$editdata[0]->album_name:""?></label>
                                  
                                </div>
                            </div>
                             
                            <br/>
                          
                            <script type = "text/javascript" src = "<?=base_url()?>assets/js/jquery.form.js"></script>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Upload Audio ( mp3)<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                 <?php echo form_open_multipart('Music/do_upload', array('class' => 'upload-image-form'));?>
					
											<input type="file" multiple = "multiple" onchange="$('.upload_audio').click();" class = "form-control" name="uploadfile[]" size="20" style="height: 150px;" />
											
											<input type="submit" name = "submit" value="Upload" class = "btn btn-primary upload_audio" style="display: none;" />
										</form>
 								
                                </div>
                            </div>
                                <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" id="percent_val" for="l0"></label>
                                </div>
                                <div class="col-md-10">
                                  <progress style="margin-top: 6px;display: none;" class="progress progress-success progress-striped progress-animated" value="0" max="100"></progress>
                                </div>
                            </div>
                       
             <? foreach($arr_audio as $item_audio) { ?>
        	    	
                      <div class="box_music" >
						   <div class="form-group row " >
							 <div class="col-md-2">
											<label class="form-control-label" for="l0">Music </label>
												
									 </div>
									  <div class="col-md-8">
									    <div class="mediatec-cleanaudioplayer">
                                                        <ul data-theme="default">
                                                            <li data-title="<?=$item_audio->music_name?>" data-artist="<?=$editdata[0]->artist_name?>" data-type="mp3" data-url="<?=base_url().$item_audio->music_path?>" data-free="true"></li>
                                                        </ul>
                                                    </div>										
									 </div>	
									 	 
						</div>
						
			     <ul class="url_download<?=$item_audio->id?>" style=" list-style: none;margin-left: 52px;" >
                            <? if(isset($item_audio->url_subject)){  
								$arr_url_subject=explode(",",$item_audio->url_subject);
								$arr_url_download=explode(",",$item_audio->url_download);
								for($i=0;$i<count($arr_url_subject);$i++){ ?>
								<li class="li"><?=$i==0?"Download":""?> &nbsp;&nbsp;&nbsp;  <input type="text" style="display: inline-block; width: 200px; <?=$i==0?"":"margin-left: 70px;"?>"  class="form-control" placeholder="subject" name="url_subject<?=$item_audio->id?>[]" value="<?=$arr_url_subject[$i]?>">&nbsp;&nbsp;&nbsp;   <input type="text" style="    display: inline-block;width: 500px; "  class="form-control" placeholder="link download" name="url_download<?=$item_audio->id?>[]" value="<?=$arr_url_download[$i]?>">&nbsp;&nbsp;&nbsp; 
								<button type="button" onClick="<?=$i==0?"btn_url('".$item_audio->id."')":"$(this).closest('.li').remove()"?>" class="btn btn-icon <?=$i==0?"btn-info":"btn-danger"?> btn-info btn-rounded margin-inline">
								<i class="fa  <?=$i==0?"fa-plus":"fa-minus"?>" aria-hidden="true"></i></button>
								</li>
							<?		
								}
							?>
									
							<?	}else{ ?>
								<li class="li">Download &nbsp;&nbsp;&nbsp;  <input type="text" style="display: inline-block; width: 200px;"  class="form-control url_subject<?=$item_audio->id?>" placeholder="subject"  name="url_subject<?=$item_audio->id?>[]" value="<?=isset($editdata[0]->url_subject)?$editdata[0]->url_subject:""?>">&nbsp;&nbsp;&nbsp;   <input type="text" style="    display: inline-block;width: 500px; "  class="form-control url_download<?=$item_audio->id?>" placeholder="link download" name="url_download<?=$item_audio->id?>[]" value="<?=isset($editdata[0]->url_download)?$editdata[0]->url_download:""?>">&nbsp;&nbsp;&nbsp; <button type="button" onClick="btn_url('<?=$item_audio->id?>')" class="btn btn-icon btn-info btn-rounded margin-inline">
                               		<i class="fa fa-plus" aria-hidden="true"></i>
                               </button></li>
                            <?	} ?>
                            </ul>
                            
                           <footer class="panel-footer text-right bg-light lter ">
                             <button type="button" onClick="update_music('<?=$item_audio->id?>')" class="btn btn-info btn-s-xs">Update</button>
                        <button type="button" onClick="delete_music('<?=base64_encode($item_audio->music_name)?>','<?=$item_audio->music_path?>')" class="btn btn-danger btn-s-xs">Delete</button>
                       
                        
                    </footer>
				    </div>
          
           
             <? } ?>               
                         <div class="upload-image-messages"><!--ajax load--> </div>
                
                             <input type="hidden" value="<?=isset($editdata[0]->id)?$editdata[0]->id:""?>" name="id" />
                        
                        <!-- End Horizontal Form -->
                 
                      
                </div>
            </div>
         
        </div>
    </section>
    <!-- End -->

</div>
</section>




<div class="main-backdrop"><!-- --></div>

</body>
</html>
<script>

    $(function(){

		 if (!(typeof angular === 'undefined')) {
            $('.mediatec-cleanaudioplayer').cleanaudioplayer();
            $('.mediatec-cleanvideoplayer').cleanvideoplayer();
        }	
        $("[data-toggle=tooltip]").tooltip();
		$(".music_menu").addClass("left-menu-list-opened").show();
		$("#audio_sub").css("color","#000");  
		
    });
	jQuery(document).ready(function($) {

            var options = {
				
				xhr: function(){
       var xhr = new window.XMLHttpRequest();
       //Upload progress
       xhr.upload.addEventListener("progress", function(evt){
       if (evt.lengthComputable) {
         var percentComplete = evt.loaded / evt.total;
         //Do something with upload progress
        
		   $('.progress-striped').val(percentComplete*100).show();
		   $('#percent_val').html(Math.round(percentComplete*100)+" %").show();
		   
         }
       }, false);
     //Download progress
       xhr.addEventListener("progress", function(evt){
         if (evt.lengthComputable) {
           var percentComplete = evt.loaded / evt.total;
         //Do something with download progress
        //   console.log(percentComplete*100);
         }
       }, false);
       return xhr;
     },
                beforeSend: function(){
                    // Replace this with your loading gif image
		
                  
                },
                complete: function(response){
                    // Output AJAX response to the div container
					 $('.progress-striped').hide();
					$('#percent_val').hide();
                    $(".upload-image-messages").html(response.responseText);
                  location.reload();
                    
                }
            };  
            // Submit the form
            $(".upload-image-form").ajaxForm(options);  
		
            return false;
            
        });
	
	function update_music(id){
		var subject="";
		var download_url="";
		$('.url_subject'+id).each(function() {
			if($(this).val()){
				subject+=$(this).val()+",";
			}
		});
		$('.url_download'+id).each(function() {
			if($(this).val()){
				download_url+=$(this).val()+",";
			}
		});
		subject = subject.replace(/,+$/, '');
		download_url= download_url.replace(/,+$/, '');
		
		$.ajax({
         type: "POST",
         url:"<?php echo site_url();?>Music/update_url_music", 
         data: {url_subject: subject,id:id,url_download:download_url},
         dataType: "text",  
         cache:false,
         success: 
              function(data){
               alert("update download");
              }
          });
     		return false;
		
	}
	
   function update_link(id,type,getvalue){
	    $.ajax({
		  type: "POST",
		  url: "<?php echo site_url();?>Music/update_link",
	      data: {id: id,type:type,getvalue:getvalue.value},
		  dataType: "text",  
		  success: function(data){
		  }
      });
   }
	
	function delete_music(music_name,path){
		  $.ajax({
		  type: "POST",
		  url: "<?php echo site_url();?>Music/delete_music",
	      data: {music_name: music_name,path:path},
		  dataType: "text",  
		  success: function(data){
			 alert("Delete ");
			location.reload();
		  }
      });
	}
	function remove_url(v){
		$(v).closest('.li').remove();
	}
	 
	function btn_url(i){
		var url_id="url_subject"+i;
		var url_download="url_download"+i;
		var url_app="<li class='li'> &nbsp;&nbsp;&nbsp;  <input type='text' style='display: inline-block;margin-left: 70px; width: 200px;'  class='form-control "+url_id+"' placeholder='subject' name='"+url_id+"[]' value=''>&nbsp;&nbsp;&nbsp;   <input type='text' style='    display: inline-block;width: 500px; '  class='form-control "+url_download+"' placeholder='link download' name='url_download[]' value=''>&nbsp;&nbsp;&nbsp; <button type='button' onclick='remove_url(this)' class='btn btn-icon btn-danger  btn-rounded margin-inline'><i class='fa fa-minus' aria-hidden='true'></i></button></li>";
		 $(".url_download"+i).append(url_app);
	}

        function readURL(input,music_name,id) {

            if (input.files && input.files[0]) {
				 var reader = new FileReader();
                     reader.onload = function (e) {
                       $('#preview_img'+id).attr('src', e.target.result);
                     }
				
                reader.readAsDataURL(input.files[0]);
            }
			
			
			  var dataimg = new FormData();
			dataimg.append('id', id);
			dataimg.append('music_name', music_name);
			dataimg.append('files_thumnail', $("#files_thumnail"+id)[0].files[0]);
			$.ajax({
				 url:"<?php echo site_url();?>Music/update_thumnail_music", 
				type:"POST",
				 processData: false, // important
				  contentType: false, // important
				  dataType : 'json',
				data: dataimg,
				success: function(data) {
				}
			});

               
        }
    

</script>
	<style>
		.progress,.fileinput-cancel-button{display: block;}
		.box_music{
			    padding: 20px;
				-webkit-border-radius: 5px;
				border-radius: 5px;
				border: 1px solid #dfe4ed;
				margin-bottom: 35px;
				position: relative;
				background: #ffffff;
		}
</style>