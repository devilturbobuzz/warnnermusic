<!doctype html>
<html>
<body class="theme-default">



<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Form Elements -->
    <section class="panel">
      
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                  
                        <h4>Music Form</h4>
                        <br />
                        <!-- Horizontal Form -->
                        <form method="post" id="artist_save" action="/Music/Form_save" enctype="multipart/form-data">
                            
                             <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Music Type<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-2">
                                  <?php
									$text_musictype="";
									if(isset($editdata[0]->music_type)){
										$text_musictype=$editdata[0]->music_type;
									}
									
								  ?>
                                   <select class="form-control" id="l13" name="music_type">
                                        <option value="Audio">Audio</option>
                                        
                                         
                                    </select>
                                </div>
							</div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Artist<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                   <select class="form-control" id="l13" name="artist_id">
                                        <option value="">Select Artist</option>
                                         <?php 
                    						foreach($arr_data_artist as $item)
											{

												$select_artist="";
												 if(isset($editdata[0]->artist_id)){
													 if($editdata[0]->artist_id==$item->artist_id){
														 $select_artist="selected";
													 }
												 }
									       ?>
                                        <option value="<?=$item->artist_id?>" <?=$select_artist?>><?=$item->artist_name?></option>
                                        
                                        <? } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Album<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                   <select class="form-control" id="l13" name="album_id">
                                        <option value="">Select Artist</option>
                                         <?php 
                    						foreach($arr_data_album as $item)
											{

												$select_album="";
												 if(isset($editdata[0]->album_id)){
													 if($editdata[0]->album_id==$item->album_id){
														 $select_album="selected";
													 }
												 }
									       ?>
                                        <option value="<?=$item->album_id?>" <?=$select_album?>><?=$item->album_name?></option>
                                        
                                        <? } ?>
                                    </select>
                                </div>
                            </div>
                          
                           
                           <div class="form-actions">
                                <div class="form-group row">
                                    <div class="col-md-1 col-md-offset-9">
                                        <button type="button"  onClick="$('#artist_save').submit();" class="btn width-150 btn-info">Submit</button>
                                        
                                    </div>
                                </div>
                            </div>
                             <input type="hidden" value="<?=isset($editdata[0]->id)?$editdata[0]->id:""?>" name="id" />
                        </form>
                        <!-- End Horizontal Form -->
                   
                </div>
            </div>
         
        </div>
    </section>
    <!-- End -->

</div>
</section>

<div class="main-backdrop"><!-- --></div>

</body>
</html>
<script>

    $(function(){


        $("[data-toggle=tooltip]").tooltip();
		$(".music_menu").addClass("left-menu-list-opened").show();
		$("#music").css("color","#000");  
    });
	

    
	
</script>
	<style>.progress,.fileinput-cancel-button{display: none;}</style>