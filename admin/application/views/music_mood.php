<body class="theme-default">


<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Tables -->
    <section class="panel">
        
        <div class="panel-body">
           
            <div class="row">
                <div class="col-lg-12">
                    <h4>Music</h4>
                    <p><code>Music / index</code></p>
                    <br />
                    
                    <div class="table-responsive margin-bottom-50">
                        <table class="table table-hover" style="    font-size: 13px;">
                            <thead>
                                <tr>
                                    <th>Artist</th>
                                    <th>Album</th>
                                    <th>Genres of music</th>
                                    <th>Nationality</th>
                                    <th>Music</th>
                                    <th>DateCreate</th>
                                    <th><a href="Music/Form" class="btn btn-xs margin-inline" style="margin: 0px;"><i class="fa fa-plus"></i></a></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Artist</th>
                                    <th>Album</th>
                                    <th>Genres of music</th>
                                    <th>Nationality</th>
                                    <th>Music</th>
                                    <th>DateCreate</th>
                                    <th><a href="Music/Form" class="btn btn-xs margin-inline" style="margin: 0px;"><i class="fa fa-plus"></i></a></th>
                                </tr>
                            </tfoot>
                            <tbody>
                              
                                 <tr>
                                 <td>
									
                                   </td>
                                    <td>
                                     
                                    </td>
									<td>
                                 	
                                   </td>
                                    <td >
                                    		
                                    </td>
                                    <td>	</td>
                                     <td></td>
                                    
                                     <td>
                                   <a href="#" target="_self"> 
                                   <span class="fa fa-pencil btn btn-xs "  title="Edit"></span>&nbsp;
                                   </a>
                                    <a href="#" target="_self">
                                    <span class="fa fa-close btn btn-xs " title="Delete"></span>
                                    </a>
                                   </td>
                                </tr>
                             
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Basic Tables  -->

</div>

<!-- Page Scripts -->
<script>

    $(function () {

        $("[data-toggle=tooltip]").tooltip();
		$(".music_menu").addClass("left-menu-list-opened").show();
		$("#musicmood_sub").css("color","#000");
		
    });

</script>
<!-- End Page Scripts -->
</section>

<div class="main-backdrop"><!-- --></div>

</body>