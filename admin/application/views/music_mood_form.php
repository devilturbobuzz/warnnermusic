<!doctype html>
<html>
<body class="theme-default">



<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Form Elements -->
    <section class="panel">
      
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                  
                        <h4>Music Form</h4>
                        <br />
                        <!-- Horizontal Form -->
                        <form method="post" id="artist_save" action="/Artist/Form_save_album" enctype="multipart/form-data">
                            
                             <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Data Type<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-2">
                                   <select class="form-control" id="l13" name="Type">
                                        <option value="">Select Type</option>
                                        <option value="Audio">Audio</option>
                                        <option value="Video">Video</option>
                                         
                                    </select>
                                </div>
							</div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Artist<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                   <select class="form-control" id="l13" name="Artist">
                                        <option value="">Select Artist</option>
                                         <?php 
                    						foreach($data_artist as $item)
											{

												$select_artist="";
												 if(isset($data_album[0]->artist_id)){
													 if($data_album[0]->artist_id==$item->artist_id){
														 $select_artist="selected";
													 }
												 }
									       ?>
                                        <option value="<?=$item->artist_id?>" <?=$select_artist?>><?=$item->artist_name?></option>
                                        
                                        <? } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Album<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                   <select class="form-control" id="l13" name="Artist">
                                        <option value="">Select Artist</option>
                                         <?php 
                    						foreach($data_artist as $item)
											{

												$select_artist="";
												 if(isset($data_album[0]->artist_id)){
													 if($data_album[0]->artist_id==$item->artist_id){
														 $select_artist="selected";
													 }
												 }
									       ?>
                                        <option value="<?=$item->artist_id?>" <?=$select_artist?>><?=$item->artist_name?></option>
                                        
                                        <? } ?>
                                    </select>
                                </div>
                            </div>
                             
                            
                             <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l15">Remark</label>
                                </div>
                                <div class="col-md-10">
                                    <textarea class="form-control"   rows="10" id="l15" name="Remark"></textarea>
                                </div>
                            </div>
                             <div class="form-group row" style="display: none;">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Tags</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" value="<?=isset($editdata[0]->tags)?$editdata[0]->tags:""?>" class="form-control" placeholder="Tags index" name="tags">
                                      <span class="text-info">ตัวอย่าง หากมี index มากกว่า 1 ให้ระบุโดยการใส่ " , " คั่นระหว่างคำ popnews,rocknew</span>
                                </div>
                               
                            </div>
                          
                           <div class="form-actions">
                                <div class="form-group row">
                                    <div class="col-md-1 col-md-offset-9">
                                        <button type="button"  onClick="$('#artist_save').submit();" class="btn width-150 btn-info">Submit</button>
                                        
                                    </div>
                                </div>
                            </div>
                              <input type="hidden" value="<?=isset($data_album[0]->album_id)?$data_album[0]->album_id:""?>" name="id" />
                        </form>
                        <!-- End Horizontal Form -->
                   
                </div>
            </div>
         
        </div>
    </section>
    <!-- End -->

</div>
</section>

<div class="main-backdrop"><!-- --></div>

</body>
</html>
<script>

    $(function(){

var file1="<?=isset($data_album[0]->thumnail_path)?"/".$data_album[0]->thumnail_path:""?>";
        $("[data-toggle=tooltip]").tooltip();
		$(".music_menu").addClass("left-menu-list-opened").show();
		$("#musicmood_sub").css("color","#000");  
    });
	
	function uploadSharepic(){
		var data = new FormData();
		jQuery.each(jQuery('#userFile')[0].files, function(i, file) {
			data.append('file-'+i, file);
		});
	 $.ajax({
				   type: "POST",
		 		   data: data,
				   url: "/Music/do_upload",
				   cache: false,
		 			success: function(msg){
					
				   }
				 });
    
	}
</script>
	<style>.progress,.fileinput-cancel-button{display: none;}</style>