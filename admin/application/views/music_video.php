<body class="theme-default">


<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Tables -->
    <section class="panel">
        
        <div class="panel-body">
           
            <div class="row">
                <div class="col-lg-12">
                    <h4>Music Video</h4>
                    <p><code>Video / index</code></p>
                    <br />
                    
                    <div class="table-responsive margin-bottom-50">
                        <table class="table table-hover" style="    font-size: 13px;">
                            <thead>
                                <tr>
                                     <th>Video</th>
                                    <th>Subject</th>
                                    <th >Description</th>
                                    <th>Date</th>
                                  
                                    <th><a href="/Music/Form_video" class="btn btn-xs margin-inline" style="margin: 0px;"><i class="fa fa-plus"></i></a></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                     <th>Video</th>
                                    <th>Subject</th>
                                    <th>Description</th>
                                    <th>Date</th>
                                   
                                    <th><a href="/Music/Form_video" class="btn btn-xs margin-inline" style="margin: 0px;"><i class="fa fa-plus"></i></a></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php 
                    			foreach($arr_data as $item)
									{
								?>
                                 <tr>
                                 <td>
                               <a href="https://www.youtube.com/watch?v=<?=$item->youtubeid?>" target="_blank" > <img src="http://img.youtube.com/vi/<?=$item->youtubeid?>/default.jpg"></a>
									 
                                   </td>
                                 <td>
									<?=$item->subject?>
                                   </td>
                                    <td width="40%">
                                     <?php
										$str_subject=$item->description;
									 if (strlen($str_subject) > 500){
										echo $str_subject = substr($str_subject, 0, 500) . '...';
									 }else{
										echo $str_subject;
									 }
										 	
								  ?>
                                  
                                    </td>
									<td>
                                 	<?=$item->released?>
                                   </td>
                                   
                                    
                                    
                                     <td>
                                   <a href="/Music/music_video_form/<?=$item->id?>" target="_self"> 
                                   <span class="fa fa-pencil btn btn-xs "  title="Edit"></span>&nbsp;
                                   </a>
                                    <a href="/Music/form_delete_video/<?=$item->id?>" target="_self">
                                    <span class="fa fa-close btn btn-xs swal-btn-warning" data-id="<?=$item->id?>" title="Delete"></span>
                                    </a>
                                   </td>
                                </tr>
                             <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Basic Tables  -->

</div>

<!-- Page Scripts -->
<script>

    $(function () {

        $("[data-toggle=tooltip]").tooltip();
		$(".home_menu").addClass("left-menu-list-opened").show();
		$("#vedio_sub").css("color","#000");
		$('.swal-btn-warning').click(function(e){
			var data_delete=$(this).data("id");
            e.preventDefault();
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this file!",
                type: "warning",
                showCancelButton: true,
                cancelButtonClass: "btn-default",
                confirmButtonClass: "btn-warning",
                confirmButtonText: "Remove",
                closeOnConfirm: false
            },
            function(){
                swal({
                    title: "Deleted!",
                    text: "File has been deleted",
                    type: "success",
                    confirmButtonClass: "btn-success"
                });
				
				window.open("/Music/form_delete_video/"+data_delete,"_self");
            });
			
        });
    });

</script>
<!-- End Page Scripts -->
</section>

<div class="main-backdrop"><!-- --></div>

</body>