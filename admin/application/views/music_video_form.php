<!doctype html>
<html>
<body class="theme-default">


<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Form Elements -->
    <section class="panel">
      
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                  
                        <h4>Music Video Form</h4>
                        <br />
                        <!-- Horizontal Form -->
                        <form method="post" id="artist_save" action="/Music/Form_save_video" enctype="multipart/form-data">
                            
                             
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Subject<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                  <input type="text" value="<?=isset($editdata[0]->subject)?$editdata[0]->subject:""?>" class="form-control" placeholder="subject video" name="subject" >
                                  <span class="error " id="subject" ></span>
                                </div>
                            </div>
                             <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l15">Description</label>
                                </div>
                                <div class="col-md-10">
                                    <textarea class="form-control"   rows="10" id="l15" name="description"><?=isset($editdata[0]->description)?$editdata[0]->description:""?></textarea>
                                    <span class="error " id="description" ></span>
                                </div>
                            </div>
                            
                              <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">date<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                   
                                       <input type='text' name="released" value="<?=isset($editdata[0]->released)?$editdata[0]->released:""?>" class="form-control" id='datetimepicker' />
                                      <small class="text-muted">Date mask input: วว/ดด/ปปปป</small>
                                </div>
                            </div>
                            
                            <div class="form-group row" >
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Youtube video id<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" value="<?=isset($editdata[0]->youtubeid)?$editdata[0]->youtubeid:""?>"  class="form-control" placeholder="youtubeid" name="youtubeid">
                                    <span class="text-info">ตัวอย่าง https://www.youtube.com/watch?v=DHrmJBt31h8 ให้ระบุแค่ DHrmJBt31h8</span>
                                     <span class="error " id="youtubeid" ></span>
                                </div>
                            </div>
                            
                         
                          
                           <div class="form-actions">
                                <div class="form-group row">
                                    <div class="col-md-1 col-md-offset-9">
                                        <button type="button"  onClick="checkvalidate()" class="btn width-150 btn-info">Submit</button>
                                        
                                    </div>
                                </div>
                            </div>
                             <input type="hidden" value="<?=isset($editdata[0]->id)?$editdata[0]->id:""?>" name="id" />
                        </form>
                        <!-- End Horizontal Form -->
                   
                </div>
            </div>
         
        </div>
    </section>
    <!-- End -->

</div>
</section>

<div class="main-backdrop"><!-- --></div>

</body>
</html>
<script>

    $(function(){
$('#datetimepicker').datetimepicker({
                 format: 'DD/MM/YYYY'
           });

        $("[data-toggle=tooltip]").tooltip();
		$(".home_menu").addClass("left-menu-list-opened").show();
		$("#vedio_sub").css("color","#000");
		$('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'L'
        });
    });
	function checkvalidate(){
	
		var status=true;
		if($( "input[name='youtubeid']").val()==""){
			$('#youtubeid').html("youtubeid require");
			$( "input[name='youtubeid']" ).css("border-color","#fb434a");
			status=false;
		}
		if($( "input[name='subject']").val()==""){
			$('#url').html("subject require");
			$( "input[name='subject']" ).css("border-color","#fb434a");
			status=false;
		}
		if($( "input[name='released']").val()==""){
			$('#released').html("released require");
			$( "input[name='released']" ).css("border-color","#fb434a");
			status=false;
		}
		if($( "input[name='description']").val()==""){
			$('#description').html("description require");
			$( "input[name='description']" ).css("border-color","#fb434a");
			status=false;
		}
		
		if(status==true){
			$('#artist_save').submit();
		}else{
			alert("please check your input data");
			
		}
		return status;
	}

    
	
</script>
	<style>.progress,.fileinput-cancel-button{display: none;}</style>