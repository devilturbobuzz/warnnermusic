<!doctype html>
<html>
<body class="theme-default">



<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Form Elements -->
    <section class="panel">
      
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                  
                        <h4>Category Form</h4>
                        <br />
                        <!-- Horizontal Form -->
                        <form method="post" id="artist_save" action="/Artist/Form_save_nationality" enctype="multipart/form-data">
                            
                            
                            
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l15">Subject<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text"  class="form-control" placeholder="nationality name" name="nationality_name" value="<?=isset($editdata[0]->nationality_name)?$editdata[0]->nationality_name:""?>">
                                     <span class="error " id="nationality_name" ></span>
                                </div>
                            </div>
                               <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Date<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                   
                                       <input type='text' name="Release_on"  value="<?=isset($editdata[0]->Release_on)?$editdata[0]->Release_on:""?>" class="form-control" id='datetimepicker'  />
                                     <small class="text-muted">Date mask input: วว/ดด/ปปปป</small>
                                   
                                </div>
                                
                            </div>
                             
                           <div class="form-actions">
                                <div class="form-group row">
                                    <div class="col-md-1 col-md-offset-9">
                                        <button type="button"  onClick="checkvalidate()" class="btn width-150 btn-info">Submit</button>
                                        
                                    </div>
                                </div>
                            </div>
                             <input type="hidden" value="<?=isset($editdata[0]->id)?$editdata[0]->id:""?>" name="id" />
                        </form>
                        <!-- End Horizontal Form -->
                   
                </div>
            </div>
         
        </div>
    </section>
    <!-- End -->

</div>
</section>

<div class="main-backdrop"><!-- --></div>

</body>
</html>
<script>

    $(function(){
		$('#datetimepicker').datetimepicker({
                 format: 'DD/MM/YYYY'
           });
        $("[data-toggle=tooltip]").tooltip();
		$(".artist_menu").addClass("left-menu-list-opened").show();
		$("#Nationality_sub").css("color","#000");
    });
	function checkvalidate(){
	
		var status=true;
		if($( "input[name='nationality_name']").val()==""){
			$('#nationality_name').html("nationality require");
			$( "input[name='nationality_name']" ).css("border-color","#fb434a");
			status=false;
		}
	
		if(status==true){
			$('#artist_save').submit();
		}else{
			alert("please check your input data");
			
		}
		return status;
	}
</script>
	<style>.progress,.fileinput-cancel-button{display: none;}</style>