<body class="theme-default">

<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Tables -->
    <section class="panel">
        
        <div class="panel-body">
           
            <div class="row">
                <div class="col-lg-12">
                    <h4>News</h4>
                    <p><code>News / index</code></p>
                   
                     <div class="col-md-2">
                        <label class="form-control-label" style="float: left;" >Filter News</label>
					</div>
                
                    <div class="col-md-2">
                    <form method="post" id="newsearch" action="/News/search" >
                     <select class="form-control"  name="keyword" onChange="$('#newsearch').submit();">
                            <option <?=isset($val_search) && $val_search=="ALL"?"SELECTED":""?> value="ALL">All News</option>
                            <option <?=isset($val_search) && $val_search=="Y"?"SELECTED":""?> value="Y">Highlight</option>
                            
					</select>
						</form>
					</div>
                    <br />   <br />   <br />
                    <div class="table-responsive margin-bottom-50">
                        <table class="table table-hover" style="    font-size: 13px;">
                            <thead>
                                <tr>
                                    <th>Subject</th>
                                    <th>Date</th>
                                   
                                    <th><a href="News/Form" class="btn btn-xs margin-inline" style="margin: 0px;"><i class="fa fa-plus"></i></a></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Subject</th>
                                  
                                   
                                    <th>Date</th>
                                   
                                    <th><a href="News/Form" class="btn btn-xs margin-inline" style="margin: 0px;"><i class="fa fa-plus"></i></a></th>
                                </tr>
                            </tfoot>
                            <tbody>
                               <?php 
                    			foreach($newsdata as $item)
									{
								?>
                                 <tr>
                                 <td>
                                 <?php
									
										echo $item->subject_news;
									 
										 	
								  ?>
									
                                   </td>
                                   
								
                                   
                                    <td><?=$item->date_news?></td>
                                    
                                    <td><!--<a href="/News/save_approve/<?=$item->id?>/<?=$item->approve=="Y"?"N":"Y"?>" target="_self"><span class="fa fa-cloud-upload btn btn-xs " title="approve"></span></a>-->
                                     <a href="/News/save_highlight/<?=$item->id?>/<?=$item->highlight_news=="Y"?"N":"Y"?>" target="_self">
                                      <span class=" fa <?=$item->highlight_news=="Y"?"fa fa-flag":"fa-flag-o"?> btn btn-xs " title="Add highlight"></span>&nbsp;
                                      </a>
                                   <a href="/News/Edit/<?=$item->id?>" target="_self"> 
                                   <span class="fa fa-pencil btn btn-xs "  title="Edit"></span>&nbsp;
                                   </a>
                                    <a href="/News/form_delete/<?=$item->id?>" target="_self">
                                    <span class="fa fa-close btn btn-xs swal-btn-warning" data-id="<?=$item->id?>" title="Delete"></span>
                                    </a>
                                   </td>
                                </tr>
                              <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Basic Tables  -->

</div>

<!-- Page Scripts -->
<script>

    $(function () {

        $("[data-toggle=tooltip]").tooltip();
		$("#news_menu").addClass("left-menu-list-active");
		$('.swal-btn-warning').click(function(e){
			var data_delete=$(this).data("id");
            e.preventDefault();
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this file!",
                type: "warning",
                showCancelButton: true,
                cancelButtonClass: "btn-default",
                confirmButtonClass: "btn-warning",
                confirmButtonText: "Remove",
                closeOnConfirm: false
            },
            function(){
                swal({
                    title: "Deleted!",
                    text: "File has been deleted",
                    type: "success",
                    confirmButtonClass: "btn-success"
                });
				
				window.open("/News/form_delete/"+data_delete,"_self");
            });
			
        });
    });

</script>
<!-- End Page Scripts -->
</section>

<div class="main-backdrop"><!-- --></div>

</body>