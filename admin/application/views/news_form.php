<!doctype html>
<html>
<body class="theme-default">


<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Form Elements -->
    <section class="panel">
      
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="margin-bottom-50">
                        <h4>News Form</h4>
                        <br />
                        <!-- Horizontal Form -->
                        <form method="post" id="news_save" action="/News/Form_save" enctype="multipart/form-data">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="form-control-label file1" for="l16">Thumbnail (941x611) .jpg .png <label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-9 ">
                                    <div class="form-group " >
                      
                   			 <input id="file-1"  type="file"  multiple=true name="fileToUpload_thumnail" >
                   			 <span class="error " id="file1" ></span>
                						</div>
                                  
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3 file2">
                                    <label class="form-control-label" for="l16">Picture highlight (340x450) .jpg .png</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group file2">
                   
                			     <input id="file-2" type="file"  multiple=true name="fileToUpload_highlight" >
                                   <span class="error " id="file2" ></span>
                                </div>
                            </div>
                             </div>
                           <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Date<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                   
                                       <input type='text' name="date_stamp"  value="<?=isset($editdata[0]->date_news)?$editdata[0]->date_news:""?>" class="form-control" id='datetimepicker'  />
                                     <small class="text-muted">Date mask input: วว/ดด/ปปปป</small>
                                   
                                </div>
                                
                            </div>
                          
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Subject<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                  
                                    <input type="text"  value="<?=isset($editdata[0]->subject_news)?$editdata[0]->subject_news:""?>" class="form-control" placeholder="Subject news" name="Subject">
                                     <span class="error " id="Subject" ></span>
                                </div>
                            </div>
							
                         
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l15">Description </label>
                                </div>
                                <div class="col-md-10">
                                    <textarea class="form-control" id="editor1"    rows="7" id="l15" name="Description"><?=isset($editdata[0]->description_news)?$editdata[0]->description_news:""?></textarea>
                                    
                                </div>
                            </div>
                          
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Tags</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" value="<?=isset($editdata[0]->tags)?$editdata[0]->tags:""?>" class="form-control" placeholder="Tags index" name="tags">
                                      <span class="text-info">ตัวอย่าง หากมี index มากกว่า 1 ให้ระบุโดยการใส่ " , " คั่นระหว่างคำ popnews,rocknew</span>
                                </div>
                               
                            </div>
                          
                          
                           <div class="form-actions">
                                <div class="form-group row">
                                    <div class="col-md-1 col-md-offset-9">
                                        <button type="button"  onClick="checkvalidate()"class="btn width-150 btn-info">Submit</button>
                                        
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" value="<?=isset($editdata[0]->id)?$editdata[0]->id:""?>" name="id" />
                        </form>
                        <!-- End Horizontal Form -->
                   
                </div>
            </div>
         
        </div>
    </section>
    <!-- End -->

</div>
</section>

<div class="main-backdrop"><!-- --></div>

</body>
</html>

<script>

    $(function(){
		$('#datetimepicker').datetimepicker({
                 format: 'DD/MM/YYYY'
           });
var file1="<?=isset($editdata[0]->head_picture_path)?"/".$editdata[0]->head_picture_path:""?>";
var file2="<?=isset($editdata[0]->picture_highlight)?"/".$editdata[0]->picture_highlight:""?>";
      
		$("#news_menu").addClass("left-menu-list-active");
		
		$("#file-1").fileinput({
		showUpload: false,
		showCaption: false,
		browseClass: "btn btn-info",
        allowedFileExtensions : ['jpg', 'png'],
		initialPreview: [ '<img src="'+file1+'" style="width:auto;height:160px;" class="kv-preview-data file-preview-image" >']
       
	});
		$("#file-2").fileinput({
		showUpload: false,
		showCaption: false,
		browseClass: "btn btn-info",
        allowedFileExtensions : ['jpg', 'png'],
		initialPreview: [ '<img src="'+file2+'" style="width:auto;height:160px;" class="kv-preview-data file-preview-image" >']
       
	});
 	CKEDITOR.replace( 'editor1' );
	CKEDITOR.config.width="100%";
	CKEDITOR.config.height="80%";
           
    
    
    });
	function checkvalidate(){
		var file1="<?=isset($editdata[0]->head_picture_path)?"/".$editdata[0]->head_picture_path:""?>";
		var file2="<?=isset($editdata[0]->picture_highlight)?"/".$editdata[0]->picture_highlight:""?>";
		var status=true;
		if($( "input[name='date_stamp']").val()==""){
			$('#date_stamp').html("Date of News require");
			$( "input[name='date_stamp']" ).css("border-color","#fb434a");
			status=false;
		}
	
		if($( "#file-1").val()=="" && file1==""){
			$('#file1').html("Thumbnail  require");
			$( ".file1" ).css("color","#fb434a");
			status=false;
		}
		if($( "#file-2").val()=="" && file2==""){
			$('#file2').html("Picture highlight  require");
			$( ".file2" ).css("color","#fb434a");
			status=false;
		}
		
		if($( "input[name='Subject']").val()==""){
			$('#Subject').html("Subject require");
			$( "input[name='Subject']" ).css("border-color","#fb434a");
			status=false;
		}
		if(status==true){
			$('#news_save').submit();
		}else{
			alert("please check your input data");
			$( "input[name='date_stamp']").focus()
		}
		return status;
	}
</script>
	<style>.progress,.fileinput-cancel-button{display: none;}</style>