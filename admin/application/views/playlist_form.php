<!doctype html>
<html>
<body class="theme-default">


<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Form Elements -->
    <section class="panel">
      
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="margin-bottom-50">
                        <h4>Playlist</h4>
                        <br />
                        <!-- Horizontal Form -->
                        <form method="post" id="news_save" action="/Playlist/Form_save" enctype="multipart/form-data">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="form-control-label" for="l16">Thumbnail (1359 x 500) .jpg .png <label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                      
                   			 <input id="file-1"  type="file"  multiple=true name="fileToUpload_thumnail" >
                						</div>
                                  
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Genre & Mood<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10 checkbox">
                                   <? if(isset($editdata[0]->id_playlist_cat)){
										$arr_playlist_cat=explode(",",$editdata[0]->id_playlist_cat);
									} ?>
                             	<? foreach($arr_playlist as $item) { ?>
											
											<?
												$select_artist="";							  
												 if(isset($editdata[0]->id_playlist_cat)){
													 for($i=0;$i<count($arr_playlist_cat);$i++){
														 if($arr_playlist_cat[$i]==$item->id){
															  $select_artist="checked";
														 }
													 }
													
												 }
													 
													
											 ?>
										  <label>	
										<input type="checkbox" <?=$select_artist?> name="id_playlist_cat[]"  value="<?=$item->id?>"/>&nbsp;<?=$item->subject?>&nbsp;&nbsp;</label>
											  
											  
											  <? } ?>

                                    
                                </div>
                            </div>
                           
                          
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Subject<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" value="<?=isset($editdata[0]->subject)?$editdata[0]->subject:""?>" class="form-control" placeholder="Subject" name="Subject">
                                </div>
                            </div>
							
                         
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" >Description </label>
                                </div>
                                <div class="col-md-10">
                                    <textarea class="form-control"   rows="3" id="editor1" name="Description"><?=isset($editdata[0]->description)?$editdata[0]->description:""?></textarea>
                                </div>
                            </div>
                           
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" >Playlist Music </label>
                                </div>
                                <div class="col-md-10">
                                    <textarea class="form-control"   rows="3" id="editor2" name="music_description"><?=isset($editdata[0]->music_description)?$editdata[0]->music_description:""?></textarea>
                                </div>
                            </div>
							  <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" >Date<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" value="<?=isset($editdata[0]->release_on)?$editdata[0]->release_on:""?>" class="form-control" id='datetimepicker' placeholder="Date" name="date_stamp">
                                </div>
                            </div>
                             <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Tags</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" value="<?=isset($editdata[0]->tags)?$editdata[0]->tags:""?>" class="form-control" placeholder="Tags index" name="tags">
                                      <span class="text-info">ตัวอย่าง หากมี index มากกว่า 1 ให้ระบุโดยการใส่ " , " คั่นระหว่างคำ popnews,rocknew</span>
                                </div>
							</div>
                           <ul class="url_download" style=" list-style: none;margin-left: 52px;" >
                            <? if(isset($editdata[0]->url_subject)){  
								$arr_url_subject=explode(",",$editdata[0]->url_subject);
								$arr_url_download=explode(",",$editdata[0]->url_download);
								for($i=0;$i<count($arr_url_subject);$i++){ ?>
								<li class="li"><?=$i==0?"Service":""?><select style="display: inline-block; width: 250px; <?=$i==0?"margin-left: 21px;":"margin-left: 68px;"?>"  class="form-control"  name="url_subject[]">
                               <? foreach($arr_gallery as $item) { ?>
                               <?
									   $select_str="";
								   if($arr_url_subject[$i]==$item->id){
									   $select_str="selected";
								   }
							   ?>
								  <option <?=$select_str?> data-class="avatar" value="<?=$item->id?>" style="background-image:url(<?=$item->thumnail_path?>);"><?=$item->name?></option>
								<? } ?>  
								</select>  &nbsp;&nbsp;&nbsp;   
                            	<input type="text" style="    display: inline-block;width: 500px; "  class="form-control" placeholder="link download" name="url_download[]" value="<?=$arr_url_download[$i]?>">&nbsp;&nbsp;&nbsp; <button type="button" onClick="<?=$i==0?"btn_url()":"$(this).closest('.li').remove()"?>" class="btn btn-icon <?=$i==0?"btn-info":"btn-danger"?> btn-info btn-rounded margin-inline">
                             		
                              		
                               		<i class="fa  <?=$i==0?"fa-plus":"fa-minus"?>" aria-hidden="true"></i>
                               </button></li>
							<?		
								}
							?>
									
							<?	}else{ ?>
								<li class="li">Service &nbsp;&nbsp;&nbsp;  
                              		<select style="display: inline-block; width: 250px; "  class="form-control"  name="url_subject[]">
                               <? foreach($arr_gallery as $item) { ?>
                               <?
									   $select_str="";
								   if($arr_url_subject[$i]==$item->id){
									   $select_str="selected";
								   }
							   ?>
								  <option <?=$select_str?> data-class="avatar" value="<?=$item->id?>" style="background-image:url(<?=$item->thumnail_path?>);"><?=$item->name?></option>
								<? } ?>  
								</select>&nbsp;&nbsp;&nbsp;   
                              		<input type="text" style="    display: inline-block;width: 500px; "  class="form-control" placeholder="link download" name="url_download[]" value="<?=isset($editdata[0]->url_download)?$editdata[0]->url_download:""?>">&nbsp;&nbsp;&nbsp; <button type="button" onClick="btn_url()" class="btn btn-icon btn-info btn-rounded margin-inline">
                               		<i class="fa fa-plus" aria-hidden="true"></i>
                               </button></li>
                            <?	} ?>
                            </ul>
                           
                           <div class="form-actions">
                                <div class="form-group row">
                                    <div class="col-md-1 col-md-offset-9">
                                        <button type="button"  onClick="$('#news_save').submit();"class="btn width-150 btn-info">Submit</button>
                                        
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" value="<?=isset($editdata[0]->id)?$editdata[0]->id:""?>" name="id" />
                        </form>
                        <!-- End Horizontal Form -->
                   
                </div>
            </div>
         
        </div>
    </section>
    <!-- End -->

</div>
</section>
<div id="picture_get" style="display: none;">
	<select style='display: inline-block; width: 250px;margin-left: 68px;'  class='form-control'  name='url_subject[]'>
       <? foreach($arr_gallery as $item) { ?>
         <option  value='<?=$item->id?>' style='background-image:url(<?=$item->thumnail_path?>);'><?=$item->name?></option>
		<? } ?>  
	</select>
</div>
<div class="main-backdrop"><!-- --></div>

</body>
</html>
<script>

    $(function(){
		$(".playlist_menu").addClass("left-menu-list-opened").show();
		$("#playlist").css("color","#000");
		
var file1="<?=isset($editdata[0]->head_picture_path)?"/".$editdata[0]->head_picture_path:""?>";

     
		$('#datetimepicker').datetimepicker({
                 format: 'DD/MM/YYYY'
           });
		
		$("#file-1").fileinput({
		showUpload: false,
		showCaption: false,
		browseClass: "btn btn-info",
        allowedFileExtensions : ['jpg', 'png'],
		initialPreview: [ '<img src="'+file1+'" style="width:auto;height:160px;" class="kv-preview-data file-preview-image" >']
       
	});
  
		CKEDITOR.replace('editor1', { height: 250 });
		CKEDITOR.replace('editor2', { height: 550 });
});

function Getmusic(artist,loop){
			$('#music'+loop).html(" <option value=''>Select Music</option>");
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('Playlist/getmusic'); ?>", 
				data: {artisti_id: artist},
				dataType:"json",
				success: function(states){
				
					   $.each(states,function(key,val){
							var opt = $('<option />');
							opt.val(val.id);
							opt.text(val.music_name);
						   $('#music'+loop).append(opt);
					   });
					var playlist_select=$("#playlist_id"+loop).val();
					console.log(playlist_select);
					$('#music'+loop+' option[value='+playlist_select+']').attr('selected','selected');
					
				},
			});
		}
			
function btn_url(){
		var picture_get=$("#picture_get").html();
		var url_app="<li class='li'> &nbsp;&nbsp;&nbsp;  "+picture_get+"&nbsp;&nbsp;&nbsp;   <input type='text' style='    display: inline-block;width: 500px; '  class='form-control' placeholder='link download' name='url_download[]' value=''>&nbsp;&nbsp;&nbsp; <button type='button' onclick='remove_url(this)' class='btn btn-icon btn-danger  btn-rounded margin-inline'><i class='fa fa-minus' aria-hidden='true'></i></button></li>";
		 $(".url_download").append(url_app);
	}
	 function remove_url(v){
		$(v).closest('.li').remove();
	}
</script>
	<style>.progress,.fileinput-cancel-button{display: none;}</style>