<!doctype html>
<html>
<body class="theme-default">


<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Form Elements -->
    <section class="panel">
      
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="margin-bottom-50">
                        <h4>Genre & Mood</h4>
                        <br />
                        <!-- Horizontal Form -->
                        <form method="post" id="news_save" action="/Playlist/Form_save_theme" enctype="multipart/form-data">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="form-control-label" for="l16">Thumbnail (1359 x 500) .jpg .png <label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                      
                   			 <input id="file-1"  type="file"  multiple=true name="fileToUpload_thumnail" >
                						</div>
                                  
                                </div>
                            </div>
                           
                          
                          
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Subject<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" value="<?=isset($editdata[0]->subject)?$editdata[0]->subject:""?>" class="form-control" placeholder="subject" name="subject">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Date<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                   
                                       <input type='text' name="Release_on"  value="<?=isset($editdata[0]->release_on)?$editdata[0]->release_on:""?>" class="form-control" id='datetimepicker'  />
                                    <small class="text-muted">Date mask input: วว/ดด/ปปปป</small>
                                   
                                </div>
                                
                            </div>
						

                           <div class="form-actions">
                                <div class="form-group row">
                                    <div class="col-md-1 col-md-offset-9">
                                        <button type="button"  onClick="$('#news_save').submit();"class="btn width-150 btn-info">Submit</button>
                                        
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" value="<?=isset($editdata[0]->id)?$editdata[0]->id:""?>" name="id" />
                        </form>
                        <!-- End Horizontal Form -->
                   
                </div>
            </div>
         
        </div>
    </section>
    <!-- End -->

</div>
</section>

<div class="main-backdrop"><!-- --></div>

</body>
</html>
<script>

    $(function(){
		$('#datetimepicker').datetimepicker({
                 format: 'DD/MM/YYYY'
           });
		
		$(".playlist_menu").addClass("left-menu-list-opened").show();
		$("#playlist_sub").css("color","#000");
		
		var file1="<?=isset($editdata[0]->head_picture_path)?"/".$editdata[0]->head_picture_path:""?>";
		$("#file-1").fileinput({
		showUpload: false,
		showCaption: false,
		browseClass: "btn btn-info",
        allowedFileExtensions : ['jpg', 'png'],
		initialPreview: [ '<img src="'+file1+'" style="width:auto;height:160px;" class="kv-preview-data file-preview-image" >']
       
	});
 
});
</script>
	<style>.progress,.fileinput-cancel-button{display: none;}</style>