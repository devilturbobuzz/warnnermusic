<!doctype html>
<html>
<body class="theme-default">



<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Form Elements -->
    <section class="panel">
      
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                  
                        <h4>Slide Top Form</h4>
                        <br />
                        <!-- Horizontal Form -->
                        <form method="post" id="artist_save" action="/Home/Form_save_slide_top" enctype="multipart/form-data">
                            <div class="form-group row">
                                <div class="col-md-3 file1">
                                    <label class="form-control-label" for="l16">Thumbnail (1400x768) .jpg .png <label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                      
                   			 <input id="file-1"  type="file" multiple=true name="fileToUpload_thumnail" >
                   			 <span class="error " id="file1" ></span>
                						</div>
                                  
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Subject<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" value="<?=isset($editdata[0]->artist_name)?$editdata[0]->artist_name:""?>"  class="form-control" placeholder="Artist Name" name="Artist">
                                   <span class="error " id="Artist" ></span>
                                </div>
                            </div>
       

   
          				  <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l15">date <label class="txt_alert">*</label></label>
                                </div>
                                 <div class="col-md-10">
                                     <input type='text' name="released" value="<?=isset($editdata[0]->released)?$editdata[0]->released:""?>" class="form-control" id='datetimepicker' />
                                     <small class="text-muted">Date mask input: วว/ดด/ปปปป</small>
                                      <span class="error " id="released" ></span>
                                </div>
                            </div>
                              <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Url<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" value="<?=isset($editdata[0]->url)?$editdata[0]->url:""?>"  class="form-control" placeholder="url" name="url">
                                      <span class="error " id="url" ></span>
                                  
                                </div>
                            </div>
                           <div class="form-actions">
                                <div class="form-group row">
                                    <div class="col-md-1 col-md-offset-9">
                                        <button type="button"  onClick="checkvalidate()" class="btn width-150 btn-info">Submit</button>
                                        
                                    </div>
                                </div>
                            </div>
                              <input type="hidden" value="<?=isset($editdata[0]->id)?$editdata[0]->id:""?>" name="id" />
                        </form>
                        <!-- End Horizontal Form -->
                   
                </div>
            </div>
         
        </div>
    </section>
    <!-- End -->

</div>
</section>

<div class="main-backdrop"><!-- --></div>

</body>
</html>



<script>

    $(function(){
 $('#datetimepicker').datetimepicker({
                 format: 'DD/MM/YYYY'
           });
var file1="<?=isset($editdata[0]->head_picture_path)?"/".$editdata[0]->head_picture_path:""?>";
        $("[data-toggle=tooltip]").tooltip();
		$(".home_menu").addClass("left-menu-list-opened").show();
		$("#slide_top_sub").css("color","#000");
   
     
       $('#date-mask-input').mask("00/00/0000", {placeholder: "__/__/____"});
	
		$("#file-1").fileinput({
		showUpload: false,
		showCaption: false,
		browseClass: "btn btn-info",
        allowedFileExtensions : ['jpg', 'png'],
		initialPreview: [ '<img src="'+file1+'" style="width:auto;height:160px;" class="kv-preview-data file-preview-image" >']	
    });
    })
	function checkvalidate(){
		var file1="<?=isset($editdata[0]->head_picture_path)?"/".$editdata[0]->head_picture_path:""?>";
		
		var status=true;
		if($( "input[name='Artist']").val()==""){
			$('#Artist').html("Artist require");
			$( "input[name='Artist']" ).css("border-color","#fb434a");
			status=false;
		}
	
		if($( "#file-1").val()=="" && file1==""){
			$('#file1').html("Thumbnail  require");
			$( ".file1" ).css("color","#fb434a");
			status=false;
		}
		if($( "input[name='url']").val()==""){
			$('#url').html("url require");
			$( "input[name='url']" ).css("border-color","#fb434a");
			status=false;
		}
		
		
		if($( "input[name='released']").val()==""){
			$('#released').html("released require");
			$( "input[name='released']" ).css("border-color","#fb434a");
			status=false;
		}
		if(status==true){
			$('#artist_save').submit();
		}else{
			alert("please check your input data");
			
		}
		return status;
	}
</script>
	<style>.progress,.fileinput-cancel-button{display: none;}</style>