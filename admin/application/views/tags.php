<body class="theme-default">


<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Tables -->
    <section class="panel">
        
        <div class="panel-body">
           
            <div class="row">
                <div class="col-lg-12">
                    <h4>Tags</h4>
                    <p><code>Tags / indexs</code></p>
                    <br />
                    
                    <div class="table-responsive margin-bottom-50">
                        <table class="table table-hover" style="    font-size: 13px;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Module</th>
                                    <th>Keyword</th>
                                    <th>DateCreate</th>
                                   
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Module</th>
                                    <th>Keyword</th>
                                    <th>DateCreate</th>
                                  <th>&nbsp;</th> 
                                </tr>
                            </tfoot>
                            <tbody>
                              <?php 
								$x=1;
                    			foreach($arr_data as $item)
								{
							  ?>
                                 <tr>
                                <td>
								 	<?=$x++;?>
								 </td>
								 <td>
								 	<?=$item->tags_module?>
								 </td>
                                <td>
								 	<?=$item->tags_index?>
								 </td>
                                 <td><?=$item->createdatetime?></td>
                                <td>
									  
										<a href="/Tags/form_delete/<?=$item->id?>" target="_self">
										<span class="fa fa-close btn btn-xs swal-btn-warning" data-id="<?=$item->id?>" title="Delete"></span>
										</a>
                                 </td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Basic Tables  -->

</div>

<!-- Page Scripts -->
<script>

    $(function () {

        $("[data-toggle=tooltip]").tooltip();
		$("#tags_menu").addClass("left-menu-list-active");
		$('.swal-btn-warning').click(function(e){
			var data_delete=$(this).data("id");
            e.preventDefault();
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this file!",
                type: "warning",
                showCancelButton: true,
                cancelButtonClass: "btn-default",
                confirmButtonClass: "btn-warning",
                confirmButtonText: "Remove",
                closeOnConfirm: false
            },
            function(){
                swal({
                    title: "Deleted!",
                    text: "File has been deleted",
                    type: "success",
                    confirmButtonClass: "btn-success"
                });
				
				window.open("/Tags/form_delete/"+data_delete,"_self");
            });
			
        });
    });

</script>
<!-- End Page Scripts -->
</section>

<div class="main-backdrop"><!-- --></div>

</body>