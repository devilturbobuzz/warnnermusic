<!doctype html>
<html>
<body class="theme-default">



<section class="page-content">
<div class="page-content-inner">

    <!-- Basic Form Elements -->
    <section class="panel">
      
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                  
                        <h4>Nationality Form</h4>
                        <br />
                        <!-- Horizontal Form -->
                        <form method="post" id="artist_save" action="/Tags/Form_save" >
                            
                            
                            
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l15">Tags Nmae<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text"  class="form-control" placeholder="tags name" name="tags_name" value="<?=isset($editdata[0]->tags_name)?$editdata[0]->tags_name:""?>">
                                </div>
                            </div>
                              <div class="form-group row ">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l0">Tags Module</label>
                                </div>
                                <div class="col-md-4 ">
                             
                                  <? //$arr_module=array('news','artist','nationality','genres','album','mood','playlist'); 
									$arr_module=array('news','artist','playlist'); 
								  ?>
                                   <select name="tags_module" class="form-control"  >
                                   <option value="">-- Choose Module --</option>
                                   <? for($i=0;$i<count($arr_module);$i++){ ?>
                                   <? 
										   $str_selected="";
										if(isset($editdata[0]->tags_module) && $editdata[0]->tags_module==$arr_module[$i]){
											$str_selected="selected";	
										}
								   ?>
                                    <option value="<?=$arr_module[$i]?>" <?=$str_selected?>   ><?=$arr_module[$i]?></option>
                                   <? } ?>
                                  </select>
                                </div>
                                 </div>
                                <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="form-control-label" for="l15">Tags index(Key word)<label class="txt_alert">*</label></label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text"  class="form-control" placeholder="Tags Search" name="tags_index" value="<?=isset($editdata[0]->tags_index)?$editdata[0]->tags_index:""?>">
                                    <span class="text-info">ตัวอย่าง popnews,rocknew หากมี Key word มากกว่า 1 ให้ระบุโดยการใส่ " , " คั่นระหว่างคำ </span>
                                </div>
                            </div>
                           
                           <div class="form-actions">
                                <div class="form-group row">
                                    <div class="col-md-1 col-md-offset-9">
                                        <button type="button"  onClick="$('#artist_save').submit();" class="btn width-150 btn-info">Submit</button>
                                        
                                    </div>
                                </div>
                            </div>
                             <input type="hidden" value="<?=isset($editdata[0]->id)?$editdata[0]->id:""?>" name="id" />
                        </form>
                        <!-- End Horizontal Form -->
                   
                </div>
            </div>
         
        </div>
    </section>
    <!-- End -->

</div>
</section>

<div class="main-backdrop"><!-- --></div>

</body>
</html>
<script>

    $(function(){
        $("[data-toggle=tooltip]").tooltip();
		$("#tags_menu").addClass("left-menu-list-active");
    })
</script>
	<style>.progress,.fileinput-cancel-button{display: none;}</style>