<html lang="en">
<head>
    <title>Bootstrap Typeahead with Ajax Example</title>  
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap3-typeahead.min.js"></script>  
</head>
<body>
<?
include_once "../../php/mydb.php"; 



	$db = new Utility();
	$conn=$db->mysqlConnect_matrix4();
	$strSQL2 = "SELECT ID as id ,PROMOTIONNOTE as value
				FROM matrix4.stdpremium
				WHERE stdpremiumstatus='Y' ";
		$result=$db->MyQuery($strSQL2,$conn);

		$data_js= json_encode($result);
?>

<div class="row">
	<div class="col-md-6">
		<br/>
	
			<input class="typeahead form-control"   type="text">

	</div>
</div>

<script type="text/javascript">
var xx='<?=$data_js?>';
var data = jQuery.parseJSON(xx);

	$('.typeahead').typeahead(
	{
	items: 15,
	source:function(request, response){    
    return  response(data) },
	 autoSelect: true,
	 displayText: function(item){ return item.value; }
	});
	
</script>
</body>
</html>