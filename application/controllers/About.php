<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->helper('url');
		
    }
   public function footer(){
		$this->load->model('news_m');
		$data['arr_tags']=$this->news_m->selectdata_tags();
		$this->load->view('footer',$data);
	}
	public function index()
	{
		$this->load->view('header');
		$this->load->view('about');
		$this->footer();
		$this->load->view('includeJS');

	}
}
