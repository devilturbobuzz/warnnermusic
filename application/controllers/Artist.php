<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artist extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->helper('url');
		$this->load->model('artist_m');
		$this->load->view('header');
	
    }
	
	function _remap($function,$params = array())
	{	
		
	
		
				$data_arr=$this->artist_m->selectdata_nationality(NULL);
				$str_getnationality="";
				$str_getid="";
				 foreach($data_arr as $item)
					{
						if($item->nationality_name==$function){
							$str_getnationality="OK";
							$str_getid=$item->id;
						}
					}


				 if($str_getnationality=="OK")
				{
				   $this->index($str_getid);
				}
				 else
				{

					if($function=="index"){
						$this->index(NULL);
					}else if($function=="album"){
						$this->$function($params[0],$params[1],$params[2]);
					}else{
						$this->$function($params[0]);
					}

				}
        
     

		
	}
	public function index($type)
	{
		
		
		$data['data_view']=$this->artist_m->selectdata(NULL,$type);
		$data['data_mood']=$this->artist_m->selectdata_mood(NULL);
		$data['data_track_artist']=$this->artist_m->selectdata_tracks_artist();
		$data['data_nationality']=$this->artist_m->selectdata_nationality(NULL);
		$data['get_type']=$type;
		$this->load->model('news_m');
		$data['arr_tags']=$this->news_m->selectdata_tags();
		$this->load->view('artists',$data);
		
	}
	
	
	public function detail($id)
	{
		
		$data['data_view']=$this->artist_m->selectdata_detail($id);
		$data['data_artist']=$this->artist_m->selectdata_artist($id);
		$data['data_gallery']=$this->artist_m->selectdata_gallery();
		$data['data_onlinech']=$this->artist_m->selectdata_onlinech();
		$data['data_track_album']=$this->artist_m->selectdata_tracks_album($id);
		$this->load->model('news_m');
		$data['arr_tags']=$this->news_m->selectdata_tags();
		$this->load->view('artists-detail',$data);
		
	}
	public function album($artist_name,$album_id,$artist_id)
	{
	    $data['arr_data'] =$this->artist_m->selectdata_album($album_id);
		$data['data_view']=$this->artist_m->selectdata_music_audio($album_id,$artist_id);
		$this->load->model('news_m');
		$this->load->model('playlist_m');
		$data['data_gallery']=$this->playlist_m->selectdata_gallery();
		$data['data_onlinech']=$this->artist_m->selectdata_onlinech();
		$data['arr_tags']=$this->news_m->selectdata_tags();
		$this->load->view('artists_album_audio',$data);
		
	}
	
	
	
	
}
