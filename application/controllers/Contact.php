<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->helper('url');
		$this->load->model('contact_m');
		
    }
	public function index()
	{
		
		$this->load->view('header');
           $this->load->model('news_m');
		$data['arr_tags']=$this->news_m->selectdata_tags();
		$this->load->view('contact',$data);
		
		$this->load->view('includeJS');

	}
	public function save()
	{
		$data=array();
		$data['name']=$this->input->post('name');
		$data['tell']=$this->input->post('phone');
		$data['email']=$this->input->post('cd-email');
		$data['message']=$this->input->post('cd-textarea');
		$data['createdatetime']= date("Y-m-d h:i:s");
		$after_save=$this->contact_m->save($data,"contact",'',"id");
		
		$config = Array(
		'protocol' => 'smtp',
		'smtp_host' => 'ssl://smtp.googlemail.com',
		'smtp_port' => 465,
		'smtp_user' => 'devilturbo@gmail.com',
		'smtp_pass' => 'T1heT2ur3456', 
		'mailtype'  => 'html', 
		'charset'   => 'utf-8'
	);
	$this->load->library('email', $config);
	$this->email->set_newline("\r\n");

// Set to, from, message, etc.
    $this->email->from('devilturbo@gmail.com', 'Information Cotact');
    $this->email->to($this->input->post('cd-email')); 
    $this->email->bcc('devilturbo@gmail.com'); 

    $this->email->subject('WARNERMUSIC CONTACT');
    $this->email->message("FROM : ".$this->input->post('cd-email')."<br>PHONE : ".$this->input->post('phone')."<br/>DESCRIPTION : ".$this->input->post('cd-textarea')."<br/>Time ".date("Y-m-d h:i:s"));  

    $this->email->send();

   // echo $this->email->print_debugger();

			redirect("/Contact/Success");
		
	}
	public function Success()
	{
		
		$this->load->view('header');
               	$this->load->model('news_m');
		$data['arr_tags']=$this->news_m->selectdata_tags();
		$this->load->view('contact-success',$data);
	
		$this->load->view('includeJS');

	}
}
