<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->helper('url');
		$this->load->model('news_m');
			
    }
	public function footer(){
		$this->load->model('news_m');
		$data['arr_tags']=$this->news_m->selectdata_tags();
		$this->load->view('footer',$data);
	}
	public function index()
	{
		
		$this->load->view('header');
		$data['newsdat_hilight']=$this->news_m->selectdata_hilight();
		$data['arr_slidetop']=$this->news_m->selectdata_slidetop();
		$data['arr_artist_month']=$this->news_m->selectdata_album();
		$data['arr_video']=$this->news_m->selectdata_video();
		$data['arr_playlist']=$this->news_m->selectdata_playlist();
		$data['arr_album_index']=$this->news_m->selectdata_album_index();
	
		$this->load->view('index',$data);
		$this->footer();
		$this->load->view('includeJS');

	}
}
