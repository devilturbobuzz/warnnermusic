<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->helper('url');
		$this->load->model('news_m');

    }
	function _remap($function,$params = array())
	{
		
		if($function=="2016" || $function=="2017"){
			$this->index($function);
		}else if($function=="News_Detail"){
			$this->News_Detail($params[0]);
		}else{
			$this->index(NULL);
		}
	}
	public function index($year)
	{
		
		
		$this->load->view('header');
		$this->load->view('includeJS');
		$data['newsdat_hilight']=$this->news_m->selectdata_hilight_sub_news($year);
		$data['newsdata']=$this->news_m->selectdata_sub_news($year);
		$data['get_year']=$year;
		$this->load->model('news_m');
		$data['arr_tags']=$this->news_m->selectdata_tags();
		$this->load->view('news',$data);
		
		$this->load->view('footer');
		
		
	}
	public function News_Detail($id)
	{
		
	
		$this->load->view('header');
		$this->load->view('includeJS');
		$this->load->model('news_m');
		$data['arr_tags']=$this->news_m->selectdata_tags();
		$data['newsdata']=$this->news_m->selectdata($id);
		$this->load->view('news-detail',$data);
		$this->load->view('footer');	
		
		
	}
	
	
	
}
