<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PAGE_404 extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->helper('url');
    }
	public function index()
	{
		$this->output->set_status_header('404'); 
		$this->load->view('header');
		$this->load->view('error_404');
		$this->load->view('includeJS');
		$this->load->model('news_m');
		$data['arr_tags']=$this->news_m->selectdata_tags();
		$this->load->view('footer',$data);
	}
}
