<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Playlist extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->helper('url');
		$this->load->model('playlist_m');
		$this->load->view('header');
		$this->load->view('includeJS');
    }
	public function index()
	{
		
	    $data['arr_playlist']=$this->playlist_m->selectdata_playlist(NULL);
		$data['arr_playlist_cat']=$this->playlist_m->selectdata_playlist_cat(NULL);
		$this->load->model('news_m');
		$data['arr_tags']=$this->news_m->selectdata_tags();
		$this->load->view('playlists',$data);
		$this->load->view('footer');
	}
	
	
	public function detail($id)
	{
		
	    $data['arr_playlist']=$this->playlist_m->selectdata_playlist($id);
		$this->load->model('artist_m');
		$data['data_gallery']=$this->artist_m->selectdata_gallery();
		$this->load->model('news_m');
		$data['arr_tags']=$this->news_m->selectdata_tags();
		$this->load->view('playlists-detail',$data);
		$this->load->view('footer');
		
	}
	public function category($id)
	{
		
	    $data['arr_playlist_sub_cat']=$this->playlist_m->selectdata_playlist_sub_cat($id);
        $data['arr_playlist_cat']=$this->playlist_m->selectdata_playlist_cat($id);
		$this->load->model('news_m');
		$data['arr_tags']=$this->news_m->selectdata_tags();
		$this->load->view('playlists-sub-cat',$data);
	}
	
	
	
	
}
