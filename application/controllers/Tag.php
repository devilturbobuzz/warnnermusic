<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tag extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->helper('url');
		$this->load->model('Tags_m');
		$this->load->model('artist_m');
		$this->load->model('playlist_m');
		$this->load->view('header');
		$this->load->view('includeJS');

    }
	function _remap($function,$params = array())
	{
		
		
			$this->index($function);
		
	}

	public function footer(){
		$this->load->model('news_m');
		$data['arr_tags']=$this->news_m->selectdata_tags();
		$this->load->view('footer',$data);
	}

	public function getstr_index($keyword){
			$keep_str="t.tags like '%".$keyword."%'";
		return $keep_str;
	}
	public function index($keyword)
	{
			$keyword= iconv(mb_detect_encoding($keyword, mb_detect_order(), true), "UTF-8", $keyword);
			$data['str_tags']=$keyword;
			$keyword=trim($this->getstr_index($keyword));
			$data['arr_tags_news']=$this->Tags_m->selectdata_tags_news($keyword);
			$data['arr_tags_artist']=$this->Tags_m->selectdata_tags_artist($keyword);
			$data['arr_tags_playist']=$this->Tags_m->selectdata_tags_playlist($keyword);
			
			$this->load->view('search-by-tag',$data);
			$this->footer();
			$this->load->view('includeJS');
	}
	
}
