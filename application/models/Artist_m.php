<?php
 class Artist_m extends CI_Model {     
	 function __construct() 
	{
		parent::__construct();
	}
	
	public function save($data,$table,$id,$wher_colums)
	{
		
		if($id){
			$this->db->update($table, $data, array($wher_colums => $id));
		}else{
			$this->db->insert($table, $data);
		}
	}
	 
	public function selectdata_tracks_artist() {
		
			  $query = $this->db->select('a.artist_id,count(b.id) as track')->from('music a')->join('music_audio b', ' a.id =b.music_id', 'left')->group_by('a.artist_id')->get(); 
		 
          return $query->result();
	} 
	 
	public function selectdata_tracks_album($artist_id) {
		
			  $query = $this->db->select('a.album_id,count(b.id) as track')->from('music a')->join('music_audio b', ' a.id =b.music_id', 'left')->where('a.artist_id',$artist_id)->group_by('a.album_id')->get(); 
		 
          return $query->result();
	}  
	public function selectdata_gallery() {
		
			$query = $this->db->select('b.thumnail_path,b.name,b.id')->from('gallery b')->order_by("b.createdatetime","desc")->get();
		 
          return $query->result();
	}   
	public function selectdata_onlinech() {
		
			$query = $this->db->select('b.thumnail_path,b.name,b.id')->from('online_ch b')->order_by("b.createdatetime","desc")->get();
		 
          return $query->result();
	}   

	 public function selectdata($id,$type) {
		
		 
		 
		 if($id==NULL){
			 if($type){
				  $query = $this->db->select('a.*,DATE_FORMAT( a.createdatetime, "%d/%m/%Y") as createdatetime')->from('artist_view a')->where('a.Nationality', $type)->order_by("a.createdatetime","desc")->get();
			 }else{
				  $query = $this->db->select('a.*,DATE_FORMAT( a.createdatetime, "%d/%m/%Y") as createdatetime')->from('artist_view a')->order_by("a.createdatetime","desc")->get();
			 }
		 	
		}else if($id){
			  $query = $this->db->select('a.*,DATE_FORMAT( a.createdatetime, "%d/%m/%Y") as createdatetime')->from('artist_view a')->where('a.artist_id', $id)->get();
			 
		 }
		
          return $query->result();

	}
	  public function selectdata_detail($id) {
		  $query = $this->db->select('a.*,(select b.Description from artist b where a.artist_id='.$id.' limit 1 ) as artist_description, (
		SELECT
			b.artist_name
		FROM
			artist b
		WHERE
			a.artist_id = b.artist_id
		LIMIT 1
	) AS artist_name
, (
		SELECT
			b.pic_description_path
		FROM
			artist b
		WHERE
			a.artist_id = b.artist_id
		LIMIT 1
	) AS pic_description_path')->from('album a')->where('a.artist_id', $id)->get();
		  return $query->result();

	}
	 
	 public function selectdata_artist($id) {
		 		$query = $this->db->select('b.*')->from('artist b')->where('artist_id', $id)->get();
		 return $query->result();

	}
	 
	  public function selectdata_album($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('b.*,DATE_FORMAT( b.createdatetime, "%d/%m/%Y") as createdatetime,(select a.artist_name from artist a where a.artist_id=b.artist_id LIMIT 1) as artist_name,,DATE_FORMAT( release_on, "%d %M %Y" ) as release_on')->from('album b')->order_by("createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('b.*,DATE_FORMAT( b.createdatetime, "%d/%m/%Y") as createdatetime,(select a.artist_name from artist a where a.artist_id=b.artist_id LIMIT 1) as artist_name,,DATE_FORMAT( release_on, "%d %M %Y" ) as release_on')->from('album b')->where('album_id', $id)->get();
			 
		 }
          return $query->result();

	}
	  public function selectdata_mood($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('genres')->order_by("genres_name","desc")->get();
		}else if($id){
			  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('genres')->where('id', $id)->get();
			 
		 }
          return $query->result();

	}
	 public function selectdata_nationality() {
		
		
		  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('nationality')->order_by("nationality_name","desc")->get();
		
          return $query->result();

	}
	 
	 
	 public function selectdata_music($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('mood')->order_by("createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('*,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('mood')->where('mood_id', $id)->get(); 
		 }
          return $query->result();
	}
	public function selectdata_music_audio($album_id,$artist_id) {		
		  $query = $this->db->select('a.*, (select COUNT(x.music_id) From music_audio x where x.music_id=m.id) as track')->from('music m')->join('music_audio a', ' m.id = a.music_id', 'left')->where('m.album_id', $album_id)->where('m.artist_id', $artist_id)->order_by("a.music_name","desc")->get();
          return $query->result();
	}
	
	   
	
	
 }

?>