<?php
 class News_m extends CI_Model {     
	 function __construct() 
	{
		parent::__construct();
	}
	
	
	 public function selectdata($id) {
		
		 if($id==NULL){
		  $query = $this->db->select('*,DATE_FORMAT( date_news, "%d %M" ) as date_news,DATE_FORMAT( createdatetime, "%d %M") as createdatetime')->from('news')->where(" date_news < now()")->order_by("date_news","desc")->get();
		}else if($id){
			  $query = $this->db->select('*,DATE_FORMAT( date_news, "%d %M %Y" ) as date_news,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('news')->where(" date_news < now()")->where('id', $id)->get();
			 
		 }
          return $query->result();

	}
	 public function selectdata_sub_news($year) {
		
		 if($year==NULL){
		  $query = $this->db->select('*,DATE_FORMAT( date_news, "%d %M" ) as date_news,DATE_FORMAT( createdatetime, "%d %M") as createdatetime')->from('news')->order_by("date_news","desc")->get();
		}else if($year){
			  $query = $this->db->select('*,DATE_FORMAT( date_news, "%d %M %Y" ) as date_news,DATE_FORMAT( createdatetime, "%d/%m/%Y") as createdatetime')->from('news')->where('Year(date_news)', $year)->get();
			 
		 }
          return $query->result();

	}
	  public function selectdata_hilight_sub_news($year) {
		
		if($year==NULL){
		  $query = $this->db->select('*,DATE_FORMAT( date_news, "%d %M %Y" ) as date_news,DATE_FORMAT( createdatetime, "%d %M %Y") as createdatetime')->from('news')->where("highlight_news='Y'  AND date_news < now()")->order_by("date_news","desc")->get();
		}else{
			 $query = $this->db->select('*,DATE_FORMAT( date_news, "%d %M %Y" ) as date_news,DATE_FORMAT( createdatetime, "%d %M %Y") as createdatetime')->from('news')->where("highlight_news='Y' AND Year(date_news)='".$year."' AND date_news < now()")->order_by("date_news","desc")->get();
		}
          return $query->result();

	}
	  public function selectdata_hilight() {
		
		
		  $query = $this->db->select('*,DATE_FORMAT( date_news, "%d %M %Y" ) as date_news,DATE_FORMAT( createdatetime, "%d %M %Y") as createdatetime')->from('news')->where("highlight_news='Y'  AND date_news < now()")->order_by("date_news","desc")->get();
          return $query->result();

	}
	  public function selectdata_slidetop() {
		  $query = $this->db->select('*,DATE_FORMAT( released, "%d %M %Y" ) as released')->from('home')->where(" released < now()")->order_by("released","desc")->get();
          return $query->result();

	}
	public function selectdata_tags() {
		  $query = $this->db->select('*')->from('tags_search')->group_by('tags_index')-> order_by('rand()')->limit(20)->get();
          return $query->result();
	}
	 public function selectdata_video() {
		  $query = $this->db->select('*,DATE_FORMAT( released, "%d %M %Y" ) as released')->from('music_video')->where(" released < now()")->order_by("released","desc")->limit(6)->get();
          return $query->result();
	}
	public function selectdata_playlist() {
		  $query = $this->db->select('*,DATE_FORMAT( release_on, "%d %M %Y" ) as released')->from('playlist')->where(" release_on < now()")->order_by("release_on","desc")->limit(6)->get();
          return $query->result();
	}
	public function selectdata_album_index() {
		  $query = $this->db->select('a.*, (
		SELECT
			b.artist_name
		FROM
			artist b
		WHERE
			a.artist_id = b.artist_id
		LIMIT 1
	) AS artist_name,DATE_FORMAT( Release_on, "%d %M %Y" ) as released')->from('album a')->where(" Release_on < now()")->order_by("Release_on","desc")->limit(6)->get();
		  return $query->result();

	} 
	
	 public function selectdata_album() {
		  $query = $this->db->select('b.*,DATE_FORMAT( b.Release_on, "%d %M %Y") as Release_on,(select a.artist_name from artist a where a.artist_id=b.artist_id LIMIT 1) as artist_name,(select a.thumnail_path from artist a where a.artist_id=b.artist_id LIMIT 1) as thumnail_path_artist,DATE_FORMAT( b.Release_on, "%M") as Release_M,DATE_FORMAT( b.Release_on, "%Y") as Release_Y,(select a.pic_description_path from artist a where a.artist_id=b.artist_id LIMIT 1) as pic_description_path,')->from('album b')->where(" Release_on < now()")->where('artist_month', "Y")->order_by("Release_on","desc")->get();
          return $query->result();

	}

	
 }

?>