<?php
 class Playlist_m extends CI_Model {     
	 function __construct() 
	{
		parent::__construct();
	}
	
	public function selectdata_playlist($id) {
		 if($id==NULL){
		  $query = $this->db->select('a.*')->from('playlist a')->order_by("a.createdatetime","desc")->get();
		}else if($id){
			  $query = $this->db->select('a.*,DATE_FORMAT( a.release_on, "%d %M %Y" ) as release_on')->from('playlist a')->where('a.id', $id)->get(); 
		 }
          return $query->result();

	 }
	 public function selectdata_playlist_sub($id) {
		
			  $query = $this->db->select('a.*')->from('playist_view a')->where('a.playlist_id', $id)->get(); 
		 
          return $query->result();

	 }
	 	public function selectdata_gallery() {
		
			$query = $this->db->select('b.thumnail_path,b.name,b.id')->from('gallery b')->order_by("b.createdatetime","desc")->get();
		 
          return $query->result();
	} 
	 public function selectdata_artist($id) {
		
			  $query = $this->db->select('a.*')->from('playist_view a')->where('a.playlist_id', $id)->group_by('a.artist_name')->get(); 
		 
          return $query->result();

	 }
	
	public function selectdata_playlist_sub_cat($id) {
	     $query = $this->db->select('p.id,p.thumnail_path,p.subject')->from('playlist p')->where("p.id_playlist_cat like '%".$id."%'")->get(); 
          return $query->result();
	}	
		
	  public function selectdata_playlist_cat($id) {
		 if($id==NULL){
		  $query = $this->db->select('a.*')->from('playlist_cat a')->order_by("a.subject","desc")->get();
		}else if($id){
			  $query = $this->db->select('a.*')->from('playlist_cat a')->where('a.id', $id)->get(); 
		 }
          return $query->result();
	}
	
 }

?>