<?php
 class tags_m extends CI_Model {     
	 function __construct() 
	{
		parent::__construct();
	}

	 public function selectdata_module($keyword) {
			  $query = $this->db->select('t.tags_module,t.tags_index')->from('tags_search t')->where('t.tags_name', $keyword)->get();
          return $query->result();

	}
	 public function selectdata_tags_news($keyword) {
				 $query = $this->db->select('t.*,DATE_FORMAT( t.date_news, "%d %M" ) as date_news')->from('news t')->where($keyword)->get();
          return $query->result();

	}
	 public function selectdata_tags_artist($keyword) {
		 $query = $this->db->select('t.*,DATE_FORMAT( t.createdatetime, "%d/%m/%Y") as createdatetime')->from('artist_view t')->where($keyword)->get();
		  return $query->result();
	 }
	 public function selectdata_tags_playlist($keyword) {
		  $query = $this->db->select('t.*,DATE_FORMAT( t.release_on, "%d %M %Y" ) as release_on')->from('playlist t')->where($keyword)->get();
		  return $query->result();
	 }
	
 }

?>