
<section id="content" class=" animsition container">
    <div id="about">
      <div class="head-con img-about">
         <span class="about-img h-mob"><img src="assets/img/about01.jpg"></span>
         <h1 class="rota-90 abs c-white left-100 top-1">Culture</h1>
         <div class="overlay opt"></div>
         <div class="line bg-white abs opt-2"></div>
         <nav class="h-mob">
             <ol class="cd-breadcrumb custom-separator">
               <li><a href="index.php" class="animsition-link" >Home</a></li>
               <li class="current"><em>About</em></li>
             </ol>
         </nav>
         <h5 class="c-white abs sp2">About</h5>
      </div>
  </div>   


      <div class=" container-inner" id="about">

          <div class="row pad-top-40 pad-bot-40">
            <div class="col-1 c-white h-mob">-</div>
            <div class="col-3 h-mob">-</div>
            <div class="col-5"><h3>Warner Music Group (Thailand)</h3></div>
            <div class="col-1 c-white h-mob">-</div>
          </div>

          <div class="row pad-top-40 pad-bot-100">
            <div class="col-2 c-white">-</div>
            <div class="col-6">
              
              Warner Music Group (abbreviated as WMG, commonly referred to as Warner Music) is an American multinational entertainment and record label conglomerate headquartered in New York City. It is one of the "big three" recording companies and the third largest in the global music industry, next to Universal Music Group (UMG) and Sony Music Entertainment (SME), being the only American music conglomerate worldwide. Formerly owned by Time Warner, the company was publicly traded on the New York Stock Exchange until May 2011, when it announced its privatization and sale to Access Industries, which was completed in July 2011. With a multibillion-dollar annual turnover, WMG employs in excess of 3,500 people and has operations in more than 50 countries throughout the world.<br><br>

              The company owns and operates some of the largest and most successful record labels in the world, including its flagship labels Warner Bros. Records, Parlophone and Atlantic Records. WMG also owns Warner/Chappell Music, one of the world's largest music publishers.
            </div>
            <div class="col-2 c-white">-</div>

          </div>

      
      </div>

  

</section>


<script>
  $(document).ready(function() {
   //$(".vertical-slider .vertical-slider-elem").eq(6).addClass('active');
  });
</script>

