<?php include('header.php') ?>

<section id="content" class=" animsition container">

    <div class="CoverImageX2 f-bure" style="background-image:url(assets/img/artists-album05.jpg)"></div> 
    <div class="overlay bg-blue opt-5 fix"></div>
    <div class="overlay bg-blue-gra opt-5 fix"></div>


      <div class="head-con head-con-playlists ">
          <div class="line bg-white abs opt-2 row"></div>
          <nav class="h-mob ">
             <ol class="cd-breadcrumb custom-separator pad-left-40">
               <li><a href="index.php" class="animsition-link" >Home</a></li>
               <li><a href="artists.php" class="animsition-link" >Atists</a></li>
               <li class="current"><em>Back pack music</em></li>
             </ol>
          </nav>
          <h5 class="c-white abs">Contact</h5>

          

      </div><!--End header-->

      <div class="row ">
          <div class="container-inner">
            <div class="desc-con pad-top-60">
              <div class="album"><div class="CoverImage" style="background-image:url(assets/img/artists-album05.jpg)"></div> </div>
              <div class="pad-left-40" style="width: 77vw; ">
                  <h3 class="c-white pad-bot-15">Back Packer Music</h3>
                  <div class="line bg-white abs opt-2 row mar-bot-20"></div>
                  <p class="">Polycat เกิดจากการรวมตัวกันตั้งวงเล่นดนตรี โดยเริ่มจาก “ผา” และ “นะ” ที่เรียนอยู่มหาวิทยาลัยเชียงใหม่ด้วยกัน ก่อตั้งวงดนตรีขึ้นมา ใช้ชื่อวงว่า “Ska Rangers” วงดนตรีแนวเร็กเก้ สกา มี “นะ” เป็นนักร้องนำ และกีต้าร์ “ผา” และ “โต้ง” เล่นเครื่องเป่า โดยได้ “ดอย” และ “เพียว” เพื่อนจากมหาวิทยาลัยพายัพ มาตีกลอง และเล่นเบสให้</p><br>
                  <div class="track pad-bot-15 pad-bot-40 ">
                    <span class="pad-right-20"><i class="fa fa-music c-blue2" aria-hidden="true"></i>3 Tracks</span>
                    <span class="pad-right-20"><i class="fa fa-clock-o c-blue2" aria-hidden="true"></i>20 hrs 34 mins Length</span>
                    
                  </div >
                  <span class=" h-mob c-white">Release on 12 June 2016</span>
              </div>          
            </div>
          </div> 
        
      </div>


      <div class="row">
          <div class="container-inner">
            <div class="desc-con">
                <div class="list-detail">
                  <?php include('player.php') ?>
                  <div id="listContainer" class="playlistContainer pad-bot-40">
                    <ul id="playListContainer">


                      <li data-src="assets/media/song1.mp3" class="lists">
                        <div class="lists">
                        
                            <div class="item1 eq">
                              <i class="fa fa-play icon c-blue2" aria-hidden="true"></i>
                              <div class='bars-eq'>
                                 <div class='bar-eq'></div>
                                 <div class='bar-eq'></div>
                                 <div class='bar-eq'></div>
                                 <div class='bar-eq'></div>
                              </div>
                            </div>

                            <div class="item1 pad-left-10 ">
                              <div class="thumb">
                                <div class="mask bg-blue"></div>
                                <img src="assets/img/playlists/thumb2.jpg">
                              </div>
                            </div>

                            <div class="item2 track pad-left-40 pad-top-5"><a><p class="c-white">Song Name 01</p></a></div>
                            <div class="item2 pad-top-5">Unapologetic (Deluxa)</div>
                            <div class="item2 pad-top-5">Unapologetic (Deluxa)</div>
                            <div class="item3 pad-top-5 ">
                              <div id="playList" class=" cd-form ">
                                <p class="cd-select icon ">
                                  <select class="download-icon">
                                    <option value="0" class="c-white">Download</option>
                                    <option value="1">Apple music</option>
                                    <option value="1">True music</option>
                                    <option value="1">Dezzer</option>
                                  </select>
                                </p>
                              </div>
                           </div>
                        
                        </div>
                      </li><!-- End Song -->
                      
                     
                        <li data-src="assets/media/song2.mp3" class="lists">
                        <div class="lists">
                        
                            <div class="item1 eq">
                              <i class="fa fa-play icon c-blue2" aria-hidden="true"></i>
                              <div class='bars-eq'>
                                 <div class='bar-eq'></div>
                                 <div class='bar-eq'></div>
                                 <div class='bar-eq'></div>
                                 <div class='bar-eq'></div>
                              </div>
                            </div>

                            <div class="item1 pad-left-10 ">
                              <div class="thumb">
                                <div class="mask bg-blue"></div>
                                <img src="assets/img/playlists/thumb1.jpg">
                              </div>
                            </div>

                            <div class="item2 track pad-left-40 pad-top-5"><a><p class="c-white">Song Name 02</p></a></div>
                            <div class="item2 pad-top-5">Unapologetic (Deluxa)</div>
                            <div class="item2 pad-top-5">Unapologetic (Deluxa)</div>
                            <div class="item3 pad-top-5 ">
                              <div id="playList" class=" cd-form ">
                                <p class="cd-select icon ">
                                  <select class="download-icon">
                                    <option value="0" class="c-white">Download</option>
                                    <option value="1">Apple music</option>
                                    <option value="1">True music</option>
                                    <option value="1">Dezzer</option>
                                  </select>
                                </p>
                              </div>
                           </div>
                        
                        </div>
                      </li><!-- End Song -->

                       <li data-src="assets/media/song3.mp3" class="lists">
                        <div class="lists">
                        
                            <div class="item1 eq">
                              <i class="fa fa-play icon c-blue2" aria-hidden="true"></i>
                              <div class='bars-eq'>
                                 <div class='bar-eq'></div>
                                 <div class='bar-eq'></div>
                                 <div class='bar-eq'></div>
                                 <div class='bar-eq'></div>
                              </div>
                            </div>

                            <div class="item1 pad-left-10 ">
                              <div class="thumb">
                                <div class="mask bg-blue"></div>
                                <img src="assets/img/playlists/thumb3.jpg">
                              </div>
                            </div>

                            <div class="item2 track pad-left-40 pad-top-5"><a><p class="c-white">Song Name 03</p></a></div>
                            <div class="item2 pad-top-5">Unapologetic (Deluxa)</div>
                            <div class="item2 pad-top-5">Unapologetic (Deluxa)</div>
                            <div class="item3 pad-top-5 ">
                              <div id="playList" class=" cd-form ">
                                <p class="cd-select icon ">
                                  <select class="download-icon">
                                    <option value="0" class="c-white">Download</option>
                                    <option value="1">Apple music</option>
                                    <option value="1">True music</option>
                                    <option value="1">Dezzer</option>
                                  </select>
                                </p>
                              </div>
                           </div>
                        
                        </div>
                      </li><!-- End Song -->

                     
                    </ul>
                  </div>
                </div>
             
            </div>
          </div>
      </div>
    

</section>

<div class="container"><?php include('footer.php') ?></div>
<?php include('includeJS.php') ?>
<script>
  $(document).ready(function() {
  $('.main-nav li').find('a').removeClass('hvr-underline-from-center-active ');
  $('.main-nav li').eq(2).find('a').addClass('hvr-underline-from-center-active');
  });
</script>

<script src="assets/js/jquery.audioControls.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
      var currentTrack = 0;
      $("#playListContainer").audioControls({
        autoPlay : true,
        shuffled: false,
        timer: 'increment',
        
        
        onPause: function(response){ 
          $('.song-thumb').removeClass('active');   
          $('#playListContainer li').eq(currentTrack).find('.eq .bar-eq').removeClass('active');
          $('#playListContainer li').eq(currentTrack).find('.eq .icon').addClass('active');


        },
        onPlay: function(response){
          $('.song-thumb').addClass('active');
          $('#playListContainer li').eq(currentTrack).find('.eq .bar-eq').addClass('active');
          $('#playListContainer li').eq(currentTrack).find('.eq .icon').removeClass('active');

        },
        
        onAudioChange : function(response){
        
          $('.songPlay').text(response.title + ' ...');
        
          currentTrack = response.index;  

          $('#playListContainer li').find('.eq .bar-eq').removeClass('active');
          $('#playListContainer li').eq(currentTrack).find('.eq .bar-eq').addClass('active');

          $('#playListContainer li').find('.eq .icon').addClass('active');
          $('#playListContainer li').eq(currentTrack).find('.eq .icon').removeClass('active');


          $('.song-thumb').css('background-image', 'url(assets/img/playlists/thumb'+response.index+'.jpg)');
          if($('.song-thumb').hasClass('active')){
              $('.song-thumb').removeClass('active');
          }
          $('.song-thumb').addClass('active');

        },

        onVolumeChange : function(vol){
          var obj = $('.volume');
          if(vol <= 0){
            obj.attr('class','volume mute');
          } else if(vol <= 33){
            obj.attr('class','volume volume1');
          } else if(vol > 33 && vol <= 66){
            obj.attr('class','volume volume2');
          } else if(vol > 66){
            obj.attr('class','volume volume3');
          } else {
            obj.attr('class','volume volume1');
          }
        }
      });

    });
</script>




