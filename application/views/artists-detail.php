
  <link rel="stylesheet" href="assets/css/tab.css">


<section id="content" class=" animsition container">

      <div class="head-con h-100vh head-con-playlists">

        <div class="CoverImage " style="background-image:url(<?=main_site_url().$data_artist[0]->pic_description_path?>)"></div> 
        <div class="overlay-2 bg-blue-gra opt-5"></div>


         <div class="line bg-white abs opt-2"></div>
         <nav class="h-mob">
             <ol class="cd-breadcrumb custom-separator pad-left-30 ">
               <li><a href="index.php" class="animsition-link" >Home</a></li>
               <li><a href="playlists.php" class="animsition-link" >Aritist</a></li>
               <li class="current"><em><?=$data_artist[0]->artist_name?></em></li>
             </ol>
          </nav>
          <h5 class="c-white abs">Artists</h5>

          <div class="desc">
            <h1 class="c-white pad-bot-15"><?=$data_artist[0]->artist_name?></h1>
            <div class="track pad-bot-15 c-white row h-mob">
          
         
           <? if($data_artist[0]->url_download){ ?>
          
           <?
			$arr_url_subject=explode(",",$data_artist[0]->url_subject);
			$arr_url_download=explode(",",$data_artist[0]->url_download);
			for($i=0;$i<count($arr_url_subject);$i++){
				$gallery_path="";
	   		 foreach($data_gallery as $item)
			{
				if($arr_url_subject[$i]==$item->id){
					$gallery_path=$item->thumnail_path;
				}
			}
				$str_img_download="<img style='width: 112px; height: 36px;' src='".main_site_url().$gallery_path."'>";
		  
           echo " <span>";
           echo anchor(prep_url($arr_url_download[$i]), $str_img_download, 'target="_blank"' ,'class="-link"'); 
            } 
			
             echo " </span>";
             
            } ?>
           
          </div >

          <div id="artists" class="release c-white h-mob">
               <span class="pad-bot-20 sp2">OFFICIAL ACCOUNT </span><br><br>
               <div class="social">
               
               
           <? if($data_artist[0]->url_off_download){ ?>
          
           <?
			$arr_url_subject=explode(",",$data_artist[0]->url_off);
			$arr_url_download=explode(",",$data_artist[0]->url_off_download);
			for($i=0;$i<count($arr_url_subject);$i++){
				$gallery_path="";
	   		 foreach($data_onlinech as $item)
			{
				if($arr_url_subject[$i]==$item->id){
					$gallery_path=$item->thumnail_path;
				}
			}
				$str_img_download="<img src='".main_site_url().$gallery_path."'>";
		  
           echo " <span>";
           echo anchor(prep_url($arr_url_download[$i]), $str_img_download, 'target="_blank"' ); 
			 echo "</span>&nbsp;&nbsp;";	
            } 
			
            
             
            } ?>
          
               
             
               </div>
               
            </div>
          </div>

      </div>

    
     
    <div class=" container row" id="artists">

      <ul class="tab">
          <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Discography')" id="defaultOpen"><h5>Discography</h5> </a></li>
          <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Biography')"><h5>Biography</h5></a></li>
      </ul>

      <div id="Discography" class="tabcontent">
         <?php 
           foreach($data_view as $item)
			{
				$keep_track="";
				foreach($data_track_album as $item_track)
				{
					if($item_track->album_id==$item->album_id){
						$keep_track=$item_track->track;
					}
				}		
		?>
          <div class="playlists-item album">
             <a href="/artist/album/<?=$item->artist_name?>/<?=$item->album_id?>/<?=$item->artist_id?>" class="animsition-link">
             <div class="CoverImage " style="background-image:url(<?=main_site_url().$item->thumnail_path?>)"></div> 
             <span class="mask bg-blue-gra2"></span>
             <h5 class="name"><?=$item->album_name?></h5></a>
             <p class="desc wrap-text"><?=$keep_track?> tracks</p>
          </div>

         <?php } ?>
      </div>

      <div id="Biography" class="tabcontent">
        <p><?=$data_artist[0]->Description?></p> 
      </div>

    </div>
  

</section>
<div class="container"><?php include('footer.php') ?></div>
<?php include('includeJS.php') ?>

<script src="<?=base_url()?>assets/js/tab.js"></script>
<script>
	$(document).ready(function() {
  $('.main-nav li').find('a').removeClass('hvr-underline-from-center-active ');
  $('.main-nav li').eq(3).find('a').addClass('hvr-underline-from-center-active');
  });
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


