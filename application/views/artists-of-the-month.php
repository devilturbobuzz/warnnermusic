

 <div id="left-section" class="bg-blue h-mob">

       <div class="container-inner">
          <h5 class="c-white sp1 ">Artist <br>of the Month</h5>
          <h3 class="c-white sp2 mar-top"><?=$arr_artist_month[0]->Release_M?></h3> 
          <h5 class="c-white sp8 "><?=$arr_artist_month[0]->Release_Y?></h5> 
          <div class="line bg-white mar-top"></div>
          <h5 class="c-white sp3 pad-bot-20 pad-top-20">Artist detail</h5> 
          <div class="line bg-white "></div>
          <div id="artist-sync2" class="owl-carousel mar-top">
             
            <?
			  foreach($arr_artist_month as $item){
			?>
              <div class="item">
                 <a  href="/artist/detail/<?=$item->artist_id?>"><div class="item-bg lazyOwl" data-src="<?=main_site_url().$item->thumnail_path?>"></div></a>
                 <h4 class=" mar-top c-white sp2"><?=$item->artist_name?></h4>
                 <p class="mar-bot c-white"><?=$item->album_name?></p>
               
                
              </div>

 	   <? } ?>
              
          </div>

       </div>
      
    </div>

    <div id="right-section" class="">


      <div id="artist-sync1" class="owl-carousel bg-gray">
         <?
			  foreach($arr_artist_month as $item){
			?>
         <a  href="/artist/detail/<?=$item->artist_id?>">
         <div class="item">
             <div class="mask bg-blue"></div>
              <div class="title"><h2 class="c-white sp5"><?=$item->artist_name?></h2></div>
          	 	  <div class="item-bg lazyOwl"  data-src="<?=main_site_url().$item->pic_description_path?>"></div>
             
          </div>
          </a>
          <? } ?>
      </div>

     
      <div id="owlStatus">
         <span class="currentItem"><h5 class=" c-white sp5 result"></h5></span>
         <span class="owlItems"><h5 class="c-white sp5 ">/</h5></span>
         <span class="owlItems"><h5 class="c-white sp5 result"></h5></span>
      </div>



    </div>
    