

    
<section id="content" class=" animsition container">

      <div class="head-con img-artists">
         <div class="line bg-white abs opt-2"></div>
         <nav class="h-mob">
             <ol class="cd-breadcrumb custom-separator">
               <li><a href="/" class="animsition-link" >Home</a></li>
               <li class="current"><em>Artists</em></li>
             </ol>
          </nav>
          <h5 class="c-white abs sp2">Artists</h5>
      </div>
     
      <div class=" container row" id="artists">
	<? if($get_type!="SEARCH_TAG"){ ?>
      <div class="container-inner pad-bot-10" id="artists-category">
      
        
       <?php 
        foreach($data_nationality as $item)
			{
					$str_="";
				if($get_type==$item->nationality_name){
					$str_="current";
				}
						
		?>
         <div class="item <?=$str_?>"><a href="Artist/<?=$item->nationality_name?>" class="animsition-link"><h5><?=$item->nationality_name?></h5></a></div>
        
         <? } ?>
      </div>
<? } ?>
  

 
      <div class="container-inner" >
      <div id="new-projects"></div>

        <div class="row pad-top-20 c-gray3 pad-bot-20"><h5>Genres of music</h5></div>
       
        <div class="row" id="artists-geners">

            <div class="item current active" data-filter="*">
                <div class="CoverImage " style="background-image:url(assets/img/genres-thumb-01.jpg)"></div> 
                <span class="mask bg-blue"></span>
                <h5>All</h5>
            </div>
			 <?php 
        foreach($data_mood as $item)
			{
					
				if($get_type==$item->mood_description){
					
				}
						
		?>
        
         <div class="item" data-filter=".<?=$item->id?>">
                <div class="CoverImage " style="background-image:url(<?=main_site_url().$item->thumnail_path?>)"></div> 
                <span class="mask bg-blue"></span>
                <h5><?=$item->genres_name?></h5>
            </div>
        
         <? } ?>
         
        </div>
      <div class="row"> <div class="line bg-gray2 mar-bot-40 mar-top-40"></div></div>

      </div>
<BR/><BR/>
      <div class="row">
        <div class="container-inner">
            <div class="col-2 c-gray bold h-mob "><h5>Select artists</h5></div>
            
              <div id="artists" class="col-15 cd-form right-2">
                <p class="cd-select icon">
                  <select class="lists">
                    <option value="*">News release</option>
                    <option value="" data-option-value="asc" data-sort-by="name">A-Z</option>
                    <option value="" data-option-value="desc" data-sort-by="name">Z-A</option>
                  </select>
                </p>
              </div>

              <div class="row"> <div class="line bg-gray2  mar-top-20"></div></div>

        </div>


      </div>

    <div class="row">

  
      <div id="container" class=" container-inner pad-top-40">

     
		<?php 
           foreach($data_view as $item)
			{
				$keep_track="";
				foreach($data_track_artist as $item_track)
				{
					if($item_track->artist_id==$item->artist_id){
						$keep_track=$item_track->track;
					}
				}		
		?>
         <div class="playlists-item element <?=strtolower($item->Genres)?>">
          <a href="artist/detail/<?=$item->artist_id?>" class="animsition-link">
          <div class="CoverImage " style="background-image:url(<?=main_site_url().$item->thumnail_path?>)"></div> 
          <span class="mask bg-blue-gra2"></span>
          <h5 class="name"><?=$item->artist_name?></h5></a>
          <p class="desc wrap-text"><?=$item->totalalbum?> album - <?=$keep_track?> tracks</p>
        </div>

        <?php } ?>

      
    
     </div> <!-- #container -->
  
   

  </div>



</section>
<div class="row">
   <div class="container"><?php include('footer.php') ?></div>
</div>

<?php include('includeJS.php') ?>

  <!--script src="assets/js/jquery-1.7.1.min.js"></script-->

<script>

  $(function(){
  $('.main-nav li').find('a').removeClass('hvr-underline-from-center-active ');
  $('.main-nav li').eq(3).find('a').addClass('hvr-underline-from-center-active');
    var $container = $('#container');
    $container.infinitescroll({
        navSelector  : '#page_nav',    
        nextSelector : '#page_nav a',  
        itemSelector : '.element',     

        loading: {
            finishedMsg: 'No more pages to load.',
            img: 'http://i.imgur.com/qkKy8.gif',
            //msg: $('<div style="text-align: center">Loading…</div>')
          }
        },
        function( newElements ) {
          $container.isotope( 'appended', $( newElements ) ); 
        }
      );


    // init Isotope
    var $grid = $('#container').isotope({
      itemSelector: '.element',
      transitionDuration: 300,
       stagger: 20,
      hiddenStyle: {opacity: 0},
      visibleStyle: {opacity: 1},
      layoutMode: 'fitRows',
      getSortData: {
        name: '.name',
        symbol: '.symbol',
        number: '.number parseInt',
        category: '[data-category]',
        weight: function( itemElem ) {
          var weight = $( itemElem ).find('.weight').text();
          return parseFloat( weight.replace( /[\(\)]/g, '') );
        }
      }
    });

    $('.lists').on( 'change', function() {

      //$('#playListContainer li').find('.eq .bar-eq').removeClass('active');

      var sortByValue = $(this).find('option:selected').attr('data-sort-by');
      var order = $(this).find('option:selected').attr('data-option-value');
      var valAscending = (order == "asc"); 
      $grid.isotope({ sortBy: sortByValue,sortAscending : valAscending });
    });

    // filter functions
    var filterFns = {
    // show if number is greater than 50
    numberGreaterThan50: function() {
      var number = $(this).find('.number').text();
      return parseInt( number, 10 ) > 50;
    },
    // show if name ends with -ium
    a: function() {
      var name = $(this).find('.name').text();
      return name.match( /n$/ );
      }


    };


    /*$('#filters').on( 'click', 'button', function() {
      var filterValue = $( this ).attr('data-filter');
      filterValue = filterFns[ filterValue ] || filterValue;
      $grid.isotope({ filter: filterValue });
    });*/


    $('.button-group').each( function( i, buttonGroup ) {
      var $buttonGroup = $( buttonGroup );
      $buttonGroup.on( 'click', 'button', function() {
      $buttonGroup.find('.is-checked').removeClass('is-checked');
      $( this ).addClass('is-checked');
      });
    });


    //$('#artists-category .item').on('click', function(event){
      
      //$( "#new-projects" ).removeAttr();
      // var filterValue = $( this ).attr('data-hrf');
      //$( "#new-projects" ).load( "artists/"+filterValue+" #container" );
      //$container.isotope( 'appended', $( newElements ) ); 
      //alert("Hello\nHow are you?");

    //});

   // Genres of musicon click
    $('.item').on('click', function(event){

      var filterValue = $( this ).attr('data-filter');
      filterValue = filterFns[ filterValue ] || filterValue;
      $grid.isotope({ filter: filterValue });
      $('.item').removeClass('active');

      $('.item').removeAttr('style').removeClass('clicked');
      var $this = $(this);
      if($this.hasClass('clicked')){
          $this.removeAttr('style').removeClass('clicked');
      } else{
         $this.css('opacity','0.5').addClass('clicked');
      }  
    });


  });
</script>


