<?php include('header.php') ?>

<section id="content" class=" animsition container">

      <div class="head-con img-artists">
         <div class="line bg-white abs opt"></div>
         <nav class="h-mob">
             <ol class="cd-breadcrumb custom-separator">
               <li><a href="index.php" class="animsition-link" >Home</a></li>
               <li class="current"><em>Artists</em></li>
             </ol>
          </nav>
          <h5 class="c-white abs sp2">Artists</h5>
      </div>
     
      <div class=" container row" id="artists">

      <div class="container-inner category">
          <ul>
            <a href="#"><li class="current c-blue"><h5>International</h5></li></a>
            <a href="artists/thai.php" class="animsition-link"><li><h5>Thai</h5></li></a>
            <a href="#"><li><h5>Asia</h5></li></a>
            <a href="#"><li><h5>Kpop</h5></li></a>
            <a href="#"><li><h5>ETC</h5></li></a>
          </ul>
      </div>


      <div class="container-inner" >

        <div class="row pad-top-20 c-gray3 pad-bot-20"><h5>Genres of music</h5></div>
       
        <div class="row" id="artists-geners">

            <div class="item current">
                 <a href="playlists-sub-cat.php" class="animsition-link">
                  <div class="CoverImage " style="background-image:url(assets/img/genres-thumb-01.jpg)"></div> 
                  <span class="mask bg-blue"></span>
                  <h5>All</h5>
                </a>
            </div>

             <div class="item">
                 <a href="playlists-sub-cat.php" class="animsition-link">
                  <div class="CoverImage " style="background-image:url(assets/img/genres-thumb-02.jpg)"></div> 
                  <span class="mask bg-blue"></span>
                  <h5>POP</h5>
                </a>
            </div>

             <div class="item">
                 <a href="playlists-sub-cat.php" class="animsition-link">
                  <div class="CoverImage " style="background-image:url(assets/img/genres-thumb-03.jpg)"></div> 
                  <span class="mask bg-blue"></span>
                  <h5>Rock</h5>
                </a>
            </div>

             <div class="item">
                 <a href="playlists-sub-cat.php" class="animsition-link">
                  <div class="CoverImage " style="background-image:url(assets/img/genres-thumb-04.jpg)"></div> 
                  <span class="mask bg-blue"></span>
                  <h5>Jazz</h5>
                </a>
            </div>

             <div class="item">
                 <a href="playlists-sub-cat.php" class="animsition-link">
                  <div class="CoverImage " style="background-image:url(assets/img/genres-thumb-05.jpg)"></div> 
                  <span class="mask bg-blue"></span>
                  <h5> R&B</h5>
                </a>
            </div>

             <div class="item">
                 <a href="playlists-sub-cat.php" class="animsition-link">
                  <div class="CoverImage " style="background-image:url(assets/img/genres-thumb-06.jpg)"></div> 
                  <span class="mask bg-blue"></span>
                  <h5>Hip hop</h5>
                </a>
            </div>

             <div class="item">
                 <a href="playlists-sub-cat.php" class="animsition-link">
                  <div class="CoverImage " style="background-image:url(assets/img/genres-thumb-07.jpg)"></div> 
                  <span class="mask bg-blue"></span>
                  <h5>Grobol</h5>
                </a>
            </div>

             <div class="item">
                 <a href="playlists-sub-cat.php" class="animsition-link">
                  <div class="CoverImage " style="background-image:url(assets/img/playlists-thumb-01.jpg)"></div> 
                  <span class="mask bg-blue"></span>
                  <h5>Back in the 90s/00s</h5>
                </a>
            </div>

        </div>

      </div>
      <div class="row"> <div class="line bg-gray2 mar-bot-40 mar-top-40"></div></div>

      <div class="row">
        <div class="container-inner">
          <div class="col-25 c-gray bold h-mob"><h5>All - New release</h5></div>
              <div class="col-25 c-white h-mob">-</div>
              <div class="col-25 c-white h-mob">-</div>
              <div id="news" class="col-25 cd-form">
                <p class="cd-select icon">
                  
                  <select class="lists">
                      <option value="default">News release</option>
                      <option value="myorder:asc">A-Z</option>
                      <option value="myorder:desc">Z-A</option>
                  </select>

                  <li class="sort" data-sort="data-cat" data-order="desc">Descending</li>
          <li class="sort" data-sort="data-cat" data-order="asc">Ascending</li>
          <li class="sort active" data-sort="default" data-order="desc">Default</li>

                            <!--li class="sort active" data-sort="default" data-order="desc">Default</li-->


                   <!--select id="SortSelect">
        <option value="myorder:asc">Asc</option>
        <option value="myorder:desc">Desc</option>
    </select-->

                </p>
              </div>
        </div>
      </div>

    

      <div class="row pad-20 " id="infi-content"> 

      <div id="Container-mix" class="container-mix">

  <!--div class="mix category-1" data-myorder="Drake"></div>
  <div class="mix category-1" data-myorder="Justin Bieber"></div>
  <div class="mix category-1" data-myorder="Twenty One Pilots"></div>
  <div class="mix category-2" data-myorder="Adele"></div>
  <div class="mix category-1" data-myorder="Bryson Tiller"></div>
  <div class="mix category-1" data-myorder="Mike Posner"></div>
  <div class="mix category-2" data-myorder="DNCE"></div>
  <div class="mix category-2" data-myorder="Sia"></div-->

  <!--div class="gap"></div>
  <div class="gap"></div-->


        
              <div class="playlists-item mix category-1" data-myorder="a">
                 <a href="artists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-02.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>One Direction</h5></a>
                 <p class="desc wrap-text">2 album - 18,984 tracks</p>
              </div>

              <div class="playlists-item mix category-2" data-myorder="a">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-03.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
                 <p class="desc wrap-text">2 album - 18,984 tracks</p>

              </div>

              <div class="playlists-item mix" data-myorder="a">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-04.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
                  <p class="desc wrap-text">2 album - 18,984 tracks</p>

              </div>

              <div class="playlists-item mix" data-myorder="b">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-05.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
                 <p class="desc wrap-text">2 album - 18,984 tracks</p>
              </div>

             <div class="playlists-item mix" data-myorder="b">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-06.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
                 <p class="desc wrap-text">2 album - 18,984 tracks</p>
              </div>

              <div class="playlists-item mix" data-myorder="c">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-07.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
                 <p class="desc wrap-text">2 album - 18,984 tracks</p>
              </div>

              <div class="playlists-item mix" data-myorder="d">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-08.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
                 <p class="desc wrap-text">2 album - 18,984 tracks</p>
              </div>

              <div class="playlists-item mix" data-myorder="e">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-09.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
                 <p class="desc wrap-text">2 album - 18,984 tracks</p>
              </div>

              <div class="playlists-item mix" data-myorder="c">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-10.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
                 <p class="desc wrap-text">2 album - 18,984 tracks</p>
              </div>

              <div class="playlists-item mix" data-myorder="g">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-11.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
                 <p class="desc wrap-text">2 album - 18,984 tracks</p>
              </div>

             <div class="playlists-item mix" data-myorder="h">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-12.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
                 <p class="desc wrap-text">2 album - 18,984 tracks</p>
              </div>

              <div class="playlists-item mix" data-myorder="i">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-13.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
                 <p class="desc wrap-text">2 album - 18,984 tracks</p>
              </div>

              <div class="playlists-item mix" data-myorder="j">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-14.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
                 <p class="desc wrap-text">2 album - 18,984 tracks</p>
              </div>

              <div class="playlists-item mix" data-myorder="c">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-15.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
                 <p class="desc wrap-text">2 album - 18,984 tracks</p>
              </div>
         </div>

      <a class="row" id="next" href="artists2.php">next page?</a>

      </div>
      
      </div>


</section>
<div class="row">
   <div class="container"><?php include('footer.php') ?></div>
</div>

<?php include('includeJS.php') ?>

<script src='assets/js/jquery.mixitup.min.js'></script>
<script>
  $(document).ready(function() {
    $(function(){
        var $sortSelect = $('.lists'),
        $container = $('#Container-mix');
        $container.mixItUp({
          animation: {effects: 'fade',duration: 300}

        });
        $sortSelect.on('change', function(){
          $container.mixItUp('sort', this.value);
        });
    });

  });
</script>



