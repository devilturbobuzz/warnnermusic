
<section id="content" class=" animsition container">

    <div class="CoverImageX2 f-bure" style="background-image:url(<?=main_site_url().$arr_data[0]->thumnail_path?>)"></div> 
    <div class="overlay bg-blue opt-5 fix"></div>
    <div class="overlay bg-blue-gra opt-5 fix"></div>
thumnail_path

      <div class="head-con head-con-playlists ">
          <div class="line bg-white abs opt-2 row"></div>
          <nav class="h-mob ">
             <ol class="cd-breadcrumb custom-separator pad-left-40">
               <li><a href="/" class="animsition-link" >Home</a></li>
               <li><a href="/Artist" class="animsition-link" >Atists</a></li>
               <li><a href="/Artist/detail/<?=$arr_data[0]->artist_id?>" class="animsition-link" ><?=isset($arr_data[0]->artist_name)?$arr_data[0]->artist_name:""?></a></li>
               <li class="current"><em><?=isset($arr_data[0]->album_name)?$arr_data[0]->album_name:""?></em></li>
             </ol>
          </nav>
         

          

      </div><!--End header-->

      <div class="row ">
          <div class="container-inner">
            <div class="desc-con pad-top-60">
              <div class="album"><div class="CoverImage" style="background-image:url(<?=main_site_url().$arr_data[0]->thumnail_path?>)"></div> </div>
              <div class="pad-left-40" style="width: 77vw; ">
                  <h3 class="c-white pad-bot-5"><?=$arr_data[0]->subject?></h3>
                 <!-- <div class="line bg-white abs opt-2 row mar-bot-20"></div>-->
                  <p class=""><?=$arr_data[0]->description?></p><br>
                 
                  <span class=" h-mob c-white">Release on <?=$arr_data[0]->release_on?></span><br>
              <div class="desc">
                  <h4 class="c-white">คุณสามารถฟังเพลงใน PLAYLIST นี้ ได้จากบริการที่คุณใช้งานอยู่ โดยเลือกที่โลโก้ด้านล่าง</h4><br>
                      <div class="track pad-bot-15 c-white row ">
                        <? if($arr_data[0]->url_download){ ?>
          
           <?
	
			$arr_url_subject=explode(",",$arr_data[0]->url_subject);
			$arr_url_download=explode(",",$arr_data[0]->url_download);
			for($i=0;$i<count($arr_url_subject);$i++){
				$gallery_path="";
	   		 foreach($data_gallery as $item)
			{
				if($arr_url_subject[$i]==$item->id){
					$gallery_path=$item->thumnail_path;
				}
			}
				$str_img_download="<img style='width: 112px; height: 36px;' src='".main_site_url().$gallery_path."'>";
		  
           echo " <span>";
           echo anchor(prep_url($arr_url_download[$i]), $str_img_download, 'target="_blank"' ,'class="-link"'); 
            } 
			
             echo " </span>";
             
            } ?>
                      </div>
                  </div>            
            </div>
          </div> 
          </div> 
        
      </div>


      
      <div class="row">
          <div class="container-inner">
            <div class="desc-con">
                <div class="list-detail">
                  <!--?php include('player.php') ?-->
                  <div id="listContainer" class="playlistContainer pad-bot-40" style="margin-left: 25%;">
                
                   <?=$arr_data[0]->music_description?>
                   
                  </div>
                </div>
             
            </div>
          </div>
      </div>

</section>

<div class="container"><?php include('footer.php') ?></div>
<?php include('includeJS.php') ?>
<script>
  $(document).ready(function() {
  $('.main-nav li').find('a').removeClass('hvr-underline-from-center-active ');
  $('.main-nav li').eq(2).find('a').addClass('hvr-underline-from-center-active');
  });
</script>

<script src="<?=base_url()?>assets/js/jquery.audioControls.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
      var currentTrack = 0;
      $("#playListContainer").audioControls({
        autoPlay : true,
        shuffled: false,
        timer: 'increment',
        
        
        onPause: function(response){ 
          $('.song-thumb').removeClass('active');   
          $('#playListContainer li').eq(currentTrack).find('.eq .bar-eq').removeClass('active');
          $('#playListContainer li').eq(currentTrack).find('.eq .icon').addClass('active');


        },
        onPlay: function(response){
          $('.song-thumb').addClass('active');
          $('#playListContainer li').eq(currentTrack).find('.eq .bar-eq').addClass('active');
          $('#playListContainer li').eq(currentTrack).find('.eq .icon').removeClass('active');

        },
        
        onAudioChange : function(response){
        
          $('.songPlay').text(response.title + ' ...');
        
          currentTrack = response.index;  

          $('#playListContainer li').find('.eq .bar-eq').removeClass('active');
          $('#playListContainer li').eq(currentTrack).find('.eq .bar-eq').addClass('active');

          $('#playListContainer li').find('.eq .icon').addClass('active');
          $('#playListContainer li').eq(currentTrack).find('.eq .icon').removeClass('active');


      /*    $('.song-thumb').css('background-image', 'url(assets/img/playlists/thumb'+response.index+'.jpg)');
          if($('.song-thumb').hasClass('active')){
              $('.song-thumb').removeClass('active');
          }
          $('.song-thumb').addClass('active');*/

        },

        onVolumeChange : function(vol){
          var obj = $('.volume');
          if(vol <= 0){
            obj.attr('class','volume mute');
          } else if(vol <= 33){
            obj.attr('class','volume volume1');
          } else if(vol > 33 && vol <= 66){
            obj.attr('class','volume volume2');
          } else if(vol > 66){
            obj.attr('class','volume volume3');
          } else {
            obj.attr('class','volume volume1');
          }
        }
      });

    });
</script>




