
<section id="content" class=" animsition container">

      <div class="head-con img-news">
         <div class="line bg-white abs opt-2"></div>
         <nav class="h-mob">
             <ol class="cd-breadcrumb custom-separator pad-left-40">
               <li><a href="index.php" class="animsition-link" >Home</a></li>
               <li class="current"><em>Contact</em></li>
             </ol>
          </nav>
          <h5 class="c-white">Contact</h5>
      </div>
     
      <div class="" id="contact">
         <div class="row ">
           <div class="col-2 h-500 img-contact-02 ">
              <div class="contact-con pad-top-40">
                  <h3 class="c-white sp5 h-mob">Contact</h3>
              </div>
              <div class="social pad-left-40">
                  <h5 class="c-white sp5">folow us</h5><br><br>
                  <a href="https://www.facebook.com/warnermusicthailand" target="_blank">
                  <i class="fa fa-facebook c-white col-2" aria-hidden="true"> </i>
                  </a>
                  <a href="https://twitter.com/warnermusicth" target="_blank">
                  <i class="fa fa-twitter c-white col-2" aria-hidden="true">
                  
                  </i>
                  </a>
                   <a href="https://www.youtube.com/warnermusicthailand" target="_blank">
                  <i class="fa fa-youtube c-white col-2" aria-hidden="true">
                  
                  </i>
                   </a>
                  
                  
                </div>
           </div>
           


           <div class="col-6 h-500 img-contact-05">
              <div class="contact-con pad-top-60">
                   <h4 class="c-white sp5 pad-bot-20">contact form</h4>
			
                  <form class="cd-form floating-labels" action="/Contact/Save" method="post">
                      <fieldset>
                          <div class="">
                              <label class="cd-label" for="">Name</label>
                              <input class="" type="text" name="name" id="name" required>
                          </div> 

                         <div class="">
                             <label class="cd-label" for="phone">Phone</label>
                             <input class="" type="number" name="phone" id="phone" required>
                          </div>

                        

                          <div class="">
                             <label class="cd-label" for="cd-email">Email</label>
                             <input class="" type="email" name="cd-email" id="email" required>
                          </div>
      
                          <div class="">
                             <label class="cd-label" for="cd-textarea">Massage</label>
                             <textarea class="" name="cd-textarea" id="cd-textarea" required></textarea>
                          </div>

                          <div>
                             <input type="submit" value="Send">
                          </div>
                      </fieldset>
                  </form>

              </div>
           </div>
           <div class="col-2 h-500 img-contact-01 h-mob"></div>
         </div>

         <div class="row">
           <div class="col-2 h-250 img-contact-04 h-mob">
                <a href="https://goo.gl/maps/mGWJ9ab74pP2" target="_blank"><img class="icon-map" src="assets/img/logo-map.png"></a>
            </div>
           <div class="col-6 h-250 ">
              <div class="contact-con pad-top-60">
                 <h4 class="bold c-gray3">Warner Music (Thailand) Ltd.</h4><br>
                  9/9 Central Plaza Grand Rama 9, 10th Fl., Room 1001, 1007, Rama 9 Rd., Huai Khwang, Huai Khwang, Bangkok, 10320<br><br>
                P: (66) 2 106 3900<br>
                F: (66) 2 106 4006<br>
                 Email : warnermusicthailand@gmail.com</div>
              </div>
           <div class="col-2 h-250 img-contact-03 h-mob"></div>
         </div>

      
      </div>

  
<?php include('footer.php') ?>
</section>


<script>
  $(document).ready(function() {
   
  });

</script>

