<?php include(base_url().'header.php') ?>

<section id="searchID" class=" animsition container">
    
    <div class="head-con img-artists">
        <div class="line bg-white abs opt-2"></div>
        <nav class="h-mob">
            <ol class="cd-breadcrumb custom-separator">
               <li><a href="index.php" class="animsition-link" >Home</a></li>
               <li class="current"><em>404</em></li>
            </ol>
        </nav>
        <h5 class="c-white abs sp2">Search</h5>
    </div>
    <div class=" container row" id="search">
        <div class="container-inner" >
            <div class="row pad-top-40 c-gray3 pad-bot-20" style="text-align: center;">
                 <h1>404</h1>
                 <h3>OOPS! THAT PAGE CAN’T BE FOUND. </h3>
            </div>
        </div>
    </div>

    <div id="container" class=" container-inner pad-top-40 pad-bot-20 min-height-500" style="text-align: center;">
        <p class="pad-bot-20">It looks like nothing was found at this location. Maybe try one of the links below or a search?</p> 

        <div id="search">
            <input type="text" id="quick-search" placeholder="Search..." />
            <i class="fa fa-search" aria-hidden="true"></i>
        </div>
    </div> <!-- #container -->
 
<?php include('../../footer.php') ?>
</section>
<?php include('../../includeJS.php') ?>

