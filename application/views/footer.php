
<section class=" bg-gray " id="footer"><!-- ===========================  Footer =========================== -->
    <div class="container">
       <div class="col-2 pad-left"><img src="<?=base_url()?>assets/img/logo-big.png" alt="Warner Music Thailand"></div>
       <div class="col-2 mar-right"><h5 class="c-gray">Contact</h5>
           <p>Warner Music (Thailand) Ltd.<br><br>
           9/9 Central Plaza Grand Rama 9, 10th Fl., Room 1001, 1007, Rama 9 Rd., Huai Khwang, Huai Khwang, Bangkok, 10320<br><br>
           P: (66) 2 106 3900 <br>F: (66) 2 106 4006<br><br>
           <a href="https://goo.gl/maps/mGWJ9ab74pP2" target="_blank">View Map</a> </p></div>
       <div class="col-25 mar-right">
           <h5 class="c-gray">Search by tag</h5>
           <div class="tag">
             <? foreach($arr_tags as $item){ ?>
              <a href="/tag/<?=$item->tags_index?>"><?=$item->tags_index?></a>
              <? } ?>
           </div>           
       </div>
       <div class="col-1 ">
           <h5 class="c-gray">Sitemap</h5>
           <ul>
             <li><a href="/">Home</a></li>
             <li><a href="/News">News</a></li>
             <li><a href="/Playlist">Playlist</a></li>
             <li><a href="/Artist">Artists</a></li>
             <li><a href="/Contact">Contact</a></li>
             <li><a href="/About">About</a></li>
           </ul>
       </div>
       <div class="col-1 f-right">
          <h5 class="c-gray">Follow</h5>
          <ul>
             <li><a href="https://www.facebook.com/warnermusicthailand" target="_blank">Facebook</a></li>
             <li><a href="https://www.youtube.com/warnermusicthailand" target="_blank">Youtube</a></li>
             <li><a href="https://twitter.com/warnermusicth" target="_blank">Twitter</a></li>
            
           </ul>
       </div>
    </div>
  </section>
  <div id="footer-last" class="h-mob bg-white"> 
      <span class="copy-r">© 2016 Warner Music Thailand. All Rights Reserved.</span> <span class="f-right"> Terms of Use l Privacy Policy</span>
  </div>

</section>

