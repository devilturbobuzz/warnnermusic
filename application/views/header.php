<!DOCTYPE html>
<html lang="en">
<head>
 <!--base href="/warnermusic/"-->
<base href="/">

  <!-- Basic Page Needs 
 ================================================== -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Warner Music Thailand">
  <meta name="author" content="Warner Music Thailand">

  <link rel="shortcut icon" href="<?=base_url()?>assets/ico/favicon.ico">
  <link rel="apple-touch-icon" href="<?=base_url()?>assets/ico/icoapple-touch-icon.png">
  <title>Warner Music Thailand</title>
  
   <!-- CSS
  ================================================== -->
   <link rel="stylesheet" href="<?=base_url()?>assets/css/reset.css"> <!-- CSS reset -->

  <!-- Plugin CSS -->
  <link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/simple-line-icons.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/device-mockups.min.css">

 <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/jquery.fullPage.css" />

  <link rel="stylesheet" href="<?=base_url()?>assets/css/owl.carousel.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/owl.theme.css">

  <link rel="stylesheet" href="<?=base_url()?>assets/css/animsition.css">

 <link rel="stylesheet" href="<?=base_url()?>assets/css/pagestep.css">

 <link rel="stylesheet" href="<?=base_url()?>assets/css/artists.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/playlists.css">


  <!--link rel="stylesheet" href="<?=base_url()?>assets/css/ribbon.css" /-->
  <link rel="stylesheet" href="<?=base_url()?>assets/css/player.css" />

  <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/theme-2.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/css/form.css">

  <link rel="stylesheet" href="<?=base_url()?>assets/css/mixed.css">


  <!-- Custom Style-->
   <link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">  
  <!-- Responsive-->
  <link rel="stylesheet" href="<?=base_url()?>assets/css/responsive.css">  


   
  <script src="<?=base_url()?>assets/js/modernizr.js"></script> <!-- Modernizr -->
  <script src="<?=base_url()?>assets/js/html5shiv.js"></script>
  <!--[if lt IE 9]>
    <script src="../assets/js/html5shiv.js"></script>
    <script src="../assets/js/respond.js"></script>
  <![endif]-->
  
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '410510065953394', {
em: 'insert_email_variable'
});
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=410510065953394&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '410510065953394'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=410510065953394&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->


</head>



<body class="bg-init"> 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-99215123-1', 'auto');
  ga('send', 'pageview');

</script>
<div id="preload"><!-- Preload -->
   <!--div class="wrapper-preload">
       <div id="preloader_1"><span></span><span></span><span></span><span></span></div>
   </div-->
   <div class="loader-img"></div></div>
</div>


<header class ="content vw "><!--=========================== Header =========================== -->
     <div class="container"> 
     
         <div class="container-inner">
<a href="/">
              <div id="logo" class=""><img src="<?=base_url()?>assets/img/logo-white.png" alt="Warner Music Thailand"></div>  </a>                            
             

                <nav class="nav h-mob main-nav">
                   <ul>
                      <li><a href="/" class="hvr-underline-from-center animsition-link">Home</a></li>
                      <li><a href="/News" class="hvr-underline-from-center animsition-link">News</a></li>
                      <li><a href="/Playlist" class="hvr-underline-from-center animsition-link">Playlists</a></li>
                      <li><a href="/Artist" class="hvr-underline-from-center animsition-link">Artists</a></li>
                      <li><a href="/Contact" class="hvr-underline-from-center animsition-link">Contact</a></li>
                      <li><a href="/About" class="hvr-underline-from-center animsition-link">About</a></li>
                    </ul>
                </nav>
             <div id="quick-search" class="h-mob">
                <input type="text" id="quick-search" placeholder="Search..." />
                <i class="fa fa-search" aria-hidden="true"></i>
             </div>

            <a href="#0" class="cd-nav-trigger s-mob"><span></span></a>
             <nav class="cd-side-nav ">
                <ul>
                      <li><a href="/" class="hvr-underline-from-center animsition-link">Home</a></li>
                      <li><a href="/News" class="hvr-underline-from-center animsition-link">News</a></li>
                      <li><a href="/Playlist" class="hvr-underline-from-center animsition-link">Playlists</a></li>
                      <li><a href="/artist" class="hvr-underline-from-center animsition-link">Artists</a></li>
                      <li><a href="/Contact" class="hvr-underline-from-center animsition-link">Contact</a></li>
                      <li><a href="/About" class="hvr-underline-from-center animsition-link">About</a></li>
                      <!--li>
                           <input type="text" id="quick-search" placeholder="Search..." />
                           <i class="fa fa-search" aria-hidden="true"></i>
                      </li-->
                  </ul>
             </nav>
            



             
              
         </div>
     </div>

</header>
