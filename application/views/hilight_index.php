    <div class="row pad-bot-40 ">

              <div id="owlStatus-hi-light">
                  <span class="currentItem-hi-light"><h5 class=" c-gray sp5 result"></h5></span>
                  <span class="owlItems-hi-light"><h5 class="c-gray sp5 ">/</h5></span>
                  <span class="owlItems-hi-light"><h5 class="c-gray sp5 result"></h5></span>
              </div>

              <div class="col-25 "><h4 class="pad-bot-40 pad-top-40 c-gray">News Highlight</h4></div>
             
              <div id="hilight-news" class="col-75">
				<?php 
                  foreach($newsdat_hilight as $item)
					{
						
				?>
                  <div class=" item bg-gray">
                    <a href="/News/News-Detail/<?=$item->id?>" class="animsition-link" >
                      <div class="desc">
                        <div class="wrapper">
                           <h5 class="c-white sp1"><?=$item->subject_news?></h5>
                           <p class="c-white"><?=$item->date_news?></p>
                           <div class="read c-white sp1"> Read <img src="assets/img/arrow-right.png"></div>
                        </div>
                      </div>
                      <span class="mask bg-blue-gra2"></span>
                      <div class="item-bg lazyOwl" data-src="<?=main_site_url().$item->picture_highlight?>"></div>
                    </a>
                  </div>
				<?php } ?>

              </div>
          </div>