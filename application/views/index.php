
<section id="content" class="container animsition">
<?php $this->load->view('intro_index'); ?>
 

  <section class="container" id="news"><!-- =========================== News ===========================-->
   
    <div class="container-inner">
   
    <?php $this->load->view('news-tab'); ?>
    </div>
  </section><!--End News -->

  <section class="section vh bg-gray " id="artists"><!-- =========================== Artists =========================== -->

  <?php $this->load->view('artists-of-the-month'); ?>
  </section><!--End Artists -->

  <section class=" container-inner section " id="hilight"> <!-- =========================== Hi light =========================== -->
 <?php $this->load->view('hilight_index'); ?>
    

</section>

<script src="<?=base_url()?>assets/js/tab.js"></script>

<script>
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>




