 <section class="section" ><!-- =========================== Intro =========================== -->

    

    <div id="intro">
    <?php 
           foreach($arr_slidetop as $item)
			{
						
	?>
      <div class="item">
          <div id="title" class="c-white">
             <h1 class="sp5"><?=$item->artist_name?></h1>
             <h6 class="sp5"><?=$item->released?></h6>
             <span class="eff-cross sp3">
             <?=anchor(prep_url($item->url), 'View more', 'target="_blank"' ,'class="animsition-link"'); ?>
             </span>
          </div>
          <span class="overlay"></span>
          <div class="item-bg lazyOwl" data-src="<?=main_site_url().$item->head_picture_path?>"></div>
      </div>
<? } ?>
    </div>

  </section><!--End Intro -->