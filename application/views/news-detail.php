
<section id="content" class=" animsition container">

      <div class="head-con img-news">
         <div class="line bg-white abs opt"></div>
         <nav class="h-mob">
             <ol class="cd-breadcrumb custom-separator pad-left-40">
               <li><a href="/" class="animsition-link" >Home</a></li>
               <li><a href="/News" class="animsition-link" >News</a></li>
               <li class="current"><em><?=isset($newsdata[0]->subject_news)?$newsdata[0]->subject_news:""?></em></li>
             </ol>
          </nav>
          <h5 class="c-white abs">News Highlight</h5>
      </div>
     
      <div class=" container-inner" id="news">

        <div class="row pad-top-40">
           <div class="col-3 bold c-gray3"><?=isset($newsdata[0]->date_news)?$newsdata[0]->date_news:""?></div>
           <div class="col-7 img-100"><img src="<?=isset($newsdata[0]->head_picture_path)?main_site_url().$newsdata[0]->head_picture_path:""?>"></div>
        </div> 

        <div class="row pad-top-40">
            <div class="col-2 c-white">-</div>
            <div class="col-7"><h3 class="c-gray3"><?=isset($newsdata[0]->subject_news)?$newsdata[0]->subject_news:""?></h3></div>
            <div class="col-1 c-white">-</div>
        </div>

        <div class="row pad-top-40 pad-bot-100">
           <div class="col-35 c-white">-</div>
           <div class="col-65 pad-right-40"><?=isset($newsdata[0]->description_news)?$newsdata[0]->description_news:""?></div>
        </div> 
          

      </div>

  

</section>


<script>
  $(document).ready(function() {
   //$(".vertical-slider .vertical-slider-elem").eq(6).addClass('active');
  });
</script>

