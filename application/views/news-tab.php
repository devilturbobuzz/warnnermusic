 
  <link rel="stylesheet" href="<?=base_url()?>assets/css/tab-news.css">

 <ul class="tab-news">
    <li><a href="javascript:void(0)"  class="tablinks" onclick="openCity(event, 'New-releases')" id="defaultOpen">
    <h5>News releases</h5> </a></li>
    <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'New-musicvideo')"><h5>New music video</h5></a></li>
    <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'New-playlists')"><h5>New playlists</h5></a></li>

  </ul>

 

	
  <div id="New-releases" class="tabcontent">
      <div id="tab-con">
          <? if(isset($arr_album_index)){ $z=0;  ?>
				<? foreach($arr_album_index as $item){ ?>
				<? $z++; ?>
        <a href="/artist/album/<?=$item->artist_name?>/<?=$item->album_id?>/<?=$item->artist_id?>" class="item col-5 hei-3 bg-gray animsition-link">
        <? if($z==3 || $z==4 ){ ?>
          <div class="thumb-img col-5 bg-blue wrapper">
                <span class="icon c-white"><i class="fa fa-plus" aria-hidden="true"></i></span>
                <div class="mask bg-blue-gra"></div>
                <div class="CoverImage " style="background-image:url(<?=main_site_url().$item->thumnail_path?>);"></div> 

            </div>
        <? } ?>
       
          
            <div class="desc col-5 hei-100 wrapper">
                <h5 class="sp1 c-gray"><?=$item->artist_name?></h5>
                <?php
					$str_subject_detail=strip_tags($item->description);
					if (strlen($str_subject_detail) > 450){
						 $str_subject_detail = substr($str_subject_detail, 0, 450) . '...';
					 }else{
						 $str_subject_detail;
					}
				?>
                <p style="word-wrap: break-word;"><?=$str_subject_detail?></p>
             
            </div>
             <? if($z==1 || $z==2 || $z==5 || $z==6){ ?>
            <div class="thumb-img col-5 bg-blue wrapper">
                <span class="icon c-white"><i class="fa fa-plus" aria-hidden="true"></i></span>
                <div class="mask bg-blue-gra"></div>
                <div class="CoverImage " style="background-image:url(<?=main_site_url().$item->thumnail_path?>);"></div> 
            </div>
            <?  } ?>
          </a>
         <? }
   			 }    
		?>

      </div>
  </div>
<div id="New-musicvideo" class="tabcontent"><!-- News Music VDO -->
   <div id="tab-con">
   <? if(isset($arr_video)){ $h=0;  ?>
	<? foreach($arr_video as $item){ ?>
	<? $h++; ?>
	  <div href="#" class="item col-5 hei-3 bg-gray">
	 <? if($h==3 || $h==4 ){ ?>
	 	    <div class="thumb-img col-5 bg-blue wrapper">
                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/<?=$item->youtubeid?>" frameborder="0" allowfullscreen></iframe>
            </div>
	 <? } ?>
	 <div class="desc col-5 hei-100 wrapper">
                <h5 class="sp1 c-gray"><?=$item->subject?></h5>
                <?php
					$str_subject_detail=strip_tags($item->description);
					if (strlen($str_subject_detail) > 450){
						 $str_subject_detail = substr($str_subject_detail, 0, 450) . '...';
					 }else{
						 $str_subject_detail;
					}
				?>
                <p><?=$str_subject_detail?></p>
               
            </div>
	  <? if($h==1 || $h==2 || $h==5 || $h==6){ ?>
	    <div class="thumb-img col-5 bg-blue wrapper">
                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/<?=$item->youtubeid?>" frameborder="0" allowfullscreen></iframe>
            </div>
	   <? } ?>
	    </div>
	<? } ?>
	<? } ?>
    
      </div>
  </div>
  <div id="New-playlists" class="tabcontent"><!-- New Playlists -->
    <div id="playlists">
      <div id="tab-con">
         
         <? if(isset($arr_playlist)){  $j=0; ?>
         
				<? foreach($arr_playlist as $item){ ?>
				<? $j++; ?>
			
          <a href="/Playlist/detail/<?=$item->id?>" class="item col-5 hei-3 bg-gray animsition-link">
            <? if($j==3 || $j==4 ){ ?>
      	   <div class="thumb-img col-5 bg-blue wrapper">
                <span class="icon c-white"><i class="fa fa-plus" aria-hidden="true"></i></span>
                <div class="mask bg-blue-gra"></div>
                <div class="CoverImage " style="background-image:url(<?=main_site_url().$item->thumnail_path?>);"></div> 
            </div>
       	  <? } ?>
            <div class="desc col-5 hei-100 wrapper">
                <h5 class="sp1 c-gray"><?=$item->subject?></h5>
                <?php
					$str_subject_detail=strip_tags($item->description);
					if (strlen($str_subject_detail) > 600){
						 $str_subject_detail = substr($str_subject_detail, 0, 600) . '...';
					 }else{
						 $str_subject_detail;
					}
				?>
                
                <p><?=$str_subject_detail?></p>
             
            </div>
            <? if($j==1 || $j==2 || $j==5 || $j==6){ ?>
            <div class="thumb-img col-5 bg-blue wrapper">
                <span class="icon c-white"><i class="fa fa-plus" aria-hidden="true"></i></span>
                <div class="mask bg-blue-gra"></div>
                <div class="CoverImage " style="background-image:url(<?=main_site_url().$item->thumnail_path?>);"></div> 

            </div>
            <? } ?>
          </a>
           
          

          <?	

           	}      
                    
				}
		    ?>
      </div>

      
    </div>
  </div>


