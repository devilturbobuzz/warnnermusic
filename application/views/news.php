

<section id="content" class=" animsition container">

      <div class="head-con img-news">
         <div class="line bg-white abs opt-2"></div>
         <nav class="h-mob">
             <ol class="cd-breadcrumb custom-separator pad-left-40">
               <li><a href="/" class="animsition-link" >Home</a></li>
               <li href="/News" class="current"><em>News</em></li>
             </ol>
          </nav>
          <h5 class="c-white abs sp2">News</h5>
      </div>
     
      <div class=" container-inner" id="news">
          <div class="pad-top-40 pad-bot-40"> 
              <div class="col-25 c-gray bold h-mob">The latest information </div>
              <div class="col-25 c-gray h-mob"><?=$get_year?$get_year:"Select Years"?></div>
              <div class="col-25 c-white h-mob">-</div>
              <!-- Select years -->
              <div id="news" class="col-25 cd-form">
                <p class="cd-select icon">
                  <select class="calendar" id="calendar" onChange="select_news()">
                      <option value="" <?=$get_year==""?"selected":""?>>Select Years</option>
                      <option value="2016" <?=$get_year=="2016"?"selected":""?>>2016</option>
                      <option value="2017" <?=$get_year=="2017"?"selected":""?>>2017</option>
                  </select>
                </p>
              </div>
          </div>
          <div class="line bg-gray01 mar-bot-40 h-mob"></div>

          <!-- News hi-light -->
          <div class="row pad-bot-40 ">
              <div class="col-25 h-mob"><h4 class="pad-bot-40 c-gray">Hi-Light <br>news and activities</h4><p>12 June 2016</p></div>
              <div id="owlStatus-hi-light" class="h-mob">
                  <span class="currentItem-hi-light"><h5 class=" c-gray sp5 result"></h5></span>
                  <span class="owlItems-hi-light"><h5 class="c-gray sp5 ">/</h5></span>
                  <span class="owlItems-hi-light"><h5 class="c-gray sp5 result"></h5></span>
              </div>

              <!--div class="col-75"></div-->
              <div id="hilight-news" class="col-75">
				<?php 
                  foreach($newsdat_hilight as $item)
					{
						
				?>
                  <div class=" item bg-gray">
                    <a href="/News/News-Detail/<?=$item->id?>" class="animsition-link" >
                      <div class="desc">
                        <div class="wrapper">
                           <h5 class="c-white sp1"><?=$item->subject_news?></h5>
                           <p class="c-white"><?=$item->date_news?></p>
                           <div class="read c-white sp1"> Read <img src="assets/img/arrow-right.png"></div>
                        </div>
                      </div>
                      <span class="mask bg-blue-gra2"></span>
                      <div class="item-bg lazyOwl" data-src="<?=main_site_url().$item->picture_highlight?>"></div>
                    </a>
                  </div>
				<?php } ?>
                

                 
              </div>
          </div>
          <!-- News all list -->
          <div class="row"><div class="line bg-gray01 mar-bot-40"></div></div>
          <div class="row pad-bot-40" >
            <div class="col-25 h-mob"><h5 class="c-gray">2016</h5></div>
            <div class="col-75" id="">
              <div id="container">
                <div class="element">
				<?php 
                  foreach($newsdata as $item)
					{
						
				?>
                  <div class="row eff-2 " >
                    <a href="/News/News-Detail/<?=$item->id?>" class="animsition-link" >
                      <div class="col-33 h-mob"><h5 class="c-gray"><?=$item->date_news?></h5></div>
                      <div class="col-33 img-100"><img src="<?=main_site_url().$item->thumnail_path?>"></div>
                      <div class="col-33 pad-left-20"><?=$item->subject_news?></div>
                    </a>
                  </div>
                  <div class="row h-mob"><div class="line bg-gray01 mar-bot-40 mar-top-40"></div></div>
			<?php } ?>
                
                </div>
              </div> <!-- #container -->
  
              <nav id="page_nav" class="row bg-red">
                <a href="pages/news/2.html"></a>
              </nav>

            </div>
              
          </div>

      </div>
  

</section>


<script>
	 function select_news(){
	 window.open("/News/"+$("#calendar").val(),"_self");
 }
  $(document).ready(function() {

   var $container = $('#container');
    $container.infinitescroll({
        navSelector  : '#page_nav',    
        nextSelector : '#page_nav a',  
        itemSelector : '.element',     

        loading: {
            finishedMsg: 'No more pages to load.',
            img: 'http://i.imgur.com/qkKy8.gif',
            //msg: $('<div style="text-align: center">Loading…</div>')
          }
        }
        /*function( newElements ) {
          $container.isotope( 'appended', $( newElements ) ); 
        })*/
      );

  });
</script>

