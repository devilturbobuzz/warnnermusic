    <div class="parent">
            <div class="main-item item-1 "><ul class="controls"><li><a  class="left" data-attr="prevAudio"></a></li></ul></div>
            <div class="main-item item-1 "><ul class="controls"><li><a class="play" data-attr="playPauseAudio"></a></li></ul></div>
            <div class="main-item item-1 "><ul class="controls"><li><a class="right" data-attr="nextAudio"></a></li></ul></div>
            <div class="main-item item item-3-1 "></div>
            <div class="main-item item-2 pad-top-5 "> 
              <div class="progress ">
                 <div data-attr="seekableTrack" class="seekableTrack"></div>
                 <div class="updateProgress"></div>
              </div>
            </div>
          <div class="main-item item-3">
            <div class="audioDetails">
              <span data-attr="timer" class="audioTime c-blue2">0:00</span>
            </div>
          </div>
          <div class="main-item item-4" style="display: none;">
            <div class="song-thumb "><div class="mask bg-blue opt-5"></div></div>
          </div>
          <div class="main-item item item-5 pad-top-10">
             <p class="songPlay song-name bold c-white">Song name</p>
             <!--p class="artist-name">Artis name</p-->
          </div>
          <div class="main-item item-9 h-mob">
            <div class="volumeControl">
              <div class="volume volume1"></div>
              <input class="bar" data-attr="rangeVolume" type="range" min="0" max="1" step="0.1" value="0.5" />
            </div>
          </div>
    </div>
