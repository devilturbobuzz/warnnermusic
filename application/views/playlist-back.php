<?php include('header.php') ?>

<section id="content" class=" animsition container">

      <div class="head-con img-news">
         <div class="line bg-white abs opt"></div>
         <nav class="h-mob">
             <ol class="cd-breadcrumb custom-separator">
               <li><a href="index.php" class="animsition-link" >Home</a></li>
               <li class="current"><em>Playlists</em></li>
             </ol>
          </nav>
          <h5 class="c-white abs sp2">Playlists</h5>
      </div>
     
      <div class=" container-inner" id="playlists-">
         <div class="row pad-top-40 pad-bot-40 c-gray3 "><h5>Mood and theme playlists</h5></div>
         <div class="row" id="playlists-cat">

        
            <div class="item">
                 <a href="playlists-sub-cat.php" class="animsition-link">
                  <div class="CoverImage " style="background-image:url(assets/img/playlists-thumb-01.jpg)"></div> 
                  <span class="mask bg-blue"></span>
                  <h5>Back in the 90s/00s</h5>
                </a>
            </div>
            
             <div class="item">
                <div class="CoverImage " style="background-image:url(assets/img/playlists-thumb-02.jpg)"></div> 
                <span class="mask bg-blue"></span>
                <h5>Back in the 90s/00s</h5>
            </div>

             <div class="item">
                <div class="CoverImage " style="background-image:url(assets/img/playlists-thumb-03.jpg)"></div> 
                <span class="mask bg-blue"></span>
                <h5>Back in the 90s/00s</h5>
            </div>

             <div class="item">
                <div class="CoverImage " style="background-image:url(assets/img/playlists-thumb-04.jpg)"></div> 
                <span class="mask bg-blue"></span>
                <h5>Back in the 90s/00s</h5>
            </div>

             <div class="item">
                <div class="CoverImage " style="background-image:url(assets/img/playlists-thumb-05.jpg)"></div> 
                <span class="mask bg-blue"></span>
                <h5>Back in the 90s/00s</h5>
            </div>

             <div class="item">
                <div class="CoverImage " style="background-image:url(assets/img/playlists-thumb-01.jpg)"></div> 
                <span class="mask bg-blue"></span>
                <h5>Back in the 90s/00s</h5>
            </div>

             <div class="item">
                <div class="CoverImage " style="background-image:url(assets/img/playlists-thumb-01.jpg)"></div> 
                <span class="mask bg-blue"></span>
                <h5>Back in the 90s/00s</h5>
            </div>

             <div class="item">
                <div class="CoverImage " style="background-image:url(assets/img/playlists-thumb-01.jpg)"></div> 
                <span class="mask bg-blue"></span>
                <h5>Back in the 90s/00s</h5>
            </div>

             <div class="item">
                <div class="CoverImage " style="background-image:url(assets/img/playlists-thumb-01.jpg)"></div> 
                <span class="mask bg-blue-gray"></span>
                <h5>Back in the 90s/00s</h5>
            </div>
         </div>
      </div>

      <div class="row"> <div class="line bg-gray2 mar-bot-40"></div></div>

      <div class=" container-inner" id="playlists">
          <div class="row"> 
             <h5 class="row  pad-bot-40 c-gray3 ">Selected Playlists</h5>
          </div>

          <div class="row" id="infi-content"> 
            
              <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-01.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

              <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-02.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

              <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-03.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

              <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-04.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

              <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-05.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

             <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-06.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

              <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-07.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

              <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-08.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

              <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-09.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

              <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-10.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

              <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-11.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

             <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-12.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

              <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-13.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

              <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-14.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

              <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-15.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

              <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-16.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

              <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-17.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

             <div class="playlists-item">
                 <a href="playlists-detail.php" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(assets/img/playlists-item-18.jpg)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5>Back in the 90s/00s</h5></a>
              </div>

             
         </div>
         <a id="next" href="playlists2.html">next page?</a>

      </div>

 </section> 
<?php include('footer.php') ?>
<?php include('includeJS.php') ?>
<script>
  $(document).ready(function() {
   //$(".vertical-slider .vertical-slider-elem").eq(6).addClass('active');
  });
</script>

