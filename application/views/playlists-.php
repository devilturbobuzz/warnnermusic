<?php include('header.php') ?>

<section id="content" class=" animsition container">

      <div class="head-con img-news">
         <div class="line bg-white abs opt"></div>
         <nav>
             <ol class="cd-breadcrumb custom-separator">
               <li><a href="index.php" class="animsition-link" >Home</a></li>
               <li class="current"><em>Playlists</em></li>
             </ol>
          </nav>
          <h5 class="c-white abs sp2">Playlists</h5>
      </div>
     
      <div class=" container-inner" id="artists">
         <div class="row pad-top-40 pad-bot-40 c-gray3 "><h5>Mood and theme playlists</h5></div>
         <div class="row" id="artists-cat">
            <div class="item">01</div>
            <div class="item">02</div>
            <div class="item">03</div>
            <div class="item">04</div>
            <div class="item">05</div>
            <div class="item">06</div>
            <div class="item">07</div>
            <div class="item">08</div>
            <div class="item">09</div>
         </div>


      </div>
     <div class="row"> <div class="line bg-gray2 mar-bot-40"></div></div>

      <div class=" container-inner" id="artists">
          <div class="row"> 
             <h5 class="row pad-top-40 pad-bot-40 c-gray3 ">Selected Playlists</h5>
          </div>
          <div class="row" id="infi-content"> 

          <h1>Infinite Scroll Demo</h1>
    
    <p>One for all and all for one, Muskehounds are always ready. One for all and all for one, helping everybody. One for all and all for one, it's a pretty story. Sharing everything with fun, that's the way to be. One for all and all for one, Muskehounds are always ready. One for all and all for one, helping everybody. One for all and all for one, can sound pretty corny. If you've got a problem chum, think how it could be.</p>

    <p>Barnaby The Bear's my name, never call me Jack or James, I will sing my way to fame, Barnaby the Bear's my name. Birds taught me to sing, when they took me to their king, first I had to fly, in the sky so high so high, so high so high so high, so - if you want to sing this way, think of what you'd like to say, add a tune and you will see, just how easy it can be. Treacle pudding, fish and chips, fizzy drinks and liquorice, flowers, rivers, sand and sea, snowflakes and the stars are free. La la la la la, la la la la la la la, la la la la la la la, la la la la la la la la la la la la la, so - Barnaby The Bear's my name, never call me Jack or James, I will sing my way to fame, Barnaby the Bear's my name.</p>

    <p>There's a voice that keeps on calling me. Down the road, that's where I'll always be. Every stop I make, I make a new friend. Can't stay for long, just turn around and I'm gone again. Maybe tomorrow, I'll want to settle down, Until tomorrow, I'll just keep moving on.</p>

    <p>Top Cat! The most effectual Top Cat! Who's intellectual close friends get to call him T.C., providing it's with dignity. Top Cat! The indisputable leader of the gang. He's the boss, he's a pip, he's the championship. He's the most tip top, Top Cat.</p>

    <p>Hong Kong Phooey, number one super guy. Hong Kong Phooey, quicker than the human eye. He's got style, a groovy style, and a car that just won't stop. When the going gets tough, he's really rough, with a Hong Kong Phooey chop (Hi-Ya!). Hong Kong Phooey, number one super guy. Hong Kong Phooey, quicker than the human eye. Hong Kong Phooey, he's fan-riffic!</p>

    <p>Barnaby The Bear's my name, never call me Jack or James, I will sing my way to fame, Barnaby the Bear's my name. Birds taught me to sing, when they took me to their king, first I had to fly, in the sky so high so high, so high so high so high, so - if you want to sing this way, think of what you'd like to say, add a tune and you will see, just how easy it can be. Treacle pudding, fish and chips, fizzy drinks and liquorice, flowers, rivers, sand and sea, snowflakes and the stars are free. La la la la la, la la la la la la la, la la la la la la la, la la la la la la la la la la la la la, so - Barnaby The Bear's my name, never call me Jack or James, I will sing my way to fame, Barnaby the Bear's my name.</p>

    <p>There's a voice that keeps on calling me. Down the road, that's where I'll always be. Every stop I make, I make a new friend. Can't stay for long, just turn around and I'm gone again. Maybe tomorrow, I'll want to settle down, Until tomorrow, I'll just keep moving on.</p>

    <p>Top Cat! The most effectual Top Cat! Who's intellectual close friends get to call him T.C., providing it's with dignity. Top Cat! The indisputable leader of the gang. He's the boss, he's a pip, he's the championship. He's the most tip top, Top Cat.</p>

    <p>Hong Kong Phooey, number one super guy. Hong Kong Phooey, quicker than the human eye. He's got style, a groovy style, and a car that just won't stop. When the going gets tough, he's really rough, with a Hong Kong Phooey chop (Hi-Ya!). Hong Kong Phooey, number one super guy. Hong Kong Phooey, quicker than the human eye. Hong Kong Phooey, he's fan-riffic!</p>
    
  </div>
  
  <a id="next" href="playlists2.html">next page?</a>





          </div>

      </div>

  
<?php include('footer.php') ?>
</section>

<?php include('includeJS.php') ?>
<script>
  $(document).ready(function() {
   //$(".vertical-slider .vertical-slider-elem").eq(6).addClass('active');
  });
</script>

