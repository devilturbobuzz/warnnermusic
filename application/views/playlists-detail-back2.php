<?php include('header.php') ?>
        <!--link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet"-->

<section id="content" class=" animsition container ">

    <div class="head-con img-news"><!-- Header -->
        <div class="line bg-white abs opt"></div>
          <nav>
             <ol class="cd-breadcrumb custom-separator">
               <li><a href="index.php" class="animsition-link" >Home</a></li>
                <li><a href="playlists.php" class="animsition-link" >Playlists</a></li>
               <li class="current"><em>Back in the 90s/80s</em></li>
             </ol>
          </nav>
        <h5 class="c-white abs sp2">Playlists</h5>
    </div>



    <div class=" container" ><!-- Content here -->
      <div id="playlists">
        <div class="left-section ">
          <div class="content">

             <img src="assets/img/playlists-item-01.jpg" class="pad-bot-20"><br>
             
             <h5 class="c-gray3 pad-left-20"> <i class="fa fa-play-circle-o" aria-hidden="true"></i>Back in the 90s/80s</h5>
             <div class="row pad-top-20 pad-bot-20 pad-bot-20 "><div class="line bg-gray2"></div></div>
              <span class="pad-right-20 pad-left-20 pad-bot-20"><i class="fa fa-music" aria-hidden="true"></i>96 Tracks</span><br>
              <span class="pad-right-20 pad-left-20 pad-bot-20"><i class="fa fa-clock-o" aria-hidden="true"></i>20 hrs 34 mins Length</span><br>
              <span class="pad-right-20 pad-left-20 pad-bot-20"><i class="fa fa-user" aria-hidden="true"></i>12 Artists</span><br>
             <p class="pad-left-20 pad-right-20">รวมเพลงฟังสบายๆ กันก่อนนอน หรือจะหาอะไรทำยามค่ำคืนก็สามารถเปิด Playlist นี้คลอๆ 
             ฟังได้เพลินๆ จัดไปสบายๆ ราตรีนี้ยังอีกยาวไกล คุณสามารถฟังเพลงใน Playlist นี้ </p>
          </div>
        </div> 
        <div class="right-section ">
          <div class="row pad-top-20 pad-bot-20 ">
            <div class="col-6">
              <h4 class="c-blue pad-left-20 pad-bot-10">Back in the 90s/80s</h4>
              <p class="pad-left-20">Release on 13 June 2016</p>
            </div>
            <div class="col-4 right">
              <span class="pad-right-20"><i class="fa fa-music" aria-hidden="true"></i>96 Tracks</span>
              <span class="pad-right-20"><i class="fa fa-clock-o" aria-hidden="true"></i>20 hrs 34 mins Length</span>
              <span class="pad-right-20"><i class="fa fa-user" aria-hidden="true"></i>12 Artists</span>
            </div>
           
          </div>

          <div class="row"><div class="line bg-gray2"></div></div>
          

          <div class="row player">
              <div class="col-15 section">
                <!--div class="record"></div-->
                <button class="btn btn-success btn-block" id="playPause">
                   <span id="play"><i class="fa fa-play c-white" aria-hidden="true"></i></span>
                   <span id="pause" style="display: none"><i class="fa fa-pause c-white" aria-hidden="true"></i></span>
                </button>
              </div>
              <div class="col-85 pad-20"><div id="waveform"></div></div>
          </div>


          <div class="row ">
              <div class="group bg-gray c-gray3">
                      <div class="title"><h5><i class="fa fa-list" aria-hidden="true"></i>Track</h5></div>
                      <div class="artist"><h5>Artists</h5></div>
                      <div class="album"><h5>Album</h5></div>
                      <div class="time"><h5>Length</h5></div>
                      <div class="download"><h5>Download</h5></div>
              </div>

              <div class="list-group" id="playlist">
                  <div class="list-group-item">
                      <div class="title"><a href="assets/media/Nirvana.mp3"><i class="fa fa-play" aria-hidden="true"></i>Smells Like Teen Spirit</a></div>
                      <div class="artist">Nirvana</div>
                      <div class="album">Nevermind</div>
                      <div class="time">0:21</div>
                      <div class="download"><a href="#"><i class="fa fa-cloud-download" aria-hidden="true"></i>Download</a></div>
                  </div>

                  <div class="list-group-item">
                      <div class="title"><a href="assets/media/Nirvana.mp3"><i class="fa fa-play" aria-hidden="true"></i>Smells Like Teen Spirit</a></div>
                      <div class="artist">Nirvana</div>
                      <div class="album">Nevermind</div>
                      <div class="time">0:21</div>
                      <div class="download"><a href="#"><i class="fa fa-cloud-download" aria-hidden="true"></i>Download</a></div>
                  </div>

                  <div class="list-group-item last">
                      <div class="title"><a href="assets/media/Nirvana.mp3"><i class="fa fa-play" aria-hidden="true"></i>Smells Like Teen Spirit</a></div>
                      <div class="artist">Nirvana</div>
                      <div class="album">Nevermind</div>
                      <div class="time">0:21</div>
                      <div class="download"><a href="#"><i class="fa fa-cloud-download" aria-hidden="true"></i>Download</a></div>
                  </div>

              </div>
          </div>

        </div>
      </div>
    </div><!-- End container -->

 </section> 
<?php include('footer.php') ?>
<?php include('includeJS.php') ?>
<script>
  $(document).ready(function() {
   //$(".vertical-slider .vertical-slider-elem").eq(6).addClass('active');
  });
</script>

