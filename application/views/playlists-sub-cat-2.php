<?php include('header.php') ?>

<section id="content" class=" animsition container">
    <div id="about">
      <div class="head-con img-about">
         <span class="about-img h-mob"><img src="assets/img/about01.jpg"></span>
         <h1 class="rota-90 abs c-white left-100 top-1">Culture</h1>
         <div class="overlay opt"></div>
         <div class="line bg-white abs opt"></div>
         <nav class="h-mob">
             <ol class="cd-breadcrumb custom-separator">
               <li><a href="index.php" class="animsition-link" >Home</a></li>
               <li class="current"><em>About</em></li>
             </ol>
         </nav>
         <h5 class="c-white abs sp2">About</h5>
      </div>
  </div>   


      <div class=" container-inner" id="about">

          <div class="row pad-top-40 pad-bot-40">
            <div class="col-1 c-white h-mob">-</div>
            <div class="col-3 h-mob">-</div>
            <div class="col-5"><h3>WMG embraces a unique collection of world-renowned companies with distinct cultures,</h3></div>
            <div class="col-1 c-white h-mob">-</div>
          </div>

          <div class="row pad-top-40 pad-bot-100">
            <div class="col-2 c-white">-</div>
            <div class="col-6">
              
              Edward Christopher "Ed" Sheeran (born 17 February 1991) is an English singer-songwriter. His early childhood memories include listening to Van 
Morrison on his trips to London with his parents and going to an intimate Damien Rice gig in Ireland when he was eleven. Raised in Framlingham, 
Suffolk, he moved to London in 2008 to pursue a musical career. In early 2011, Sheeran released an independent extended play, No. 5 Collaborations 
Project, which caught the attention of both Elton John and Jamie Foxx. He then signed to Asylum Records. His 2011 debut album, +, containing 
the singles "The A Team" and "Lego House", was certified quintuple platinum in the United Kingdom. In 2012, he won two BRIT Awards for Best 
British Male and British Breakthrough. "The A Team" won the Ivor Novello Award for Best Song Musically and Lyrically.<br><br>

Sheeran began to be known in the United States in 2012. He made a guest appearance on Taylor Swift's fourth album, Red, and wrote songs 
for One Direction. "The A Team" was nominated for Song of the Year at the 2013 Grammy Awards and he performed the song in duet with 
Elton John during the ceremony. He spent much of 2013 touring North America as the opening act for Taylor Swift's Red Tour. He was 
nominated for Best New Artist at the 56th Annual Grammy Awards.<br><br>

His second studio album is titled x (read as "multiply"), and is scheduled for a worldwide release on 23 June 2014. He recorded the album in Los 
Angeles with legendary producer Rick Rubin, who advised Sheeran to record the album acoustically with a good mood; extra instrumentation 
was added later. He debuted an album track, "Tenerife Sea", at Madison Square Garden in October 2013. "Sing", the lead single, which is co-written 
with Pharrell Williams, was released on 7 April 2014.
            </div>
            <div class="col-2 c-white">-</div>

          </div>

      
      </div>

  
<?php include('footer.php') ?>
</section>

<?php include('includeJS.php') ?>
<script>
  $(document).ready(function() {
   //$(".vertical-slider .vertical-slider-elem").eq(6).addClass('active');
  });
</script>

