

<section id="content" class=" animsition container">

      <div class="head-con h-500 head-con-playlists">

         <!--img src="assets/img/playlists-sub-cat-header.jpg"  class="cover abs"/-->
         <div class="CoverImage " style="background-image:url(<?=main_site_url().$arr_playlist_cat[0]->head_picture_path?>)"></div> 

         <div class="overlay-2 bg-blue-gra opt-5"></div>
        

         <div class="line bg-white abs opt-2"></div>
         <nav class="h-mob">
             <ol class="cd-breadcrumb custom-separator pad-left-40">
               <li><a href="/" class="animsition-link" >Home</a></li>
               <li><a href="/Playlist" class="animsition-link" >Playlists</a></li>
               <li class="current"><em><?=$arr_playlist_cat[0]->subject?></em></li>
             </ol>
          </nav>
          <h5 class="c-white abs">Playlists</h5>

          <div class="desc ">
           <h2 class="c-white pad-bot-15"><?=$arr_playlist_cat[0]->subject?></h2>
          </div>


      </div>
     
      <div class=" container row pad-20" id="playlists">
      
        <div class="row"> 
             <h5 class="row  pad-bot-40 c-gray3 ">Selected Playlists</h5>
          </div>

          <div class="row" id="infi-content"> 
              <? foreach($arr_playlist_sub_cat as $item){ ?>
               <div class="playlists-item">
                 <a href="/Playlist/detail/<?=$item->id_playlist?>" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(<?=main_site_url().$item->thumnail_path?>)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5><?=$item->subject?></h5></a>
              </div>
			<? } ?> 
         
             
         </div>
         <a id="next" href="playlists2.html">next page?</a>


      </div>

  

</section>
<div class="container"><?php include('footer.php') ?></div>


<script>
  $(document).ready(function() {
  $('.main-nav li').find('a').removeClass('hvr-underline-from-center-active ');
  $('.main-nav li').eq(2).find('a').addClass('hvr-underline-from-center-active');
  });
</script>

<script src="assets/js/jquery.audioControls.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
      var currentTrack = 0;
      $("#playListContainer").audioControls({
        autoPlay : false,
        shuffled: false,
        timer: 'increment',
        
        
        onPause: function(response){ 
          $('.song-thumb').removeClass('active');   
          $('#playListContainer li').eq(currentTrack).find('.eq .bar-eq').removeClass('active');
          $('#playListContainer li').eq(currentTrack).find('.eq .icon').addClass('active');


        },
        onPlay: function(response){
          $('.song-thumb').addClass('active');
          $('#playListContainer li').eq(currentTrack).find('.eq .bar-eq').addClass('active');
          $('#playListContainer li').eq(currentTrack).find('.eq .icon').removeClass('active');

        },
        
        onAudioChange : function(response){
        
          $('.songPlay').text(response.title + ' ...');
        
          currentTrack = response.index;  

          $('#playListContainer li').find('.eq .bar-eq').removeClass('active');
          $('#playListContainer li').eq(currentTrack).find('.eq .bar-eq').addClass('active');

          $('#playListContainer li').find('.eq .icon').addClass('active');
          $('#playListContainer li').eq(currentTrack).find('.eq .icon').removeClass('active');


          $('.song-thumb').css('background-image', 'url(assets/img/playlists/thumb'+response.index+'.jpg)');
          if($('.song-thumb').hasClass('active')){
              $('.song-thumb').removeClass('active');
          }
          $('.song-thumb').addClass('active');

        },

        onVolumeChange : function(vol){
          var obj = $('.volume');
          if(vol <= 0){
            obj.attr('class','volume mute');
          } else if(vol <= 33){
            obj.attr('class','volume volume1');
          } else if(vol > 33 && vol <= 66){
            obj.attr('class','volume volume2');
          } else if(vol > 66){
            obj.attr('class','volume volume3');
          } else {
            obj.attr('class','volume volume1');
          }
        }
      });

    });
</script>




