
<section id="content" class=" animsition container">

      <div class="head-con img-news">
         <div class="line bg-white abs opt-2"></div>
         <nav class="h-mob">
             <ol class="cd-breadcrumb custom-separator">
               <li><a href="index.php" class="animsition-link" >Home</a></li>
               <li class="current"><em>Playlists</em></li>
             </ol>
          </nav>
          <h5 class="c-white abs sp2">Playlists</h5>
      </div>
     
      <div class=" container-inner" id="playlists-">
         <div class="row pad-top-40 pad-bot-40 c-gray3 "><h5>Mood and theme playlists</h5></div>
         <div class="row" id="playlists-cat">

        <? foreach($arr_playlist_cat as $item){ ?>
            <div class="item">
                 <a href="/Playlist/category/<?=$item->id?>" class="animsition-link">
                  <div class="CoverImage " style="background-image:url(<?=main_site_url().$item->thumnail_path?>)"></div> 
                  <span class="mask bg-blue"></span>
                  <h5><?=$item->subject?></h5>
                </a>
            </div>
           <? } ?> 
             

            
         </div>
      </div>

      <div class="row"> <div class="line bg-gray2 mar-bot-40"></div></div>

      <div class=" container-inner" id="playlists">
          <div class="row"> 
             <h5 class="row  pad-bot-40 c-gray3 ">Selected Playlists</h5>
          </div>

          <div class="row" id="infi-content"> 
            
              

	<? foreach($arr_playlist as $item){ ?>
             <div class="playlists-item">
                 <a href="/Playlist/detail/<?=$item->id?>" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(<?=main_site_url().$item->thumnail_path?>)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5><?=$item->subject?></h5></a>
              </div>
	<? } ?>
             
         </div>
        <!-- <a id="next" href="playlists2.html">next page?</a>-->

      </div>

 </section> 

<script>
  $(document).ready(function() {
  $('.main-nav li').find('a').removeClass('hvr-underline-from-center-active ');
  $('.main-nav li').eq(2).find('a').addClass('hvr-underline-from-center-active');
  });
</script>

