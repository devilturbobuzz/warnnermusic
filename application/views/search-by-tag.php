
<section id="search" class=" animsition container">
    <div class="head-con img-artists">
        <div class="line bg-white abs opt-2"></div>
        <nav class="h-mob">
            <ol class="cd-breadcrumb custom-separator">
               <li><a href="index.php" class="animsition-link" >Home</a></li>
               <li class="current"><em>Search</em></li>
            </ol>
        </nav>
        <h5 class="c-white abs sp2">Search</h5>
    </div>
    <div class=" container row" id="search">
        <div class="container-inner" >
            <div class="row pad-top-40 c-gray3 pad-bot-20"><h3>Tag :  <?=$str_tags=="index"?"-":$str_tags?><h3>

            </div>
            
            <div class="row"> <div class="line bg-gray2 mar-bot-20 "></div></div>
            <div class="col-2 c-gray bold h-mob "><h5>Artists</h5></div>
            <div class="row"> <div class="line bg-gray2  mar-top-20 mar-bot-20"></div></div>
        </div>
    </div>
    <div id="container" class=" container-inner pad-top-40">
	<?php 
         foreach($arr_tags_artist as $item){
	?>
         <div class="playlists-item element rock">
          <a href="artist/detail/<?=$item->artist_id?>" class="animsition-link">
          <div class="CoverImage " style="background-image:url(<?=main_site_url().$item->thumnail_path?>)"></div> 
          <span class="mask bg-blue-gra2"></span>
          <h5 class="name"><?=$item->artist_name?></h5></a>
        </div>

   <? } ?>

    </div> <!-- #container -->
    <div id="container" class=" container-inner pad-top-40">
        <div class=" container row" id="search">
            <div class="col-2 c-gray bold h-mob "><h5>Playlists</h5></div>
            <div class="row"> <div class="line bg-gray2  mar-top-20 mar-bot-20"></div></div>
		<?php 
         foreach($arr_tags_playist as $item){
		?>
            <div class="playlists-item">
                 <a href="/playlist/detail/<?=$item->id?>" class="animsition-link">
                 <div class="CoverImage " style="background-image:url(<?=main_site_url().$item->thumnail_path?>)"></div> 
                 <span class="mask bg-blue-gra2"></span>
                 <h5><?=$item->subject?></h5></a>
              </div>

            <? } ?>
        </div>
        <div class=" container row" id="search">
            <div class="col-2 c-gray bold h-mob "><h5>News</h5></div>
            <div class="row"> <div class="line bg-gray2  mar-top-20 mar-bot-20"></div></div>
               <?php 
				foreach($arr_tags_news as $item){
			?>
               <div class=" eff-2 " >
                  <a href="/News/News-Detail/<?=$item->id?>" class="animsition-link" >
                    <div class="col-25" style="height: 400px;">
                       <img style="padding-bottom: 10px; width: 90%;" src="<?=main_site_url().$item->thumnail_path?>"><br>
                       <h5 class="c-gray"><?=$item->date_news?></h5><br>
                       <p style="width: 90%;">
                      <?=$item->subject_news?>
                       </p>
                     </div>
                  </a>
                </div>
                <? } ?>
                </div>
              </div>
            </div>

        </div>
    </div>

    
 

</section>


