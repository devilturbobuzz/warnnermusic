(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());


(function(){

    var History = window.History;
    if (History.enabled) {
        if(!detectmob()){
          State = History.getState();
          // set initial state to first page that was loaded
          History.pushState({urlPath: window.location.pathname}, $("title").text(), State.urlPath);
        }else{
        }

    } else {
        return false;
    }

    var loadAjaxContent = function(target, urlBase, selector) {
        //console.log(urlBase + ' ' + selector);
        //var selector = '#container';
        //var target = '#preContainer';
        $.fn.fullpage.moveTo('1');

        if($('#preload').length > 0){
            $('#preload').remove();
        }

        $('body').prepend('<div id="preload"><div class="loader-img"></div></div>');

        $.ajax({
            url: urlBase,
            dataType: 'html',
            success: function(html) {
                //var data = html;
                var data = $('#container', $(html)).addClass('done');

                setTimeout(function () {
                    $('#preload').remove();

                    OutAllEffect(function(){
                        $('#content').html(data);
                        $.fn.fullpage.destroy('all');
                        initContent();
                    });

                }, 800);


            }
        });

    };

    var updateContent = function(State) {

        selector = '#container';
        loadAjaxContent('#content', State.url, selector);
    };

    var updateContentMobile = function (State) {
        location.href = State.url;
    };

    // Content update and back/forward button handler
    History.Adapter.bind(window, 'statechange', function() {
        if(!detectmob()){
            updateContent(History.getState());
        }else{
            updateContentMobile(History.getState())
        }

    });

    // navigation link handler
    $('body').on('click', 'a.history-link', function(e) {
        var urlPath = $(this).attr('href');
        var title = $(this).text();
        History.pushState({urlPath: urlPath}, title, urlPath);
        return false; // prevents default click action of <a ...>
    });


    //Preload for 1st open
    $('.animsition-overlay').animsition({
        inClass: 'fade-in',
        inDuration: 1000,
        linkElement: '.animsition-link',
        loading: true,
        loadingParentElement: 'body', //animsition wrapper element
        loadingClass: 'logo-loading',
        loadingInner: '<img src="img/logo-loading.png" />', // e.g '<img src="loading.svg" />'
        timeout: false,
        timeoutCountdown: 2000,
        onLoadEvent: true,
        browser: [ 'animation-duration', '-webkit-animation-duration'],
        // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
        // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
        overlay : false,
        overlayClass : 'animsition-overlay-slide'
        //overlayParentElement : '.layoutHide',
        //transition: function(url){ window.location.href = url; }
    })
    .one('animsition.inStart',function(){
        $('body').removeClass('bg-init');
        initContent();
    });

    function initContent(){

        //Slider Init
        $(".section-main-slider").owlCarousel({
            items : 3,
            itemsDesktopSmall: [1199,2],
            itemsTablet: [768,2],
            lazyLoad : true,
            navigation : true,
            navigationText : ["&larr;","&rarr;"],
            pagination : true
        });

        //Single Slider Init
        $(".single-slider").owlCarousel({
            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true

        });

        //Single Slider Init
        $(".solution-slider").owlCarousel({
            items : 4, // above 1000px browser width
            lazyLoad : true,
            itemsDesktop : [1000,3], // between 1000px and 901px
            itemsDesktopSmall : [900,3], // betweem 900px and 601px
            itemsTablet: [600,2], // between 600 and 0
            itemsMobile :  [460,1], // itemsMobile disabled - inherit from itemsTablet option
            pagination : false,
            navigation : true,
            lazyLoad : true,
            navigationText: false
        });

        //Single Slider Init
        $(".gallery-slider").owlCarousel({
            items : 4, // above 1000px browser width
            itemsDesktop : [1000,3], //1000px and 901px
            itemsDesktopSmall : [900,2], // betweem 900px and 601px
            itemsTablet: [600,2], // between 600 and 0
            itemsMobile : [460,1], // itemsMobile disabled - inherit from itemsTablet option
            pagination : false,
            navigation : true,
            navigationText: false,
            lazyLoad : true,
            scrollPerPage: 4
            //afterAction : galleryCounter
        });

        $('.chess-slider').owlCarousel({
            items : 1, // above 1000px browser width
            scrollPerPage: true,
            itemsDesktop : [1000,1], //1000px and 901px
            itemsDesktopSmall : [900,1], // betweem 900px and 601px
            itemsTablet: [600,1], // between 600 and 0
            itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
            pagination : false,
            navigation : true,
            transitionStyle: 'fade',
            addClassActive: true,
            navigationText : ["&larr;","&rarr;"],
            afterInit: function () {
                
            },
            afterMove: function () {
                
            }

        });

        function galleryCounter(){
            $('.gallery-counter .length').html(this.owl.owlItems.length);
            $('.gallery-counter .current').html(this.owl.currentItem);
            
        }

        var download_slider = new Swiper('.download-slider', {
            pagination: '.swiper-pagination',
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            nextButton: '.download-next-btn',
            prevButton: '.download-prev-btn',
            coverflow: {
                rotate: 5,
                stretch: 0,
                depth: 100,
                modifier: 4,
                slideShadows : true
            }
        });

        //Circle Slider Init
        var circleSlider = new Swiper('.circle-slider', {
            slidesPerView: 3,
            centeredSlides: true,
            paginationClickable: true,
            spaceBetween: 30,
            nextButton: '.circle-next-btn',
            prevButton: '.circle-prev-btn',
            breakpoints: {
                480: {
                    slidesPerView: 1,
                }
            },
            onInit: function (swiper) {
                swiper.slideTo(2);

            }
        });


        //Vertical Slider
        var vertical_slider = new Swiper('.vertical-slider', {
            slidesPerView: 'auto',
            direction: 'vertical',
            spaceBetween: 5,
            centeredSlides: true,
            mousewheelControl: true,
            nextButton: '.vertical-next-btn',
            prevButton: '.vertical-prev-btn',
            autoHeight: true,
            slideToClickedSlide: true,
            slidesOffsetBefore: -90,
            onInit: function(swiper){

                LogoChange();

                swiper.slideTo(3);

                $(".vertical-slider .vertical-slider-elem").eq(6).addClass('active');

                //$('.swiper-slide-active').css({'padding': '0 0 100px 0'});
            },
            onSlideChangeStart: function (swiper) {

                $(".vertical-slider .vertical-slider-elem").removeClass('active');
                $(".vertical-slider .vertical-slider-elem").eq(swiper.activeIndex).addClass('active');

                //$('.swiper-slide').css({'height': 'auto'});
                //$('.swiper-slide-active').css({'height': '200px'});
                LogoChange();
            }
        });
        var vertical_slider_content = new Swiper('.vertical-slider-content', {
            direction: 'vertical',
            autoHeight: true,
            onInit: function(swiper){
                swiper.slideTo(3);
                contentChange();
            },
            onSlideChangeStart: function (swiper) {
                contentChange();
            }
        });

        function LogoChange(){
            var data = $('.swiper-slide-active').html();
            $('.vertical-slider-selected').html(data);
        }

        function contentChange(){
            var data = $('.vertical-slider-content .swiper-slide-active').html();
            $('#data-content').html(data);
        }


        //Slider Synce
        vertical_slider.params.control = vertical_slider_content;
        vertical_slider_content.params.control = vertical_slider;


        //Slider init for SCROLLABLE CONTENT
        var ContentSlider = new Swiper('.content-slider', {
            scrollbar: '.swiper-scrollbar',
            direction: 'vertical',
            slidesPerView: 'auto',
            mousewheelControl: true,
            freeMode: true
        });

        var galleryTop = new Swiper('.gallery-top', {
            nextButton: '.gallery-button-next',
            prevButton: '.gallery-button-prev',
            spaceBetween: 0,
            onInit: function (swiper) {
                swiper.slideTo(6);

            }
        });
        var galleryThumbs = new Swiper('.gallery-thumbs', {
            spaceBetween: 0,
            centeredSlides: true,
            slidesPerView: 'auto',
            touchRatio: 0.2,
            slideToClickedSlide: true,
            onInit: function (swiper) {
                swiper.slideTo(6);

            }
        });
        galleryTop.params.control = galleryThumbs;
        galleryThumbs.params.control = galleryTop;


        //Prevent Fullpagejs Mousescroll when in this dom
        $('.vertical-slider,.prevent-page-scroll,.white-popup').hover(function () {
            $.fn.fullpage.setMouseWheelScrolling(false);
        }, function () {
            $.fn.fullpage.setMouseWheelScrolling(true);
        });

        $('.popup').magnificPopup({
            removalDelay: 300,
            mainClass: 'mfp-fade'
        });

        //Tabs Init
        $('.tab-list a').click(function (e) {
            e.preventDefault();

            $(this).parents('.tab-list').find('li a').removeClass('active');
            $(this).addClass('active');

            var tabFor = $(this).parents('.tab-list').attr('data-tab-for');
            var tab = $(this).attr('data-tab');

            console.log(tab);

            $(tabFor + ' ' + '.tab-pane').removeClass('active');
            $(tabFor + ' ' + tab).addClass('active');

            if($(tabFor + ' ' +'.tab-pane').hasClass('scale-effect')){

                $(tabFor + ' ' + '.tab-pane').removeClass('animated');

                setTimeout(function () {
                    $(tabFor + ' ' + tab).addClass('animated');
                },100);
            }


        });


        //slider on click
        $('.section-main-slider .item').click(slideClick);


        //Loop add hide start effect
        $('.layoutHide').each(function(i){
            var row = $(this);
            row.removeClass('end');
            row.addClass('start');
        });

        //start effect for slider item
        $('.item').each(function(i){
            var row = $(this);
            row.addClass('inEffect');
            setTimeout(function() {
                row.removeClass('inEffect');
            }, 100*i);
        });

        //init fullpage js
        if(!detectmob()){
            $('#content').fullpage({
                responsiveWidth: 1200,
                scrollOverflow: true,
                scrollOverflowOptions: {
                    scrollbars: true,
                    mouseWheel: true,
                    hideScrollbars: false,
                    fadeScrollbars: false,
                    disableMouse: true
                },
                afterRender : function(){

                    InAllEffect(1);

                },
                onLeave: function (index, nextIndex, direction){
                    //console.log(nextIndex);
                    InAllEffect(nextIndex);
                }
            });
        }else{
            $('#content').fullpage({
                responsiveWidth: 1200,
                afterRender : function(){

                    InAllEffect(1);

                },
                onLeave: function (index, nextIndex, direction){
                    //console.log(nextIndex);
                    InAllEffect(nextIndex);
                }
            });
        }


        
        //For Apply Jobs Study append
        var eduelem = $('.study-elem').wrap('<p/>').parent().html();
        var edulimit = $('.study-data').attr('data-limit');
        $('#add-edu').click(function () {
            var count = $('.study-elem').length;

            if(count < edulimit || typeof edulimit == 'undefined' || edulimit == ''){
                $('.study-data').append('<div class="edu-count">ประวัติการศึกษา #'+(count+1)+'</div>').append(eduelem);
                ContentSlider.update();
            }else{
                $(this).hide();
            }

        });

        //For Apply Jobs Exp append
        var work = $('.work-elem').wrap('<p/>').parent().html();
        var worklimit = $('.work-data').attr('data-limit');
        $('#add-work').click(function () {
            var count = $('.work-elem').length;

            if(count < worklimit || typeof worklimit == 'undefined' || worklimit == ''){
                $('.work-data').append('<div class="edu-count">ประวัติการทำงาน #'+(count+1)+'</div>').append(work);
                ContentSlider.update();
            }else{
                $(this).hide();
            }

        });

        $('.mobile-tiggle').click(function () {
            $('.header').toggleClass('active');
        });

        $('.mobile-close').click(function () {
            $('.header').removeClass('active');
        });

    }

    function OutAllEffect(callback) {

        var elems =  $('.layoutHide').withinviewport(),
            count = elems.length;

        //console.log(elems);

        elems.each(function(key){
            var row = $(this);
            row.removeClass('start').addClass('end');

            var delay = 150*key;

            if(typeof $(this).attr('data-delay') !== "undefined"){
                delay = $(this).attr('data-delay');
            }

            row.transition({'width':'100%',delay: delay}, 500,'easeInSine',function(){
                if (!--count) callback();
            });


        });

    }

    function InAllEffect(index){

        var vh = $('#content').find('section.section').eq(index-1);

        vh.find('.layoutHide').each(function (key) {

            var delay = 150*key;

            if(typeof $(this).attr('data-delay') !== "undefined"){
                delay = $(this).attr('data-delay');
            }

            $(this).transition({'width':'0%',delay: delay}, 600,'easeOutSine');
        });
        //console.log(data.find())
    }


    function slideClick(e){
        e.preventDefault();

        var html = $(this).html();

        var top = $(this).offset().top,
            left = $(this).offset().left,
            width = $(this).outerWidth(),
            height = $(this).outerHeight();

        var dupCon = $(this).parents('.section-main-slider-con').find('.slide-animate');

        dupCon.html(html);

        var dubElem = dupCon.find('.item-bg');

        //Init Style before animate
        dubElem.css({
            'top':top,
            'left': left,
            'width': width,
            'height': height
        });

        //Animate effect
        dubElem.transition({
            'width': '100%',
            'left':'0',
            'right':'0',
            delay: 200
        }, 700,'ease');

        //console.log(top,left);

    }


})(jQuery);

function detectmob() {
    if( navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
    ){
        return true;
    }
    else {
        return false;
    }
}