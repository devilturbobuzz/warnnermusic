// Create a WaveSurfer instance
var wavesurfer = Object.create(WaveSurfer);


// Init on DOM ready
document.addEventListener('DOMContentLoaded', function () {
    wavesurfer.init({
        container: '#waveform',
        waveColor: '#428bca',
        progressColor: '#31708f',
        height: 30,
        barWidth: 3
    });
});


// Bind controls
document.addEventListener('DOMContentLoaded', function () {
    var playPause = document.querySelector('#playPause');
    playPause.addEventListener('click', function () {
        wavesurfer.playPause();
    });

    // Toggle play/pause text
    wavesurfer.on('play', function () {
        document.querySelector('#play').style.display = 'none';
        document.querySelector('#pause').style.display = '';

        $('.record').addClass('active');
    });
    wavesurfer.on('pause', function () {
        document.querySelector('#play').style.display = '';
        document.querySelector('#pause').style.display = 'none';
        $('.record').removeClass('active');
    });


    // The playlist links
    var links = document.querySelectorAll('#playlist a');
    
    var currentTrack = 0;

    // Load a track by index and highlight the corresponding link
    var setCurrentSong = function (index) {
        //links[currentTrack].classList.remove('active');
        currentTrack = index;

       //links[currentTrack].classList.add('active');

       // $('#plList li:eq(' + index + ')').addClass('active');



        wavesurfer.load(links[currentTrack].href);
        
       /* this.find("#list-group-item").eq(currentTrack).addClass("active");*/
         /*("#list-group-item").addClass("active");*/
    };


      var Target = $('.list-group-item a');
        Target.on('click', function(index){
        event.preventDefault();
       $('.list-group-item').removeClass('active');
       $(this).parents('.list-group-item').addClass('active');
       //alert($(this));
      });
       

    // Load the track on click
    Array.prototype.forEach.call(links, function (link, index) {
        link.addEventListener('click', function (e) {
            e.preventDefault();
            setCurrentSong(index);
        });
    });

    // Play on audio load
    wavesurfer.on('ready', function () {
        //wavesurfer.play();
        //wavesurfer.playPause();


        
    });

    // Go to the next track on finish
    wavesurfer.on('finish', function () {
        setCurrentSong((currentTrack + 1) % links.length);
    });

    // Load the first track
    setCurrentSong(currentTrack);
});
