$(function() {

    var newHash      = "",
        $mainContent = $("#main-content"),
        $pageWrap    = $("#page-wrap"),
        baseHeight   = 0,
        $el;
        
    $pageWrap.height($pageWrap.height());
    baseHeight = $pageWrap.height() - $mainContent.height();
    
    $("nav").delegate("a", "click", function() {
        window.location.hash = $(this).attr("href");
        return false;
    });

    $("#mySelect").change(function(){
      $mainContent.load( $(this).val() );
       //window.location.hash = $(this).val();

        //location.href = $(this).val();

      //alert($(this).val());

        return false;


    });
    
    $(window).bind('hashchange', function(){
    
        newHash = window.location.hash.substring(1);
        //$mainContent.hide().load(newHash + " #guts", function() {

        
        if (newHash) {
            $mainContent
                .find("#guts")
                .fadeOut(200, function() {
                    $mainContent.hide().load(newHash + " #guts", function() {
                        $mainContent.fadeIn(200, function() {
                            $pageWrap.animate({
                                height: baseHeight + $mainContent.height() + "px"
                            });
                        });
                        //$("nav a").removeClass("current");
                        //$("nav a[href="+newHash+"]").addClass("current");
                    });
                });
        };

        
    $("#artist-sync1").owlCarousel({
            navigation : true,
            singleItem:true,
            lazyLoad : true,
            pagination : false,
            navigationText: false,
            responsiveRefreshRate : 200
            //afterAction : syncPosition
        });

        
    });
    
    //$(window).trigger('hashchange');

  

});