jQuery(document).ready(function($){
    
    /*initTab()
    function initTab(){

        $('.tab-list a').click(function (e) {
            e.preventDefault();

            $(this).parents('.tab-list').find('li a').removeClass('active');
            $(this).addClass('active');

            var tabFor = $(this).parents('.tab-list').attr('data-tab-for');
            var tab = $(this).attr('data-tab');

            console.log(tab);

            $(tabFor + ' ' + '.tab-pane').removeClass('active');
            $(tabFor + ' ' + tab).addClass('active');

            if($(tabFor + ' ' +'.tab-pane').hasClass('scale-effect')){

                $(tabFor + ' ' + '.tab-pane').removeClass('animated');

                setTimeout(function () {
                    $(tabFor + ' ' + tab).addClass('animated');
                },100);
            }
        });
    }*/
    initSlide();
    function initSlide(){

        var sync1 = $("#artist-sync1"),sync2 = $("#artist-sync2"),
         status = $("#owlStatus"),status2 = $("#owlStatus-intro");

        $("#intro").owlCarousel({
 
            pagination : true,
            navigation : true,
            lazyLoad : true,
            navigationText: false,
            slideSpeed : 400,
            paginationSpeed : 400,
            singleItem:true,
            autoPlay : true,
            stopOnHover : true,
            afterAction : pagePosition
  
        });

        $("#hilight #right-section").owlCarousel({
 
            items : 3, 
            lazyLoad : true,
            itemsDesktop : [1000,3], 
            itemsDesktopSmall : [900,3], 
            itemsTablet: [600,2], 
            itemsMobile :  [460,1],
            pagination : true,
            navigation : false,
            navigationText: false
         });

        
        
 
         sync1.owlCarousel({
            navigation : true,
            singleItem:true,
            lazyLoad : true,
            pagination : false,
            navigationText: false,
            responsiveRefreshRate : 200,
            afterAction : syncPosition
        });

        sync2.owlCarousel({
           singleItem:true,
           pagination:false,
           lazyLoad : true,
           navigationText: false,
           responsiveRefreshRate : 100,
           afterInit : function(el){
            el.find(".owl-item").eq(0).addClass("synced");
           }
        });
 
        function updateResult(pos,value){
            status.find(pos).find(".result").text(value);
        }
         function updateResult2(pos,value){
           status2.find(pos).find(".result").text(value);
        }
        function pagePosition(){

            updateResult2(".owlItems-intro", this.owl.owlItems.length);
            updateResult2(".currentItem-intro", this.owl.currentItem +1);
        }
      

        function syncPosition(el){

           updateResult(".owlItems", this.owl.owlItems.length);
           updateResult(".currentItem", this.owl.currentItem +1);

           var current = this.currentItem;
           $("#artist-sync2")
           .find(".owl-item")
           .removeClass("synced")
           .eq(current)
           .addClass("synced")
           if($("#artist-sync2").data("owlCarousel") !== undefined){
               center(current)
           }
        }
        

        function center(number){
           var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
           var num = number;
           var found = false;
           for(var i in sync2visible){
              if(num === sync2visible[i]){
                 var found = true;
              }
           }
           if(found===false){
              if(num>sync2visible[sync2visible.length-1]){
                 sync2.trigger("owl.goTo", num - sync2visible.length+1)
           }else{
           if(num - 1 === -1){
             num = 0;
           }
              sync2.trigger("owl.goTo", num);
           }
           } else if(num === sync2visible[sync2visible.length-1]){
               sync2.trigger("owl.goTo", sync2visible[1])
           } else if(num === sync2visible[0]){
           sync2.trigger("owl.goTo", num-1)
           }
    
  }
 

   

    }
           
});