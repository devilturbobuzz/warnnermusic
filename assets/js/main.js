jQuery(document).ready(function($){

      //addLoading();
    

      
      function initSlide(){

        var sync1 = $("#artist-sync1"),sync2 = $("#artist-sync2"),
         status = $("#owlStatus"),status2 = $("#owlStatus-intro"),status3 = $("#owlStatus-hi-light");;

        var time = 7; // time in seconds
        var $progressBar,$bar, $elem, isPause, tick,percentTime;
 

        $("#intro").owlCarousel({
 
            pagination : true,
            navigation : true,
            lazyLoad : true,
            navigationText: false,
            slideSpeed : 500,
            paginationSpeed : 500,
            singleItem:true,
            afterInit : progressBar,
            afterMove : moved,
            startDragging : pauseOnDragging,
            afterAction : pagePosition
  
        });

         //Init progressBar where elem is $("#owl-demo")
        function progressBar(elem){
          $elem = elem;
          //build progress bar elements
          buildProgressBar();
          //start counting
          start();
        }
        //create div#progressBar and div#bar then prepend to $("#owl-demo")
        function buildProgressBar(){
           $progressBar = $("<div>",{
              id:"progressBar"
           });
           $bar = $("<div>",{
             id:"bar"
           });
          $progressBar.append($bar).prependTo($elem);
        }
        function start() {
           //reset timer
           percentTime = 0;
           isPause = false;
           //run interval every 0.01 second
          tick = setInterval(interval, 10);
        };
        function interval() {
          if(isPause === false){
              percentTime += 1 / time;
              $bar.css({
              width: percentTime+"%"
           });
           //if percentTime is equal or greater than 100
          if(percentTime >= 100){
              //slide to next item 
              $elem.trigger('owl.next')
             }
           }
        }


        //pause while dragging 
        function pauseOnDragging(){
          isPause = true;
        }
 
        //moved callback
        function moved(){
          //clear interval
          clearTimeout(tick);
          //start again
          start();
        }
 
 

        $("#hilight-news").owlCarousel({
            items : 3, 
            lazyLoad : true,
            itemsDesktop : [1000,3], 
            itemsDesktopSmall : [768,2], 
            itemsTablet: [600,1], 
            itemsMobile :  [460,1],
            pagination : true,
            navigation : true,
            navigationText: false,
            afterAction : pagePosition
         });


        $("#playlists-cat").owlCarousel({
 
            items : 6, 
            lazyLoad : true,
            itemsDesktop : [1000,6], 
            itemsDesktopSmall : [900,3], 
            itemsTablet: [600,2], 
            itemsMobile :  [460,1],
            pagination : true,
            navigation : false,
            navigationText: false

         });

       

        $("#artists-category").owlCarousel({
 
            items : 5, 
            lazyLoad : true,
            itemsDesktop : [1000,4], 
            itemsDesktopSmall : [900,3], 
            itemsTablet: [600,2], 
            itemsMobile :  [460,1],
            pagination : true,
            navigation : false,
            navigationText: false

         });

        $("#artists-geners").owlCarousel({
 
            items : 6, 
            lazyLoad : true,
            itemsDesktop : [1000,6], 
            itemsDesktopSmall : [900,3], 
            itemsTablet: [600,2], 
            itemsMobile :  [460,1],
            pagination : true,
            navigation : false,
            navigationText: false

         });




        
        
 
         sync1.owlCarousel({
            navigation : true,
            singleItem:true,
            lazyLoad : true,
            pagination : false,
            navigationText: false,
            responsiveRefreshRate : 200,
            afterAction : syncPosition
        });

        sync2.owlCarousel({
           singleItem:true,
           pagination:false,
           lazyLoad : true,
           navigationText: false,
           responsiveRefreshRate : 100,
           afterInit : function(el){
            el.find(".owl-item").eq(0).addClass("synced");
           }
        });
 
        function updateResult(pos,value){
            status.find(pos).find(".result").text(value);
        }

         function updateResult2(pos,value){
           status2.find(pos).find(".result").text(value);
        }

         function updateResult3(pos,value){
           status3.find(pos).find(".result").text(value);
        }

        function pagePosition(){

            updateResult2(".owlItems-intro", this.owl.owlItems.length);
            updateResult2(".currentItem-intro", this.owl.currentItem +1);

            updateResult3(".owlItems-hi-light", this.owl.owlItems.length);
            updateResult3(".currentItem-hi-light", this.owl.currentItem +1);
        }
      

        function syncPosition(el){

           updateResult(".owlItems", this.owl.owlItems.length);
           updateResult(".currentItem", this.owl.currentItem +1);

           var current = this.currentItem;
           $("#artist-sync2")
           .find(".owl-item")
           .removeClass("synced")
           .eq(current)
           .addClass("synced")
           if($("#artist-sync2").data("owlCarousel") !== undefined){
               center(current)
           }
        }
        

        function center(number){
           var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
           var num = number;
           var found = false;
           for(var i in sync2visible){
              if(num === sync2visible[i]){
                 var found = true;
              }
           }
           if(found===false){
              if(num>sync2visible[sync2visible.length-1]){
                 sync2.trigger("owl.goTo", num - sync2visible.length+1)
           }else{
           if(num - 1 === -1){
             num = 0;
           }
              sync2.trigger("owl.goTo", num);
           }
           } else if(num === sync2visible[sync2visible.length-1]){
               sync2.trigger("owl.goTo", sync2visible[1])
           } else if(num === sync2visible[0]){
           sync2.trigger("owl.goTo", num-1)
           }
    
        }
       
      }

      function addLoading(){  
        if($('#preload').length > 0){
            $('#preload').addClass('hide');
        }
        $('#preload').addClass('show');
      }

      initPreload();
      function initPreload(){

      // addLoading();


        $('.animsition').animsition({
           inClass: 'fade-in-down-sm',
           outClass: 'fade-out-up-sm',
           inDuration: 500,
           outDuration: 500,
           linkElement: '.animsition-link',
           loading: true,
           loadingParentElement: '#content', //animsition wrapper element
           loadingClass: 'loading',
           loadingInner: '<img src="assets/img/logo-loading.png" />', // e.g '<img src="loading.svg" />'
           timeout: false,
           onLoadEvent: true,
           browser: [ 'animation-duration', '-webkit-animation-duration']
      
        })

        .one('animsition.inStart',function(){
           /*Init Content*/
           $('body').removeClass('bg-init');
           //initFullpage();
           initSlide();
           Formmail();
           initInfinitScroll();
           
        });   
        $('.animsition').on('animsition.inEnd', function(){  $('#preload').addClass('hide'); });
        $('.animsition').on('animsition.outStart', function(){ });
        $('body').on('click', 'a.animsition-link', function(e) { /*$('#preload').addClass('show');*/  addLoading(); }); 

      }

      
      function initFullpage(){
          $('#content').fullpage({
              responsiveWidth: 1000,
              scrollOverflow: true,
              css3: true,
              verticalCentered: false,
          
              afterLoad: function(anchorLink, index){
                 if(index == 2){}
              },
              afterRender : function(){
                  //InAllEffect(1);
              },
              onLeave: function (index, nextIndex, direction){
               //InAllEffect(nextIndex);
                if (index == 1 && direction == 'down'){
                   $('header').removeClass('moveDown').addClass('moveUp');
                }
                else if(index == 2 && direction == 'up'){
                  $('header').removeClass('moveUp').addClass('moveDown');
                }
              }
          });
      }

      function InAllEffect(index){
        var vh = $('#content').find('section.section').eq(index-1);
          vh.find('.layoutHide').each(function (key) {
              var delay = 100*key;
            if(typeof $(this).attr('data-delay') !== "undefined"){
                delay = $(this).attr('data-delay');
            }
            //$(this).transition({'width':'0%',delay: delay}, 500,'ease-out');
          });
      }

      function Formmail(){

         if( $('.floating-labels').length > 0 ) floatLabels();

  function floatLabels() {
    var inputFields = $('.floating-labels .cd-label').next();
    inputFields.each(function(){
      var singleInput = $(this);
      //check if user is filling one of the form fields 
      checkVal(singleInput);
      singleInput.on('change keyup', function(){
        checkVal(singleInput);  
      });
    });
  }

  function checkVal(inputField) {
    ( inputField.val() == '' ) ? inputField.prev('.cd-label').removeClass('float') : inputField.prev('.cd-label').addClass('float');
  }
      }

      //cache DOM elements
      var sidebarTrigger = $('.cd-nav-trigger'),sidebar = $('.cd-side-nav');
      //mobile only - open sidebar when user clicks the hamburger menu
      sidebarTrigger.on('click', function(event){
          event.preventDefault();
         $([sidebar, sidebarTrigger]).toggleClass('nav-is-visible');
      });
       
      function initInfinitScroll(){
        $('#infi-content').infinitescroll({
          navSelector: "#next:last",
          nextSelector: "#next:last",
          itemSelector: "#infi-content",
          debug: false,
          dataType: 'html',
          path: undefined, 
          prefill: false, 
          maxPage: undefined 
        });
      }

      

   






           
});