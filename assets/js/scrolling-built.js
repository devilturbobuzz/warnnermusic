

// main menu
/*$(function(){
   $("a.px-gotoID").click(function(e){
        e.preventDefault();
         moveToID($(this).attr('href'));
    });
    function moveToID(id) {
        if (id.charAt(0) != "#") {
            location.href = id;
            return 0;
        }
        var pY = $(id).offset().top;        
        $('html,body').animate({scrollTop: pY}, 800, "easeInOutSine");
    }
});

$(function () {
    var _mainHeader = false;
    function moveScroll(){
        var _winTop = $(window).scrollTop();
        if(_winTop>0){
            if(!_mainHeader){
                $('.main-header').addClass('collapsed');
                _mainHeader = true;
            }
        }else if(_winTop<1 || _winTop==0){
            if(_mainHeader){
                $('.main-header').removeClass('collapsed');
                _mainHeader = false;
            }
        }
    }
    $(window).scroll(moveScroll);
});*/


//$('.fp-pagination').removeClass('show');
// fix sc detail
$(function () {

    var _vewH = $('html, body').height(),
        _screen = 0,
        _active = false;
		
		 $('.fp-pagination').addClass('show');
		
    function moveScroll(){
        var _winTop = $(window).scrollTop(),
            _anchorTop = $('#Coupon').offset().top,
            _anchorBottom = $('#Scan').offset().top;

        if(_winTop > _anchorTop && _winTop < _anchorBottom){
            if(!_active){
                $('.fp-pagination').addClass('show');
                _active = true;
            }
        }else if(_winTop > _anchorBottom){
            if(_active){
                $('.fp-pagination').removeClass('show');
                _active = false;
            }else{
            }
        }else if(_winTop < _anchorTop){
            if(_active){
                $('.fp-pagination').removeClass('show');
                _active = false;
            }
        }

    }
    moveScroll();
    $(window).scroll(moveScroll).resize(function(){
        moveScroll();
        _vewH = $('html, body').height();
        _anchorTop = $('#Coupon').offset().top;
        _anchorBottom = $('#Storelocator').offset().top;
    });

});

// scroll by id
$(function(){

    var _navHead = $('.active-menu'),
        _list = ['#Coupon','#Clubcard','#Promotion','#Notification','#Storelocator','#IBcon','#Scan'],
        _max = _list.length,
        _scrollFix = false,
        _pG = 0;
        _pY = 1000;
		
		
       
    for(var i = 0;i<_max;i++){
        _list[i] = $(_list[i]);
    }

    function moveToID(id){
        var pY = $(id).offset().top;
        $('html,body').animate({scrollTop:pY}, 800,"easeInOutSine", function() {  });
    }
    _navHead.find("li").click(function(e){
        e.preventDefault();
        moveToID($(this).find("a").attr('href'));
    });
  
    function viewPage(a){
        var b = a; 
        for (var i = _max-1; i>=0; i--){
            if(_list[i].offset().top<b)return i;
        }
        return -1;
    }

    var _section = $(".sc-role"),
        _maxSec = _section.length,
        _pg = $(".fp-pagination").find("li"),
        _offsetNavTop = _section.height()/2;
        _navIndex = 0;
		
		$(_pg[_navIndex]).addClass("active");

    function navScroll(){
        var _top = $(window).scrollTop()+_offsetNavTop,
            _pY = $(window).scrollTop(),
            _reflow = true,
            _index = viewPage(_top);
            
        for( var i = 0; i < _maxSec; i++){
            var _obj = $(_section[i]),
                _sTop = _obj.offset().top,
                _sBottom = _sTop + _obj.height();
            if((_top>=_sTop)&&(_top<_sBottom)){
                if(_navIndex == i) {
                    _reflow = false;
                    break;
                }
                break;
            }
        }

        if(_reflow && (_navIndex!=i)){
            _navIndex = i;
            _section.removeClass("focus");
            _pg.removeClass("active");

            if(i<_maxSec){
                _obj.addClass("focus");
                $(_pg[_navIndex]).addClass("active");
                iosSliderDesktop(_navIndex);
            }
        }

    }
    navScroll();
    $(window).scroll(navScroll).resize(function(){
        navScroll();
    });

});

// autoSc
$(function () {

    var _section = $('.autoSc'),
        _endAuto = $('.endautoSc'),
        _totalSection = _section.length,
        _navIndex = -1;
		
    var winW = $('html, body').width();
	  
    function moveScroll() {
		
        var _pY = $(window).scrollTop(),
            _reflow = true;
           
        for( var i = 0; i < _totalSection; i++){
            var _obj = $(_section[i]),
                _top = _obj.offset().top,
                _bottom = _top + _obj.innerHeight();
               
            if((_pY>=_top)&&(_pY<_bottom)){
                if(_navIndex == i) {
                    _reflow = false;
                    break;
                }
                break;
            }
        }
	
		 clearTimeout($.data(this, 'scrollTimer'));
        $.data(this, 'scrollTimer', setTimeout(function() {
            var _notAuto = _endAuto.offset().top + _obj.innerHeight();
            if(_pY<_notAuto){
				 var _halfPoint = _top+(_section.innerHeight()/2);
				  //var _halfPoint = 0;
                if(_pY<_halfPoint){ 
                    TweenLite.to(window, 0.4, {scrollTo:{y:_top, x:0}, ease:Power4.easeOut})
                }
                if(_pY>_halfPoint){
                    TweenLite.to(window, 0.4, {scrollTo:{y:_bottom, x:0}, ease:Power4.easeOut})
                }
            }else if(_pY>_notAuto){
				
          }
      }, 100));
	
    }
	
   /* $(window).scroll(moveScroll).resize(function(){ 
		   moveScroll(); 
   });*/
   $( window ).resize(function() {
		winW = $('html, body').width();
		 moveScroll(); 
	})
    $(window).scroll(function(){
		if(winW >= 800) {	
		   moveScroll(); 
		}else{}
   })


});


